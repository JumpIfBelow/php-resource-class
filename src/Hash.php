<?php

namespace ResourceClass;

use ResourceWrapper\AbstractResourceWrapper;

/**
 * Class Hash
 * @package ResourceWrapper
 * @link https://www.php.net/manual/en/book.hash.php
 */
class Hash extends AbstractResourceWrapper
{
    public const HMAC = HASH_HMAC;

    /**
     * @return array
     * @see hash_algos()
     */
    public static function algos()
    {
        return static::staticCall('hash_algos', func_get_args());
    }

    /**
     * @param string $knownString
     * @param string $userString
     * @return bool
     * @see hash_equals()
     */
    public static function equals(string $knownString, string $userString)
    {
        return static::staticCall('hash_equals', func_get_args());
    }

    /**
     * @param string $algorithm
     * @param string $filename
     * @param bool $rawOutput
     * @return string
     * @see hash_file()
     */
    public static function file(string $algorithm, string $filename, bool $rawOutput = false)
    {
        return static::staticCall('hash_file', func_get_args());
    }

    /**
     * @param string $algorithm
     * @param string $filename
     * @param string $key
     * @param bool $rawOutput
     * @return string
     * @see hash_hmac_file()
     */
    public static function hmacFile(string $algorithm, string $filename, string $key, bool $rawOutput = false)
    {
        return static::staticCall('hash_hmac_file', func_get_args());
    }

    /**
     * @param string $algorithm
     * @param string $data
     * @param string $key
     * @param bool $rawOutput
     * @return string
     * @see hash_hmac()
     */
    public static function hmac(string $algorithm, string $data, string $key, bool $rawOutput = false)
    {
        return static::staticCall('hash_hmac', func_get_args());
    }

    /**
     * @param string $algorithm
     * @param int $options
     * @param string|null $key
     * @return static
     * @see hash_init()
     */
    public static function init(string $algorithm, int $options = 0, string $key = null)
    {
        return static::initResource('hash_init', func_get_args());
    }

    /**
     * @param string $algorithm
     * @param string $password
     * @param string $salt
     * @param int $iterations
     * @param int $length
     * @param bool $rawOutput
     * @return mixed
     * @see hash_pbkdf2()
     */
    public static function pbkdf2(string $algorithm, string $password, string $salt, int $iterations, int $length = 0, bool $rawOutput = false)
    {
    	return static::staticCall('hash_pbkdf2', func_get_args());
    }

    /**
     * @param string $algorithm
     * @param string $data
     * @param bool $rawOutput
     * @return string
     * @see hash()
     */
    public static function hash(string $algorithm, string $data, bool $rawOutput = false)
    {
        return static::staticCall('hash', func_get_args());
    }

    /**
     * @inheritdoc
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'Hash Context',
        ];
    }

    /**
     * @return static
     * @see hash_copy()
     */
    public function copy()
    {
        return static::initResource('hash_copy', $this->compileParameters());
    }

    /**
     * @param bool $rawOutput
     * @return string
     * @see hash_final()
     */
    public function final(bool $rawOutput = false)
    {
	    return $this->dynamicCall('hash_finale', func_get_args());
    }

    /**
     * @param string $filename
     * @param Stream|resource|null $context
     * @return bool
     * @see hash_update_file()
     */
    public function updateFile(string $filename, $context = null)
    {
	    return $this->dynamicCall('hash_update_file', func_get_args());
    }

    /**
     * @param Stream|resource|null $handle
     * @param int $length
     * @return int
     * @see hash_update_stream()
     */
    public function updateStream($handle, $length = -1)
    {
	    return $this->dynamicCall('hash_update_stream', func_get_args());
    }

    /**
     * @param string $data
     * @return bool
     * @see hash_update()
     */
    public function update(string $data)
    {
	    return $this->dynamicCall('hash_update', func_get_args());
    }
}
