<?php

namespace ResourceClass;

use ResourceWrapper\AbstractCloseableResourceWrapper;

/**
 * Class File
 * @package ResourceClass
 * @link https://www.php.net/manual/en/book.filesystem.php
 */
class File extends AbstractCloseableResourceWrapper
{
    public const SEEK_END = SEEK_END;

    public const LOCK_SH  = LOCK_SH;
    public const LOCK_EX = LOCK_EX;
    public const LOCK_UN = LOCK_UN;
    public const LOCK_NB = LOCK_NB;

    public const GLOB_BRACE = GLOB_BRACE;
    public const GLOB_ERR = GLOB_ERR;
    public const GLOB_ONLYDIR = GLOB_ONLYDIR;
    public const GLOB_MARK = GLOB_MARK;
    public const GLOB_NOSORT = GLOB_NOSORT;
    public const GLOB_NOCHECK = GLOB_NOCHECK;
    public const GLOB_NOESCAPE = GLOB_NOESCAPE;
    public const GLOB_AVAILABLE_FLAGS = GLOB_AVAILABLE_FLAGS;

    public const PATHINFO_DIRNAME = PATHINFO_DIRNAME;
    public const PATHINFO_BASENAME = PATHINFO_BASENAME;
    public const PATHINFO_EXTENSION = PATHINFO_EXTENSION;
    public const PATHINFO_FILENAME = PATHINFO_FILENAME;

    public const FILE_USE_INCLUDE_PATH = FILE_USE_INCLUDE_PATH;
    public const FILE_NO_DEFAULT_CONTEXT = FILE_NO_DEFAULT_CONTEXT;
    public const FILE_APPEND = FILE_APPEND;
    public const FILE_IGNORE_NEW_LINES = FILE_IGNORE_NEW_LINES;
    public const FILE_SKIP_EMPTY_LINES = FILE_SKIP_EMPTY_LINES;
    public const FILE_BINARY = FILE_BINARY;
    public const FILE_TEXT = FILE_TEXT;

    public const INI_SCANNER_NORMAL = INI_SCANNER_NORMAL;
    public const INI_SCANNER_RAW = INI_SCANNER_RAW;
    public const INI_SCANNER_TYPED = INI_SCANNER_TYPED;

    public const FNM_NOESCAPE = FNM_NOESCAPE;
    public const FNM_PATHNAME = FNM_PATHNAME;
    public const FNM_PERIOD = FNM_PERIOD;
    public const FNM_CASEFOLD = FNM_CASEFOLD;

    /**
     * @param string $path
     * @param string|null $suffix
     * @return string
     * @see basename()
     */
    public static function basename(string $path, string $suffix = null)
    {
        return static::staticCall('basename', func_get_args());
    }

    /**
     * @param string $filename
     * @param string|int $group
     * @return bool
     * @see chgrp()
     */
    public static function chgrp(string $filename, $group)
    {
        return static::staticCall('chgrp', func_get_args());
    }

    /**
     * @param string $filename
     * @param int $mode
     * @return bool
     * @see chmod()
     */
    public static function chmod(string $filename, int $mode)
    {
        return static::staticCall('chmod', func_get_args());
    }

    /**
     * @param string $filename
     * @param string|int $user
     * @return bool
     * @see chown()
     */
    public static function chown(string $filename, $user)
    {
        return static::staticCall('chown', func_get_args());
    }

    /**
     * @param bool $clearRealpathCache
     * @param string|null $filename
     * @see clearstatcache()
     */
    public static function clearStatCache(bool $clearRealpathCache = false, string $filename = null)
    {
        return static::staticCall('clearstatcache', func_get_args());
    }

    /**
     * @param string $source
     * @param string $destination
     * @param Stream|resource|null $context
     * @return bool
     * @see copy()
     */
    public static function copy(string $source, string $destination, $context = null)
    {
        return static::staticCall('copy', func_get_args());
    }

    /**
     * @param string $path
     * @param int $levels
     * @return string
     * @see dirname()
     */
    public static function dirname(string $path, int $levels = 1)
    {
        return static::staticCall('dirname', func_get_args());
    }

    /**
     * @param string $directory
     * @return float
     * @see disk_free_space()
     */
    public static function diskFreeSpace(string $directory)
    {
        return static::staticCall('disk_free_space', func_get_args());
    }

    /**
     * @param string $directory
     * @return float
     * @see disk_total_space()
     */
    public static function diskTotalSpace(string $directory)
    {
        return static::staticCall('disk_total_space', func_get_args());
    }

    /**
     * @param string $filename
     * @return bool
     * @see file_exists()
     */
    public static function exists(string $filename)
    {
        return static::staticCall('file_exists', func_get_args());
    }

    /**
     * @param string $filename
     * @param int $flags
     * @param Stream|resource|null $context
     * @param int $offset
     * @param int|null $maxLength
     * @return string|false
     * @see file_get_contents()
     */
    public static function getContents(string $filename, int $flags = FILE_TEXT, $context = null, int $offset = 0, int $maxLength = null)
    {
        return static::staticCall('file_get_contents', func_get_args());
    }

    /**
     * @param string $filename
     * @param Stream|resource|array|string $data
     * @param int $flags
     * @param null $context
     * @return int
     * @see file_put_contents()
     */
    public static function putContents(string $filename, $data, int $flags = FILE_TEXT, $context = null)
    {
        return static::staticCall('file_put_contents', func_get_args());
    }

    /**
     * @param string $filename
     * @param int $flags
     * @param Stream|resource|null $context
     * @return array|false
     * @see file()
     */
    public static function file(string $filename, int $flags = FILE_TEXT, $context = null)
    {
        return static::staticCall('file', func_get_args());
    }

    /**
     * @param string $filename
     * @return int
     * @see fileatime()
     */
    public static function atime(string $filename)
    {
        return static::staticCall('fileatime', func_get_args());
    }

    /**
     * @param string $filename
     * @return int
     * @see filectime()
     */
    public static function ctime(string $filename)
    {
        return static::staticCall('filectime', func_get_args());
    }

    /**
     * @param string $filename
     * @return int
     * @see filegroup()
     */
    public static function group(string $filename)
    {
        return static::staticCall('filegroup', func_get_args());
    }

    /**
     * @param string $filename
     * @return int
     * @see fileinode()
     */
    public static function inode(string $filename)
    {
        return static::staticCall('fileinode', func_get_args());
    }

    /**
     * @param string $filename
     * @return int
     * @see filemtime()
     */
    public static function mtime(string $filename)
    {
        return static::staticCall('filemtime', func_get_args());
    }

    /**
     * @param string $filename
     * @return int
     * @see fileowner()
     */
    public static function owner(string $filename)
    {
        return static::staticCall('fileowner', func_get_args());
    }

    /**
     * @param string $filename
     * @return int
     * @see fileperms()
     */
    public static function perms(string $filename)
    {
        return static::staticCall('fileperms', func_get_args());
    }

    /**
     * @param string $filename
     * @return int
     * @see filesize()
     */
    public static function size(string $filename)
    {
        return static::staticCall('filesize', func_get_args());
    }

    /**
     * @param string $filename
     * @return string
     * @see filetype()
     */
    public static function type(string $filename)
    {
        return static::staticCall('filetype', func_get_args());
    }

    /**
     * @param string $pattern
     * @param string $string
     * @param int $flags
     * @return bool
     * @see fnmatch()
     */
    public static function fnMatch(string $pattern, string $string, int $flags = 0)
    {
        return static::staticCall('fnmatch', func_get_args());
    }

    /**
     * @param string $filename
     * @param string $mode
     * @param bool|null $useIncludePath
     * @param Stream|resource|null $context
     * @return static
     * @see fopen()
     */
    public static function open(string $filename, string $mode, bool $useIncludePath = null, $context = null)
    {
        return static::initResource('fopen', func_get_args());
    }

    /**
     * @param string $hostname
     * @param int|null $port
     * @param int|null $errNo
     * @param string|null $errStr
     * @param float|null $timeout
     * @return static
     * @see fsockopen()
     */
    public static function sockOpen(string $hostname, int $port = null, int &$errNo = null, string &$errStr = null, float $timeout = null)
    {
        return static::initResource('fsockopen', func_get_args());
    }

    /**
     * @param string $pattern
     * @param int $flags
     * @return array
     * @see glob()
     */
    public static function glob(string $pattern, int $flags = 0)
    {
        return static::staticCall('glob', func_get_args());
    }

    /**
     * @param string $filename
     * @return bool
     * @see is_dir()
     */
    public static function isDir(string $filename)
    {
        return static::staticCall('is_dir', func_get_args());
    }

    /**
     * @param string $filename
     * @return bool
     * @see is_executable()
     */
    public static function isExecutable(string $filename)
    {
        return static::staticCall('is_executable', func_get_args());
    }

    /**
     * @param string $filename
     * @return bool
     * @see is_file()
     */
    public static function isFile(string $filename)
    {
        return static::staticCall('is_file', func_get_args());
    }

    /**
     * @param string $filename
     * @return bool
     * @see is_link()
     */
    public static function isLink(string $filename)
    {
        return static::staticCall('is_link', func_get_args());
    }

    /**
     * @param string $filename
     * @return bool
     * @see is_readable()
     */
    public static function isReadable(string $filename)
    {
        return static::staticCall('file_get_contents', func_get_args());
    }

    /**
     * @param string $filename
     * @return bool
     * @see is_uploaded_file()
     */
    public static function isUploaded(string $filename)
    {
        return static::staticCall('is_uploaded_file', func_get_args());
    }

    /**
     * @param string $filename
     * @return bool
     * @see is_writable()
     */
    public static function isWritable(string $filename)
    {
        return static::staticCall('is_writable', func_get_args());
    }

    /**
     * @param string $filename
     * @param string|int $group
     * @return bool
     * @see lchgrp()
     */
    public static function lchgrp(string $filename, $group)
    {
        return static::staticCall('lchgrp', func_get_args());
    }

    /**
     * @param string $filename
     * @param $user
     * @return bool
     * @see lchown()
     */
    public static function lchown(string $filename, $user)
    {
        return static::staticCall('lchown', func_get_args());
    }

    /**
     * @param string $target
     * @param string $link
     * @return bool
     * @see link()
     */
    public static function link(string $target, string $link)
    {
        return static::staticCall('link', func_get_args());
    }

    /**
     * @param string $path
     * @return int
     * @see linkinfo()
     */
    public static function linkInfo(string $path)
    {
        return static::staticCall('linkinfo', func_get_args());
    }

    /**
     * @param string $filename
     * @return array
     * @see lstat()
     */
    public static function lstat(string $filename)
    {
        return static::staticCall('lstat', func_get_args());
    }

    /**
     * @param string $pathname
     * @param int $mode
     * @param bool $recursive
     * @param Stream|resource|null $context
     * @return bool
     * @see mkdir()
     */
    public static function mkdir(string $pathname, int $mode = 0777, bool $recursive = false, $context = null)
    {
        return static::staticCall('mkdir', func_get_args());
    }

    /**
     * @param string $filename
     * @param string $destination
     * @return bool
     * @see move_uploaded_file()
     */
    public static function moveUploadedFile(string $filename, string $destination)
    {
        return static::staticCall('move_uploaded_file', func_get_args());
    }

    /**
     * @param string $path
     * @param int $options
     * @return mixed
     * @see pathinfo()
     */
    public static function pathInfo(string $path, int $options = PATHINFO_DIRNAME | PATHINFO_BASENAME | PATHINFO_EXTENSION | PATHINFO_FILENAME)
    {
        return static::staticCall('pathinfo', func_get_args());
    }

    /**
     * @param string $filename
     * @param bool $useIncludePath
     * @param Stream|resource|null $context
     * @return int
     * @see readfile()
     */
    public static function readFile(string $filename, bool $useIncludePath = false, $context = null)
    {
        return static::staticCall('readfile', func_get_args());
    }

    /**
     * @param string $path
     * @return string
     * @see readlink()
     */
    public static function readLink(string $path)
    {
        return static::staticCall('readlink', func_get_args());
    }

    /**
     * @return array
     * @see realpath_cache_get()
     */
    public static function realPathCacheGet()
    {
        return static::staticCall('realpath_cache_get', func_get_args());
    }

    /**
     * @return int
     * @see realpath_cache_size()
     */
    public static function realPathCacheSize()
    {
        return static::staticCall('realpath_cache_size', func_get_args());
    }

    /**
     * @param string $path
     * @return string
     * @see realpath()
     */
    public static function realPath(string $path)
    {
        return static::staticCall('realpath', func_get_args());
    }

    /**
     * @param string $oldName
     * @param string $newName
     * @param Stream|resource|null $context
     * @return bool
     * @see rename()
     */
    public static function rename(string $oldName, string $newName, $context = null)
    {
        return static::staticCall('rename', func_get_args());
    }

    /**
     * @param string $dirName
     * @param Stream|resource|null $context
     * @return bool
     * @see rmdir()
     */
    public static function rmdir(string $dirName, $context = null)
    {
        return static::staticCall('rmdir', func_get_args());
    }

    /**
     * @param string $filename
     * @return array
     * @see stat()
     */
    public static function statistics(string $filename)
    {
        return static::staticCall('stat', func_get_args());
    }

    /**
     * @param string $target
     * @param string $link
     * @return bool
     * @see symlink()
     */
    public static function symlink(string $target, string $link)
    {
        return static::staticCall('symlink', func_get_args());
    }

    /**
     * @param string $dir
     * @param string $prefix
     * @return string
     * @see tempnam()
     */
    public static function tempnam(string $dir, string $prefix)
    {
        return static::staticCall('tempnam', func_get_args());
    }

    /**
     * @return static
     * @see tmpfile()
     */
    public static function tmpfile()
    {
        return static::initResource('tmpfile');
    }

    /**
     * @param string $filename
     * @param int|null $time
     * @param int|null $atime
     * @return bool
     * @see touch()
     */
    public static function touch(string $filename, int $time = null, int $atime = null)
    {
        return static::staticCall('touch', func_get_args());
    }

    /**
     * @param int|null $mask
     * @return int
     * @see umask()
     */
    public static function umask(int $mask = null)
    {
        return static::staticCall('umask', func_get_args());
    }

    /**
     * @param string $filename
     * @param Stream|resource|null $context
     * @return bool
     * @see unlink()
     */
    public static function unlink(string $filename, $context = null)
    {
        return static::staticCall('unlink', func_get_args());
    }

    /**
     * @return array
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'stream',
        ];
    }

    /**
     * @return bool
     * @see fclose()
     */
    public function close(): bool
    {
        return $this->dynamicCall('fclose');
    }

    /**
     * @return bool
     * @see feof()
     */
    public function eof()
    {
        return $this->dynamicCall('feof');
    }

    /**
     * @return bool
     * @see fflush()
     */
    public function flush()
    {
        return $this->dynamicCall('fflush');
    }

    /**
     * @return string
     * @see fgetc()
     */
    public function getc()
    {
        return $this->dynamicCall('fgetc');
    }

    /**
     * @param int $length
     * @param string $delimiter
     * @param string $enclosure
     * @param string $escape
     * @return array
     * @see fgetcsv()
     */
    public function getCsv(int $length = 0, string $delimiter = ',', string $enclosure = '"', string $escape = '\\')
    {
        return $this->dynamicCall('fgetcsv', func_get_args());
    }

    /**
     * @param int|null $length
     * @return string
     * @see fgets()
     */
    public function gets(int $length = null)
    {
        return $this->dynamicCall('fgets', func_get_args());
    }

    /**
     * @param int|null $length
     * @param string|null $allowableTags
     * @return string
     * @see fgetss()
     */
    public function getss(int $length = null, string $allowableTags = null)
    {
        return $this->dynamicCall('fgetss', func_get_args());
    }

    /**
     * @param int $operation
     * @param int|null $wouldBlock
     * @return bool
     * @see flock()
     */
    public function lock(int $operation, int &$wouldBlock = null)
    {
        return $this->dynamicCall('flock', func_get_args());
    }

    /**
     * @return int
     * @see fpassthru()
     */
    public function passthru()
    {
        return $this->dynamicCall('fpassthru', func_get_args());
    }

    /**
     * @param array $fields
     * @param string $delimiter
     * @param string $enclosure
     * @param string $escapeChar
     * @return int
     * @see fputcsv()
     */
    public function putCsv(array $fields, string $delimiter = ',', string $enclosure = '"', string $escapeChar = '\\')
    {
        return $this->dynamicCall('fputcsv', func_get_args());
    }

    /**
     * Alias of {@see File::write()}
     * @param string $string
     * @param int|null $length
     * @return int
     * @see fputs()
     */
    public function puts(string $string, int $length = null)
    {
        return $this->dynamicCall('fputs', func_get_args());
    }

    /**
     * @param int $length
     * @return string
     * @see fread()
     */
    public function read(int $length)
    {
        return $this->dynamicCall('fread', func_get_args());
    }

    /**
     * @param string $format
     * @param array ...$args
     * @return mixed
     * @see fscanf()
     */
    public function scanf(string $format, &...$args)
    {
        return $this->dynamicCall('fscanf', func_get_args());
    }

    /**
     * @param int $offset
     * @param int $whence
     * @return int
     * @see fseek()
     */
    public function seek(int $offset, int $whence = SEEK_SET)
    {
        return $this->dynamicCall('fseek', func_get_args());
    }

    /**
     * @return array
     * @see fstat()
     */
    public function stat()
    {
        return $this->dynamicCall('fstat');
    }

    /**
     * @return int
     * @see ftell()
     */
    public function tell()
    {
        return $this->dynamicCall('ftell');
    }

    /**
     * @param int $size
     * @return bool
     * @see ftruncate()
     */
    public function truncate(int $size)
    {
        return $this->dynamicCall('ftruncate', func_get_args());
    }

    /**
     * @param string $string
     * @param int|null $length
     * @return int
     * @see fwrite()
     */
    public function write(string $string, int $length = null)
    {
        return $this->dynamicCall('fwrite', func_get_args());
    }

    /**
     * @return bool
     * @see rewind()
     */
    public function rewind()
    {
        return $this->dynamicCall('rewind');
    }

    /**
     * @param int $buffer
     * @return int
     * @see stream_set_write_buffer()
     */
    public function setWriteBuffer(int $buffer)
    {
        return $this->dynamicCall('stream_set_write_buffer', func_get_args());
    }
}
