<?php

namespace ResourceClass\Curl;

use ResourceWrapper\AbstractCloseableResourceWrapper;

/**
 * Class CurlMulti
 * @package ResourceClass\Curl
 * @link https://www.php.net/manual/en/book.curl.php
 */
class CurlMulti extends AbstractCloseableResourceWrapper
{
    public const CALL_MULTI_PERFORM = CURLM_CALL_MULTI_PERFORM;
    public const OK = CURLM_OK;
    public const BAD_HANDLE = CURLM_BAD_HANDLE;
    public const BAD_EASY_HANDLE = CURLM_BAD_EASY_HANDLE;
    public const OUT_OF_MEMORY = CURLM_OUT_OF_MEMORY;
    public const INTERNAL_ERROR = CURLM_INTERNAL_ERROR;

    /**
     * @param Curl $curl
     * @return string
     * @see curl_multi_getcontent()
     */
    public static function getContent(Curl $curl)
    {
	    return static::staticCall('curl_multi_get_content', func_get_args());
    }

    /**
     * @return self
     * @see curl_multi_init()
     */
    public static function init()
    {
        return static::initResource('curl_multi_init');
    }

    /**
     * @param int $errorNumber
     * @return null|string
     * @see curl_multi_strerror()
     */
    public static function strError(int $errorNumber)
    {
        return static::staticCall('curl_multi_strerror', func_get_args());
    }

    /**
     * @inheritdoc
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'curl_multi',
        ];
    }

    /**
     * @return bool
     * @see curl_multi_close()
     */
    public function close(): bool
    {
	    $this->dynamicCall('curl_multi_close');
        return true;
    }

    /**
     * @param Curl $curl
     * @return int
     * @see curl_multi_add_handle()
     */
    public function addHandle(Curl $curl)
    {
	    return $this->dynamicCall('curl_multi_add_handle', func_get_args());
    }

    /**
     * @param int $stillRunning
     * @return int
     * @see curl_multi_exec()
     */
    public function exec(int &$stillRunning)
    {
	    return $this->dynamicCall('curl_multi_exec', func_get_args());
    }

    /**
     * @param int|null $messagesInQueue
     * @return array
     * @see curl_multi_info_read()
     */
    public function infoRead(int $messagesInQueue = null)
    {
	    return $this->dynamicCall('curl_multi_info_read', func_get_args());
    }

    /**
     * @param Curl $curl
     * @return int
     * @see curl_multi_remove_handle()
     */
    public function removeHandle(Curl $curl)
    {
	    return $this->dynamicCall('curl_multi_remove_handle', func_get_args());
    }

    /**
     * @param float $timeout
     * @return int
     * @see curl_multi_select()
     */
    public function select(float $timeout = 1.0)
    {
	    return $this->dynamicCall('curl_multi_select', func_get_args());
    }

    /**
     * @param int $option
     * @param mixed $value
     * @return bool
     * @see curl_multi_setopt()
     */
    public function setOpt(int $option, $value)
    {
	    return $this->dynamicCall('curl_multi_setopt', func_get_args());
    }
}
