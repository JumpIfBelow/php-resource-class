<?php

namespace ResourceClass\Curl;

use CURLFile;
use ResourceWrapper\AbstractCloseableResourceWrapper;

/**
 * Class Curl
 * @package ResourceClass\Curl
 * @link https://www.php.net/manual/en/book.curl.php
 */
class Curl extends AbstractCloseableResourceWrapper
{
    public const OPT_AUTOREFERER = CURLOPT_AUTOREFERER;
    public const OPT_COOKIESESSION = CURLOPT_COOKIESESSION;
    public const OPT_DNS_USE_GLOBAL_CACHE = CURLOPT_DNS_USE_GLOBAL_CACHE;
    public const OPT_DNS_CACHE_TIMEOUT = CURLOPT_DNS_CACHE_TIMEOUT;
    public const OPT_FTP_SSL = CURLOPT_FTP_SSL;

    public const FTP_CREATE_DIR = CURLFTP_CREATE_DIR;
    public const FTP_CREATE_DIR_NONE = CURLFTP_CREATE_DIR_NONE;
    public const FTP_CREATE_DIR_RETRY = CURLFTP_CREATE_DIR_RETRY;

    public const FTPSSL_TRY = CURLFTPSSL_TRY;
    public const FTPSSL_ALL = CURLFTPSSL_ALL;
    public const FTPSSL_CONTROL = CURLFTPSSL_CONTROL;
    public const FTPSSL_NONE = CURLFTPSSL_NONE;

    public const OPT_PRIVATE = CURLOPT_PRIVATE;
    public const OPT_FTPSSLAUTH = CURLOPT_FTPSSLAUTH;
    public const OPT_PORT = CURLOPT_PORT;
    public const OPT_FILE = CURLOPT_FILE;
    public const OPT_INFILE = CURLOPT_INFILE;
    public const OPT_INFILESIZE = CURLOPT_INFILESIZE;
    public const OPT_URL = CURLOPT_URL;
    public const OPT_PROXY = CURLOPT_PROXY;
    public const OPT_VERBOSE = CURLOPT_VERBOSE;
    public const OPT_HEADER = CURLOPT_HEADER;
    public const OPT_HTTPHEADER = CURLOPT_HTTPHEADER;
    public const OPT_NOPROGRESS = CURLOPT_NOPROGRESS;
    public const OPT_NOBODY = CURLOPT_NOBODY;
    public const OPT_FAILONERROR = CURLOPT_FAILONERROR;
    public const OPT_UPLOAD = CURLOPT_UPLOAD;
    public const OPT_POST = CURLOPT_POST;
    public const OPT_FTPLISTONLY = CURLOPT_FTPLISTONLY;
    public const OPT_FTPAPPEND = CURLOPT_FTPAPPEND;
    public const OPT_FTP_CREATE_MISSING_DIRS = CURLOPT_FTP_CREATE_MISSING_DIRS;
    public const OPT_NETRC = CURLOPT_NETRC;
    public const OPT_FOLLOWLOCATION = CURLOPT_FOLLOWLOCATION;
    public const OPT_FTPASCII = CURLOPT_FTPASCII;
    public const OPT_PUT = CURLOPT_PUT;
    public const OPT_MUTE = CURLOPT_MUTE;
    public const OPT_USERPWD = CURLOPT_USERPWD;
    public const OPT_PROXYUSERPWD = CURLOPT_PROXYUSERPWD;
    public const OPT_RANGE = CURLOPT_RANGE;
    public const OPT_TIMEOUT = CURLOPT_TIMEOUT;
    public const OPT_TIMEOUT_MS = CURLOPT_TIMEOUT_MS;
    public const OPT_TCP_NODELAY = CURLOPT_TCP_NODELAY;
    public const OPT_POSTFIELDS = CURLOPT_POSTFIELDS;
    public const OPT_PROGRESSFUNCTION = CURLOPT_PROGRESSFUNCTION;
    public const OPT_REFERER = CURLOPT_REFERER;
    public const OPT_USERAGENT = CURLOPT_USERAGENT;
    public const OPT_FTPPORT = CURLOPT_FTPPORT;
    public const OPT_FTP_USE_EPSV = CURLOPT_FTP_USE_EPSV;
    public const OPT_LOW_SPEED_LIMIT = CURLOPT_LOW_SPEED_LIMIT;
    public const OPT_LOW_SPEED_TIME = CURLOPT_LOW_SPEED_TIME;
    public const OPT_RESUME_FROM = CURLOPT_RESUME_FROM;
    public const OPT_COOKIE = CURLOPT_COOKIE;
    public const OPT_SSLCERT = CURLOPT_SSLCERT;
    public const OPT_SSLCERTPASSWD = CURLOPT_SSLCERTPASSWD;
    public const OPT_WRITEHEADER = CURLOPT_WRITEHEADER;
    public const OPT_SSL_VERIFYHOST = CURLOPT_SSL_VERIFYHOST;
    public const OPT_COOKIEFILE = CURLOPT_COOKIEFILE;
    public const OPT_SSLVERSION = CURLOPT_SSLVERSION;

    public const SSLVERSION_DEFAULT = CURL_SSLVERSION_DEFAULT;
    public const SSLVERSION_TLSv1 = CURL_SSLVERSION_TLSv1;
    public const SSLVERSION_SSLv2 = CURL_SSLVERSION_SSLv2;
    public const SSLVERSION_SSLv3 = CURL_SSLVERSION_SSLv3;
    public const SSLVERSION_TLSv1_0 = CURL_SSLVERSION_TLSv1_0;
    public const SSLVERSION_TLSv1_1 = CURL_SSLVERSION_TLSv1_1;
    public const SSLVERSION_TLSv1_2 = CURL_SSLVERSION_TLSv1_2;

    public const OPT_TIMECONDITION = CURLOPT_TIMECONDITION;
    public const OPT_TIMEVALUE = CURLOPT_TIMEVALUE;
    public const OPT_CUSTOMREQUEST = CURLOPT_CUSTOMREQUEST;
    public const OPT_STDERR = CURLOPT_STDERR;
    public const OPT_TRANSFERTEXT = CURLOPT_TRANSFERTEXT;
    public const OPT_RETURNTRANSFER = CURLOPT_RETURNTRANSFER;
    public const OPT_QUOTE = CURLOPT_QUOTE;
    public const OPT_POSTQUOTE = CURLOPT_POSTQUOTE;
    public const OPT_INTERFACE = CURLOPT_INTERFACE;
    public const OPT_KRB4LEVEL = CURLOPT_KRB4LEVEL;
    public const OPT_HTTPPROXYTUNNEL = CURLOPT_HTTPPROXYTUNNEL;
    public const OPT_FILETIME = CURLOPT_FILETIME;
    public const OPT_WRITEFUNCTION = CURLOPT_WRITEFUNCTION;
    public const OPT_READFUNCTION = CURLOPT_READFUNCTION;
    public const OPT_PASSWDFUNCTION = CURLOPT_PASSWDFUNCTION;
    public const OPT_HEADERFUNCTION = CURLOPT_HEADERFUNCTION;
    public const OPT_MAXREDIRS = CURLOPT_MAXREDIRS;
    public const OPT_MAXCONNECTS = CURLOPT_MAXCONNECTS;
    public const OPT_CLOSEPOLICY = CURLOPT_CLOSEPOLICY;
    public const OPT_FRESH_CONNECT = CURLOPT_FRESH_CONNECT;
    public const OPT_FORBID_REUSE = CURLOPT_FORBID_REUSE;
    public const OPT_RANDOM_FILE = CURLOPT_RANDOM_FILE;
    public const OPT_EGDSOCKET = CURLOPT_EGDSOCKET;
    public const OPT_CONNECTTIMEOUT = CURLOPT_CONNECTTIMEOUT;
    public const OPT_CONNECTTIMEOUT_MS = CURLOPT_CONNECTTIMEOUT_MS;
    public const OPT_SSL_VERIFYPEER = CURLOPT_SSL_VERIFYPEER;
    public const OPT_CAINFO = CURLOPT_CAINFO;
    public const OPT_CAPATH = CURLOPT_CAPATH;
    public const OPT_COOKIEJAR = CURLOPT_COOKIEJAR;
    public const OPT_SSL_CIPHER_LIST = CURLOPT_SSL_CIPHER_LIST;
    public const OPT_BINARYTRANSFER = CURLOPT_BINARYTRANSFER;
    public const OPT_NOSIGNAL = CURLOPT_NOSIGNAL;
    public const OPT_PROXYTYPE = CURLOPT_PROXYTYPE;
    public const OPT_BUFFERSIZE = CURLOPT_BUFFERSIZE;
    public const OPT_HTTPGET = CURLOPT_HTTPGET;
    public const OPT_HTTP_VERSION = CURLOPT_HTTP_VERSION;
    public const OPT_SSLKEY = CURLOPT_SSLKEY;
    public const OPT_SSLKEYTYPE = CURLOPT_SSLKEYTYPE;
    public const OPT_SSLKEYPASSWD = CURLOPT_SSLKEYPASSWD;
    public const OPT_SSLENGINE = CURLOPT_SSLENGINE;
    public const OPT_SSLENGINE_DEFAULT = CURLOPT_SSLENGINE_DEFAULT;
    public const OPT_SSLCERTTYPE = CURLOPT_SSLCERTTYPE;
    public const OPT_CRLF = CURLOPT_CRLF;
    public const OPT_ENCODING = CURLOPT_ENCODING;
    public const OPT_PROXYPORT = CURLOPT_PROXYPORT;
    public const OPT_UNRESTRICTED_AUTH = CURLOPT_UNRESTRICTED_AUTH;
    public const OPT_FTP_USE_EPRT = CURLOPT_FTP_USE_EPRT;
    public const OPT_HTTP200ALIASES = CURLOPT_HTTP200ALIASES;
    public const OPT_HTTPAUTH = CURLOPT_HTTPAUTH;

    public const AUTH_BASIC = CURLAUTH_BASIC;
    public const AUTH_DIGEST = CURLAUTH_DIGEST;
    public const AUTH_GSSNEGOTIATE = CURLAUTH_GSSNEGOTIATE;
    public const AUTH_NEGOTIATE = CURLAUTH_NEGOTIATE;
    public const AUTH_NTLM = CURLAUTH_NTLM;
    public const AUTH_NTLM_WB = CURLAUTH_NTLM_WB;
    public const AUTH_ANY = CURLAUTH_ANY;
    public const AUTH_ANYSAFE = CURLAUTH_ANYSAFE;

    public const OPT_PROXYAUTH = CURLOPT_PROXYAUTH;
    public const OPT_MAX_RECV_SPEED_LARGE = CURLOPT_MAX_RECV_SPEED_LARGE;
    public const OPT_MAX_SEND_SPEED_LARGE = CURLOPT_MAX_SEND_SPEED_LARGE;
    public const OPT_HEADEROPT = CURLOPT_HEADEROPT;
    public const OPT_PROXYHEADER = CURLOPT_PROXYHEADER;

    public const CLOSEPOLICY_LEAST_RECENTLY_USED = CURLCLOSEPOLICY_LEAST_RECENTLY_USED;
    public const CLOSEPOLICY_LEAST_TRAFFIC = CURLCLOSEPOLICY_LEAST_TRAFFIC;
    public const CLOSEPOLICY_SLOWEST = CURLCLOSEPOLICY_SLOWEST;
    public const CLOSEPOLICY_CALLBACK = CURLCLOSEPOLICY_CALLBACK;
    public const CLOSEPOLICY_OLDEST = CURLCLOSEPOLICY_OLDEST;

    public const INFO_PRIVATE = CURLINFO_PRIVATE;
    public const INFO_EFFECTIVE_URL = CURLINFO_EFFECTIVE_URL;
    public const INFO_HTTP_CODE = CURLINFO_HTTP_CODE;
    public const INFO_RESPONSE_CODE = CURLINFO_RESPONSE_CODE;
    public const INFO_HEADER_OUT = CURLINFO_HEADER_OUT;
    public const INFO_HEADER_SIZE = CURLINFO_HEADER_SIZE;
    public const INFO_REQUEST_SIZE = CURLINFO_REQUEST_SIZE;
    public const INFO_TOTAL_TIME = CURLINFO_TOTAL_TIME;
    public const INFO_NAMELOOKUP_TIME = CURLINFO_NAMELOOKUP_TIME;
    public const INFO_CONNECT_TIME = CURLINFO_CONNECT_TIME;
    public const INFO_PRETRANSFER_TIME = CURLINFO_PRETRANSFER_TIME;
    public const INFO_SIZE_UPLOAD = CURLINFO_SIZE_UPLOAD;
    public const INFO_SIZE_DOWNLOAD = CURLINFO_SIZE_DOWNLOAD;
    public const INFO_SPEED_DOWNLOAD = CURLINFO_SPEED_DOWNLOAD;
    public const INFO_SPEED_UPLOAD = CURLINFO_SPEED_UPLOAD;
    public const INFO_FILETIME = CURLINFO_FILETIME;
    public const INFO_SSL_VERIFYRESULT = CURLINFO_SSL_VERIFYRESULT;
    public const INFO_CONTENT_LENGTH_DOWNLOAD = CURLINFO_CONTENT_LENGTH_DOWNLOAD;
    public const INFO_CONTENT_LENGTH_UPLOAD = CURLINFO_CONTENT_LENGTH_UPLOAD;
    public const INFO_STARTTRANSFER_TIME = CURLINFO_STARTTRANSFER_TIME;
    public const INFO_CONTENT_TYPE = CURLINFO_CONTENT_TYPE;
    public const INFO_REDIRECT_TIME = CURLINFO_REDIRECT_TIME;
    public const INFO_REDIRECT_COUNT = CURLINFO_REDIRECT_COUNT;
    public const INFO_REDIRECT_URL = CURLINFO_REDIRECT_URL;
    public const INFO_PRIMARY_IP = CURLINFO_PRIMARY_IP;
    public const INFO_PRIMARY_PORT = CURLINFO_PRIMARY_PORT;
    public const INFO_LOCAL_IP = CURLINFO_LOCAL_IP;
    public const INFO_LOCAL_PORT = CURLINFO_LOCAL_PORT;

    public const PUSH_OK = CURL_PUSH_OK;
    public const PUSH_DENY = CURL_PUSH_DENY;

    public const REDIR_POST_301 = CURL_REDIR_POST_301;
    public const REDIR_POST_302 = CURL_REDIR_POST_302;
    public const REDIR_POST_303 = CURL_REDIR_POST_303;
    public const REDIR_POST_ALL = CURL_REDIR_POST_ALL;

    public const TIMECOND_IFMODSINCE = CURL_TIMECOND_IFMODSINCE;
    public const TIMECOND_IFUNMODSINCE = CURL_TIMECOND_IFUNMODSINCE;
    public const TIMECOND_LASTMOD = CURL_TIMECOND_LASTMOD;

    public const VERSION_ALTSVC = CURL_VERSION_ALTSVC;
    public const VERSION_CURLDEBUG = CURL_VERSION_CURLDEBUG;
    public const VERSION_IPV6 = CURL_VERSION_IPV6;
    public const VERSION_KERBEROS4 = CURL_VERSION_KERBEROS4;
    public const VERSION_KERBEROS5 = CURL_VERSION_KERBEROS5;
    public const VERSION_HTTP2 = CURL_VERSION_HTTP2;
    public const VERSION_PSL = CURL_VERSION_PSL;
    public const VERSION_SSL = CURL_VERSION_SSL;
    public const VERSION_UNIX_SOCKETS = CURL_VERSION_UNIX_SOCKETS;
    public const VERSION_LIBZ = CURL_VERSION_LIBZ;
    public const VERSION_NOW = CURLVERSION_NOW;

    public const E_OK = CURLE_OK;
    public const E_UNSUPPORTED_PROTOCOL = CURLE_UNSUPPORTED_PROTOCOL;
    public const E_FAILED_INIT = CURLE_FAILED_INIT;
    public const E_URL_MALFORMAT = CURLE_URL_MALFORMAT;
    public const E_URL_MALFORMAT_USER = CURLE_URL_MALFORMAT_USER;
    public const E_COULDNT_RESOLVE_PROXY = CURLE_COULDNT_RESOLVE_PROXY;
    public const E_COULDNT_RESOLVE_HOST = CURLE_COULDNT_RESOLVE_HOST;
    public const E_COULDNT_CONNECT = CURLE_COULDNT_CONNECT;
    public const E_FTP_WEIRD_SERVER_REPLY = CURLE_FTP_WEIRD_SERVER_REPLY;
    public const E_FTP_ACCESS_DENIED = CURLE_FTP_ACCESS_DENIED;
    public const E_FTP_USER_PASSWORD_INCORRECT = CURLE_FTP_USER_PASSWORD_INCORRECT;
    public const E_FTP_WEIRD_PASS_REPLY = CURLE_FTP_WEIRD_PASS_REPLY;
    public const E_FTP_WEIRD_USER_REPLY = CURLE_FTP_WEIRD_USER_REPLY;
    public const E_FTP_WEIRD_PASV_REPLY = CURLE_FTP_WEIRD_PASV_REPLY;
    public const E_FTP_WEIRD_227_FORMAT = CURLE_FTP_WEIRD_227_FORMAT;
    public const E_FTP_CANT_GET_HOST = CURLE_FTP_CANT_GET_HOST;
    public const E_FTP_CANT_RECONNECT = CURLE_FTP_CANT_RECONNECT;
    public const E_FTP_COULDNT_SET_BINARY = CURLE_FTP_COULDNT_SET_BINARY;
    public const E_PARTIAL_FILE = CURLE_PARTIAL_FILE;
    public const E_FTP_COULDNT_RETR_FILE = CURLE_FTP_COULDNT_RETR_FILE;
    public const E_FTP_WRITE_ERROR = CURLE_FTP_WRITE_ERROR;
    public const E_FTP_QUOTE_ERROR = CURLE_FTP_QUOTE_ERROR;
    public const E_HTTP_NOT_FOUND = CURLE_HTTP_NOT_FOUND;
    public const E_WRITE_ERROR = CURLE_WRITE_ERROR;
    public const E_MALFORMAT_USER = CURLE_MALFORMAT_USER;
    public const E_FTP_COULDNT_STOR_FILE = CURLE_FTP_COULDNT_STOR_FILE;
    public const E_READ_ERROR = CURLE_READ_ERROR;
    public const E_OUT_OF_MEMORY = CURLE_OUT_OF_MEMORY;
    public const E_OPERATION_TIMEOUTED = CURLE_OPERATION_TIMEOUTED;
    public const E_FTP_COULDNT_SET_ASCII = CURLE_FTP_COULDNT_SET_ASCII;
    public const E_FTP_PORT_FAILED = CURLE_FTP_PORT_FAILED;
    public const E_FTP_COULDNT_USE_REST = CURLE_FTP_COULDNT_USE_REST;
    public const E_FTP_COULDNT_GET_SIZE = CURLE_FTP_COULDNT_GET_SIZE;
    public const E_HTTP_RANGE_ERROR = CURLE_HTTP_RANGE_ERROR;
    public const E_HTTP_POST_ERROR = CURLE_HTTP_POST_ERROR;
    public const E_SSL_CONNECT_ERROR = CURLE_SSL_CONNECT_ERROR;
    public const E_FTP_BAD_DOWNLOAD_RESUME = CURLE_FTP_BAD_DOWNLOAD_RESUME;
    public const E_FILE_COULDNT_READ_FILE = CURLE_FILE_COULDNT_READ_FILE;
    public const E_LDAP_CANNOT_BIND = CURLE_LDAP_CANNOT_BIND;
    public const E_LDAP_SEARCH_FAILED = CURLE_LDAP_SEARCH_FAILED;
    public const E_LIBRARY_NOT_FOUND = CURLE_LIBRARY_NOT_FOUND;
    public const E_FUNCTION_NOT_FOUND = CURLE_FUNCTION_NOT_FOUND;
    public const E_ABORTED_BY_CALLBACK = CURLE_ABORTED_BY_CALLBACK;
    public const E_BAD_FUNCTION_ARGUMENT = CURLE_BAD_FUNCTION_ARGUMENT;
    public const E_BAD_CALLING_ORDER = CURLE_BAD_CALLING_ORDER;
    public const E_HTTP_PORT_FAILED = CURLE_HTTP_PORT_FAILED;
    public const E_BAD_PASSWORD_ENTERED = CURLE_BAD_PASSWORD_ENTERED;
    public const E_TOO_MANY_REDIRECTS = CURLE_TOO_MANY_REDIRECTS;
    public const E_UNKNOWN_TELNET_OPTION = CURLE_UNKNOWN_TELNET_OPTION;
    public const E_TELNET_OPTION_SYNTAX = CURLE_TELNET_OPTION_SYNTAX;
    public const E_OBSOLETE = CURLE_OBSOLETE;
    public const E_SSL_PEER_CERTIFICATE = CURLE_SSL_PEER_CERTIFICATE;
    public const E_GOT_NOTHING = CURLE_GOT_NOTHING;
    public const E_SSL_ENGINE_NOTFOUND = CURLE_SSL_ENGINE_NOTFOUND;
    public const E_SSL_ENGINE_SETFAILED = CURLE_SSL_ENGINE_SETFAILED;
    public const E_SEND_ERROR = CURLE_SEND_ERROR;
    public const E_RECV_ERROR = CURLE_RECV_ERROR;
    public const E_SHARE_IN_USE = CURLE_SHARE_IN_USE;
    public const E_SSL_CERTPROBLEM = CURLE_SSL_CERTPROBLEM;
    public const E_SSL_CIPHER = CURLE_SSL_CIPHER;
    public const E_SSL_CACERT = CURLE_SSL_CACERT;
    public const E_BAD_CONTENT_ENCODING = CURLE_BAD_CONTENT_ENCODING;
    public const E_LDAP_INVALID_URL = CURLE_LDAP_INVALID_URL;
    public const E_FILESIZE_EXCEEDED = CURLE_FILESIZE_EXCEEDED;
    public const E_FTP_SSL_FAILED = CURLE_FTP_SSL_FAILED;
    public const E_SSH = CURLE_SSH;

    public const FTPAUTH_DEFAULT = CURLFTPAUTH_DEFAULT;
    public const FTPAUTH_SSL = CURLFTPAUTH_SSL;
    public const FTPAUTH_TLS = CURLFTPAUTH_TLS;

    public const PROXY_HTTP = CURLPROXY_HTTP;
    public const PROXY_HTTP_1_0 = CURLPROXY_HTTP_1_0;
    public const PROXY_SOCKS4 = CURLPROXY_SOCKS4;
    public const PROXY_SOCKS5 = CURLPROXY_SOCKS5;

    public const NETRC_OPTIONAL = CURL_NETRC_OPTIONAL;
    public const NETRC_IGNORED = CURL_NETRC_IGNORED;
    public const NETRC_REQUIRED = CURL_NETRC_REQUIRED;

    public const HTTP_VERSION_NONE = CURL_HTTP_VERSION_NONE;
    public const HTTP_VERSION_1_0 = CURL_HTTP_VERSION_1_0;
    public const HTTP_VERSION_1_1 = CURL_HTTP_VERSION_1_1;
    public const HTTP_VERSION_2 = CURL_HTTP_VERSION_2;
    public const HTTP_VERSION_2_0 = CURL_HTTP_VERSION_2_0;
    public const HTTP_VERSION_2TLS = CURL_HTTP_VERSION_2TLS;
    public const HTTP_VERSION_2_PRIOR_KNOWLEDGE = CURL_HTTP_VERSION_2_PRIOR_KNOWLEDGE;

    public const MSG_DONE = CURLMSG_DONE;

    public const OPT_KEYPASSWD = CURLOPT_KEYPASSWD;
    public const OPT_SSH_AUTH_TYPES = CURLOPT_SSH_AUTH_TYPES;
    public const OPT_SSH_HOST_PUBLIC_KEY_MD5 = CURLOPT_SSH_HOST_PUBLIC_KEY_MD5;
    public const OPT_SSH_PRIVATE_KEYFILE = CURLOPT_SSH_PRIVATE_KEYFILE;
    public const OPT_SSH_PUBLIC_KEYFILE = CURLOPT_SSH_PUBLIC_KEYFILE;
    public const OPT_SSL_OPTIONS = CURLOPT_SSL_OPTIONS;

    public const SSLOPT_ALLOW_BEAST = CURLSSLOPT_ALLOW_BEAST;
    public const SSLOPT_NO_REVOKE = CURLSSLOPT_NO_REVOKE;

    public const OPT_USERNAME = CURLOPT_USERNAME;
    public const OPT_SASL_IR = CURLOPT_SASL_IR;
    public const OPT_DNS_INTERFACE = CURLOPT_DNS_INTERFACE;
    public const OPT_DNS_LOCAL_IP4 = CURLOPT_DNS_LOCAL_IP4;
    public const OPT_DNS_LOCAL_IP6 = CURLOPT_DNS_LOCAL_IP6;
    public const OPT_XOAUTH2_BEARER = CURLOPT_XOAUTH2_BEARER;
    public const OPT_LOGIN_OPTIONS = CURLOPT_LOGIN_OPTIONS;
    public const OPT_EXPECT_100_TIMEOUT_MS = CURLOPT_EXPECT_100_TIMEOUT_MS;
    public const OPT_SSL_ENABLE_ALPN = CURLOPT_SSL_ENABLE_ALPN;
    public const OPT_SSL_ENABLE_NPN = CURLOPT_SSL_ENABLE_NPN;
    public const OPT_PINNEDPUBLICKEY = CURLOPT_PINNEDPUBLICKEY;
    public const OPT_UNIX_SOCKET_PATH = CURLOPT_UNIX_SOCKET_PATH;
    public const OPT_SSL_VERIFYSTATUS = CURLOPT_SSL_VERIFYSTATUS;
    public const OPT_PATH_AS_IS = CURLOPT_PATH_AS_IS;
    public const OPT_SSL_FALSESTART = CURLOPT_SSL_FALSESTART;
    public const OPT_PIPEWAIT = CURLOPT_PIPEWAIT;
    public const OPT_PROXY_SERVICE_NAME = CURLOPT_PROXY_SERVICE_NAME;
    public const OPT_SERVICE_NAME = CURLOPT_SERVICE_NAME;
    public const OPT_DEFAULT_PROTOCOL = CURLOPT_DEFAULT_PROTOCOL;
    public const OPT_STREAM_WEIGHT = CURLOPT_STREAM_WEIGHT;
    public const OPT_TFTP_NO_OPTIONS = CURLOPT_TFTP_NO_OPTIONS;
    public const OPT_CONNECT_TO = CURLOPT_CONNECT_TO;
    public const OPT_TCP_FASTOPEN = CURLOPT_TCP_FASTOPEN;

    public const MOPT_PIPELINING = CURLMOPT_PIPELINING;
    public const MOPT_MAXCONNECTS = CURLMOPT_MAXCONNECTS;
    public const MOPT_CHUNK_LENGTH_PENALTY_SIZE = CURLMOPT_CHUNK_LENGTH_PENALTY_SIZE;
    public const MOPT_CONTENT_LENGTH_PENALTY_SIZE = CURLMOPT_CONTENT_LENGTH_PENALTY_SIZE;
    public const MOPT_MAX_HOST_CONNECTIONS = CURLMOPT_MAX_HOST_CONNECTIONS;
    public const MOPT_MAX_PIPELINE_LENGTH = CURLMOPT_MAX_PIPELINE_LENGTH;
    public const MOPT_MAX_TOTAL_CONNECTIONS = CURLMOPT_MAX_TOTAL_CONNECTIONS;
    public const MOPT_PUSHFUNCTION = CURLMOPT_PUSHFUNCTION;

    public const SSH_AUTH_AGENT = CURLSSH_AUTH_AGENT;
    public const SSH_AUTH_ANY = CURLSSH_AUTH_ANY;
    public const SSH_AUTH_DEFAULT = CURLSSH_AUTH_DEFAULT;
    public const SSH_AUTH_HOST = CURLSSH_AUTH_HOST;
    public const SSH_AUTH_KEYBOARD = CURLSSH_AUTH_KEYBOARD;
    public const SSH_AUTH_NONE = CURLSSH_AUTH_NONE;
    public const SSH_AUTH_PASSWORD = CURLSSH_AUTH_PASSWORD;
    public const SSH_AUTH_PUBLICKEY = CURLSSH_AUTH_PUBLICKEY;

    public const WRAPPERS_ENABLED = CURL_WRAPPERS_ENABLED;

    public const PAUSE_ALL = CURLPAUSE_ALL;
    public const PAUSE_CONT = CURLPAUSE_CONT;
    public const PAUSE_RECV = CURLPAUSE_RECV;
    public const PAUSE_RECV_CONT = CURLPAUSE_RECV_CONT;
    public const PAUSE_SEND = CURLPAUSE_SEND;
    public const PAUSE_SEND_CONT = CURLPAUSE_SEND_CONT;

    public const PIPE_NOTHING = CURLPIPE_NOTHING;
    public const PIPE_HTTP1 = CURLPIPE_HTTP1;
    public const PIPE_MULTIPLEX = CURLPIPE_MULTIPLEX;

    public const PROXY_SOCKS4A = CURLPROXY_SOCKS4A;
    public const PROXY_SOCKS5_HOSTNAME = CURLPROXY_SOCKS5_HOSTNAME;

    public const HEADER_SEPARATE = CURLHEADER_SEPARATE;
    public const HEADER_UNIFIED = CURLHEADER_UNIFIED;

    public const PROTO_SMB = CURLPROTO_SMB;
    public const PROTO_SMBS = CURLPROTO_SMBS;

    public const OPT_REQUEST_TARGET = CURLOPT_REQUEST_TARGET;

    public const AUTH_GSSAPI = CURLAUTH_GSSAPI;
    public const E_WEIRD_SERVER_REPLY = CURLE_WEIRD_SERVER_REPLY;

    public const INFO_CONTENT_LENGTH_DOWNLOAD_T = CURLINFO_CONTENT_LENGTH_DOWNLOAD_T;
    public const INFO_CONTENT_LENGTH_UPLOAD_T = CURLINFO_CONTENT_LENGTH_UPLOAD_T;
    public const INFO_HTTP_VERSION = CURLINFO_HTTP_VERSION;
    public const INFO_PROTOCOL = CURLINFO_PROTOCOL;
    public const INFO_PROXY_SSL_VERIFYRESULT = CURLINFO_PROXY_SSL_VERIFYRESULT;
    public const INFO_SCHEME = CURLINFO_SCHEME;
    public const INFO_SIZE_DOWNLOAD_T = CURLINFO_SIZE_DOWNLOAD_T;
    public const INFO_SIZE_UPLOAD_T = CURLINFO_SIZE_UPLOAD_T;
    public const INFO_SPEED_DOWNLOAD_T = CURLINFO_SPEED_DOWNLOAD_T;
    public const INFO_SPEED_UPLOAD_T = CURLINFO_SPEED_UPLOAD_T;

    public const MAX_READ_SIZE = CURL_MAX_READ_SIZE;

    public const OPT_ABSTRACT_UNIX_SOCKET = CURLOPT_ABSTRACT_UNIX_SOCKET;
    public const OPT_KEEP_SENDING_ON_ERROR = CURLOPT_KEEP_SENDING_ON_ERROR;
    public const OPT_PRE_PROXY = CURLOPT_PRE_PROXY;
    public const OPT_PROXY_CAINFO = CURLOPT_PROXY_CAINFO;
    public const OPT_PROXY_CAPATH = CURLOPT_PROXY_CAPATH;
    public const OPT_PROXY_CRLFILE = CURLOPT_PROXY_CRLFILE;
    public const OPT_PROXY_KEYPASSWD = CURLOPT_PROXY_KEYPASSWD;
    public const OPT_PROXY_PINNEDPUBLICKEY = CURLOPT_PROXY_PINNEDPUBLICKEY;
    public const OPT_PROXY_SSLCERT = CURLOPT_PROXY_SSLCERT;
    public const OPT_PROXY_SSLCERTTYPE = CURLOPT_PROXY_SSLCERTTYPE;
    public const OPT_PROXY_SSL_CIPHER_LIST = CURLOPT_PROXY_SSL_CIPHER_LIST;
    public const OPT_PROXY_SSLKEY = CURLOPT_PROXY_SSLKEY;
    public const OPT_PROXY_SSLKEYTYPE = CURLOPT_PROXY_SSLKEYTYPE;
    public const OPT_PROXY_SSL_OPTIONS = CURLOPT_PROXY_SSL_OPTIONS;
    public const OPT_PROXY_SSL_VERIFYHOST = CURLOPT_PROXY_SSL_VERIFYHOST;
    public const OPT_PROXY_SSL_VERIFYPEER = CURLOPT_PROXY_SSL_VERIFYPEER;
    public const OPT_PROXY_SSLVERSION = CURLOPT_PROXY_SSLVERSION;
    public const OPT_PROXY_TLSAUTH_PASSWORD = CURLOPT_PROXY_TLSAUTH_PASSWORD;
    public const OPT_PROXY_TLSAUTH_TYPE = CURLOPT_PROXY_TLSAUTH_TYPE;
    public const OPT_PROXY_TLSAUTH_USERNAME = CURLOPT_PROXY_TLSAUTH_USERNAME;
    public const OPT_SOCKS5_AUTH = CURLOPT_SOCKS5_AUTH;
    public const OPT_SUPPRESS_CONNECT_HEADERS = CURLOPT_SUPPRESS_CONNECT_HEADERS;

    public const PROXY_HTTPS = CURLPROXY_HTTPS;

    public const SSLVERSION_MAX_DEFAULT = CURL_SSLVERSION_MAX_DEFAULT;
    public const SSLVERSION_MAX_NONE = CURL_SSLVERSION_MAX_NONE;
    public const SSLVERSION_MAX_TLSv1_0 = CURL_SSLVERSION_MAX_TLSv1_0;
    public const SSLVERSION_MAX_TLSv1_1 = CURL_SSLVERSION_MAX_TLSv1_1;
    public const SSLVERSION_MAX_TLSv1_2 = CURL_SSLVERSION_MAX_TLSv1_2;
    public const SSLVERSION_MAX_TLSv1_3 = CURL_SSLVERSION_MAX_TLSv1_3;
    public const SSLVERSION_TLSv1_3 = CURL_SSLVERSION_TLSv1_3;

    public const VERSION_HTTPS_PROXY = CURL_VERSION_HTTPS_PROXY;

    /**
     * @param string $filename
     * @param string|null $mimeType
     * @param string|null $postName
     * @return CURLFile
     */
    public static function fileCreate(string $filename, string $mimeType = null, string $postName = null)
    {
        return new CURLFile(...func_get_args());
    }

    /**
     * @param int $errorNumber
     * @return NULL|string
     * @see curl_strerror()
     */
    public static function strError(int $errorNumber)
    {
        return static::staticCall('curl_strerror', func_get_args());
    }

    /**
     * @param int $age
     * @return array
     * @see curl_version()
     */
    public static function version(int $age = CURLVERSION_NOW)
    {
        return static::staticCall('curl_version', func_get_args());
    }

    /**
     * @param string|null $url
     * @return static
     * @see curl_init()
     */
    public static function init(string $url = null)
    {
        return static::initResource('curl_init', func_get_args());
    }

    /**
     * @inheritdoc
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'curl',
        ];
    }

    /**
     * @return bool
     * @see curl_close()
     */
    public function close(): bool
    {
	    $this->dynamicCall('curl_close');

        return true;
    }

    /**
     * @return Curl
     * @see curl_copy_handle()
     */
    public function copyHandle()
    {
        return static::initResource('curl_copy_handle', $this->compileParameters(func_get_args()));
    }

    /**
     * @return int
     * @see curl_errno()
     */
    public function errorNumber()
    {
	    return $this->dynamicCall('curl_errno');
    }

    /**
     * @return string
     * @see curl_error()
     */
    public function errorMessage()
    {
	    return $this->dynamicCall('curl_error');
    }

    /**
     * @param string $str
     * @return bool|string
     * @see curl_escape()
     */
    public function escape(string $str)
    {
	    return $this->dynamicCall('curl_escape', func_get_args());
    }

    /**
     * @return mixed
     * @see curl_exec()
     */
    public function exec()
    {
	    return $this->dynamicCall('curl_exec');
    }

    /**
     * @param int $options
     * @return mixed
     * @see curl_getinfo()
     */
    public function getInfo(int $options)
    {
	    return $this->dynamicCall('curl_getinfo', func_get_args());
    }

    /**
     * @param int $bitMask
     * @return int
     * @see curl_pause()
     */
    public function pause(int $bitMask)
    {
	    return $this->dynamicCall('curl_pause', func_get_args());
    }

    /**
     * @return void
     * @see curl_reset()
     */
    public function reset()
    {
	    $this->dynamicCall('curl_reset');
    }

    /**
     * @param array $options
     * @return bool
     * @see curl_setopt_array()
     */
    public function setOptArray(array $options)
    {
	    return $this->dynamicCall('curl_setopt_array', func_get_args());
    }

    /**
     * @param int $option
     * @param mixed $value
     * @return bool
     * @see curl_setopt()
     */
    public function setOpt(int $option, $value)
    {
	    return $this->dynamicCall('curl_setopt', func_get_args());
    }

    /**
     * @param string $str
     * @return bool|string
     * @see curl_unescape()
     */
    public function unescape(string $str)
    {
	    return $this->dynamicCall('curl_unescape', func_get_args());
    }
}
