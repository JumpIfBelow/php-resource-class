<?php

namespace ResourceClass\Curl;

use ResourceWrapper\AbstractCloseableResourceWrapper;

/**
 * Class CurlShare
 * @package ResourceClass\Curl
 * @link https://www.php.net/manual/en/book.curl.php
 */
class CurlShare extends AbstractCloseableResourceWrapper
{
    /**
     * @return CurlShare
     * @see curl_share_init()
     */
    public static function init()
    {
        return static::initResource('curl_share_init');
    }

    /**
     * @inheritdoc
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'curl_share',
        ];
    }

    /**
     * @return bool
     * @see curl_share_close()
     */
    public function close(): bool
    {
	    $this->dynamicCall('curl_share_close');
        return true;
    }

    /**
     * @param int $option
     * @param string $value
     * @return bool
     * @see curl_share_setopt()
     */
    public function setOpt(int $option, string $value)
    {
	    return $this->dynamicCall('curl_share_setopt', func_get_args());
    }
}
