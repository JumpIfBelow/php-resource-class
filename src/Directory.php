<?php

namespace ResourceClass;

use ResourceWrapper\AbstractCloseableResourceWrapper;

/**
 * Class Directory
 * @package ResourceClass
 * @link https://www.php.net/manual/en/book.dir.php
 */
class Directory extends AbstractCloseableResourceWrapper
{
    public const DIRECTORY_SEPARATOR = DIRECTORY_SEPARATOR;
    public const PATH_SEPARATOR = PATH_SEPARATOR;

    public const SCANDIR_SORT_ASCENDING = SCANDIR_SORT_ASCENDING;
    public const SCANDIR_SORT_DESCENDING = SCANDIR_SORT_DESCENDING;
    public const SCANDIR_SORT_NONE = SCANDIR_SORT_NONE;

    /**
     * @return array
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'stream',
        ];
    }

    /**
     * @param string $directory
     * @return bool
     * @see chdir()
     */
    public static function change(string $directory)
    {
        return static::staticCall('chdir', func_get_args());
    }

    /**
     * @param string $directory
     * @return bool
     * @see chroot()
     */
    public static function changeRoot(string $directory)
    {
        return static::staticCall('chroot', func_get_args());
    }

    /**
     * @return string
     * @see getcwd()
     */
    public static function getCurrentWorkingDirectory()
    {
        return static::staticCall('getcwd');
    }

    /**
     * @param string $path
     * @param Stream|resource|null $context
     * @return static
     * @see opendir()
     */
    public static function open(string $path, $context = null)
    {
        return static::initResource('opendir', func_get_args());
    }

    /**
     * @param string $directory
     * @param int $sortingOrder
     * @param Stream|resource|null $context
     * @return array
     * @see scandir()
     */
    public static function scan(string $directory, int $sortingOrder = SCANDIR_SORT_ASCENDING, $context = null)
    {
        return static::staticCall('scandir', func_get_args());
    }

    /**
     * @return bool
     * @see closedir()
     */
    public function close(): bool
    {
        $this->dynamicCall('closedir');

        return true;
    }

    /**
     * @return string
     * @see readdir()
     */
    public function read()
    {
        return $this->dynamicCall('readdir');
    }

    /**
     * @return void
     * @see rewinddir()
     */
    public function rewind()
    {
        $this->dynamicCall('rewinddir');
    }
}
