<?php

namespace ResourceClass\Ssh2;

use ResourceWrapper\AbstractResourceWrapper;

/**
 * Class Ssh2Sftp
 * @package ResourceClass\Ssh2
 * @link https://www.php.net/manual/en/book.ssh2.php
 */
class Ssh2Sftp extends AbstractResourceWrapper
{
    /**
     * @inheritdoc
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'SSH2 SFTP',
        ];
    }

    /**
     * @param string $filename
     * @param int $mode
     * @return bool
     * @see ssh2_sftp_chmod()
     */
    public function chmod(string $filename, int $mode)
    {
	    return $this->dynamicCall('ssh2_sftp_chmod', func_get_args());
    }

    /**
     * @param string $path
     * @return array
     * @see ssh2_sftp_lstat()
     */
    public function lstat(string $path)
    {
	    return $this->dynamicCall('ssh2_sftp_lstat', func_get_args());
    }

    /**
     * @param string $directoryName
     * @param int $mode
     * @param bool $recursive
     * @return bool
     * @see ssh2_sftp_mkdir()
     */
    public function mkdir(string $directoryName, int $mode = 0777, bool $recursive = false)
    {
	    return $this->dynamicCall('ssh2_sftp_mkdir', func_get_args());
    }

    /**
     * @param string $link
     * @return string
     * @see ssh2_sftp_readlink()
     */
    public function readLink(string $link)
    {
	    return $this->dynamicCall('ssh2_sftp_readlink', func_get_args());
    }

    /**
     * @param string $filename
     * @return string
     * @see ssh2_sftp_realpath()
     */
    public function realPath(string $filename)
    {
	    return $this->dynamicCall('ssh2_sftp_realpath', func_get_args());
    }

    /**
     * @param string $from
     * @param string $to
     * @return bool
     * @see ssh2_sftp_rename()
     */
    public function rename(string $from, string $to)
    {
	    return $this->dynamicCall('ssh2_sftp_rename', func_get_args());
    }

    /**
     * @param string $directoryName
     * @return bool
     * @see ssh2_sftp_rmdir()
     */
    public function rmdir(string $directoryName)
    {
	    return $this->dynamicCall('ssh2_sftp_rmdir', func_get_args());
    }

    /**
     * @param string $path
     * @return array
     * @see ssh2_sftp_stat()
     */
    public function stat(string $path)
    {
	    return $this->dynamicCall('ssh2_sftp_stat', func_get_args());
    }

    /**
     * @param string $target
     * @param string $link
     * @return bool
     * @see ssh2_sftp_symlink()
     */
    public function symlink(string $target, string $link)
    {
	    return $this->dynamicCall('ssh2_sftp_symlink', func_get_args());
    }

    /**
     * @param string $filename
     * @return bool
     * @see ssh2_sftp_unlink()
     */
    public function unlink(string $filename)
    {
	    return $this->dynamicCall('ssh2_sftp_unlink', func_get_args());
    }
}
