<?php

namespace ResourceClass\Ssh2;

use ResourceWrapper\AbstractCloseableResourceWrapper;
use ResourceClass\File;
use ResourceClass\Stream;

/**
 * Class Ssh2
 * @package ResourceClass\Ssh2
 * @link https://www.php.net/manual/en/book.ssh2.php
 */
class Ssh2 extends AbstractCloseableResourceWrapper
{
    public const FINGERPRINT_MD5 = SSH2_FINGERPRINT_MD5;
    public const FINGERPRINT_SHA1 = SSH2_FINGERPRINT_SHA1;
    public const FINGERPRINT_HEX = SSH2_FINGERPRINT_HEX;
    public const FINGERPRINT_RAW = SSH2_FINGERPRINT_RAW;

    public const TERM_UNIT_CHARS = SSH2_TERM_UNIT_CHARS;
    public const TERM_UNIT_PIXELS = SSH2_TERM_UNIT_PIXELS;

    public const DEFAULT_TERM_WIDTH = SSH2_DEFAULT_TERM_WIDTH;
    public const DEFAULT_TERM_HEIGHT = SSH2_DEFAULT_TERM_HEIGHT;
    public const DEFAULT_TERM_UNIT = SSH2_DEFAULT_TERM_UNIT;

    public const STREAM_STDIO = SSH2_STREAM_STDIO;
    public const STREAM_STDERR = SSH2_STREAM_STDERR;

    public const DEFAULT_TERMINAL = SSH2_DEFAULT_TERMINAL;

    /**
     * @param string $host
     * @param int $port
     * @param array $methods
     * @param array $callbacks
     * @return static
     * @see ssh2_connect()
     */
    public static function connect(string $host, int $port = 22, array $methods = null, array $callbacks = null)
    {
        return static::initResource('ssh2_connect', func_get_args());
    }

    /**
     * @inheritdoc
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'SSH2 Session',
        ];
    }

    /**
     * @param string $username
     * @return bool
     * @see ssh2_auth_agent()
     */
    public function authenticationAgent(string $username)
    {
	    return $this->dynamicCall('ssh2_auth_agent', func_get_args());
    }

    /**
     * @param string $username
     * @param string $hostname
     * @param string $publicKeyFile
     * @param string $privateKeyFile
     * @param string|null $passPhrase
     * @param string|null $localUsername
     * @return bool
     * @see ssh2_auth_hostbased_file()
     */
    public function authenticationHostBasedFile(string $username, string $hostname, string $publicKeyFile, string $privateKeyFile, string $passPhrase = null, string $localUsername = null)
    {
	    return $this->dynamicCall('ssh2_auth_hostbased_file', func_get_args());
    }

    /**
     * @param string $username
     * @return mixed
     * @see ssh2_auth_none()
     */
    public function authenticationNone(string $username)
    {
	    return $this->dynamicCall('ssh2_auth_none', func_get_args());
    }

    /**
     * @param string $username
     * @param string $password
     * @return bool
     * @see ssh2_auth_password()
     */
    public function authenticationPassword(string $username, string $password)
    {
	    return $this->dynamicCall('ssh2_auth_password', func_get_args());
    }

    /**
     * @param string $username
     * @param string $publicKeyFile
     * @param string $privateKeyFile
     * @param string|null $passPhrase
     * @return bool
     * @see ssh2_auth_pubkey_file()
     */
    public function authenticationPublicKeyFile(string $username, string $publicKeyFile, string $privateKeyFile, string $passPhrase = null)
    {
	    return $this->dynamicCall('ssh2_auth_pubkey_file', func_get_args());
    }

    /**
     * @return bool
     */
    public function close(): bool
    {
        $result = $this->exec('exit');
        return $result->isResource();
    }

    /**
     * @param string $command
     * @param string|null $pty
     * @param array|null $env
     * @param int $width
     * @param int $height
     * @param int $widthHeightType
     * @return Stream
     * @see ssh2_exec()
     */
    public function exec(string $command, string $pty = null, array $env = null, int $width = 80, int $height = 25, int $widthHeightType = SSH2_TERM_UNIT_CHARS)
    {
        return Stream::initResource('ssh2_exec', $this->compileParameters(func_get_args()));
    }

    /**
     * @param int $streamId
     * @return File
     * @see ssh2_fetch_stream()
     */
    public function fetchStream(int $streamId)
    {
        return File::initResource('ssh2_fetch_stream', ($this->compileParameters(func_get_args())));
    }

    /**
     * @param int $flags
     * @return string
     * @see ssh2_fingerprint()
     */
    public function fingerprint(int $flags = SSH2_FINGERPRINT_MD5 | SSH2_FINGERPRINT_HEX)
    {
	    return $this->dynamicCall('ssh2_fingerprint', func_get_args());
    }

    /**
     * @return array
     * @see ssh2_methods_negotiated()
     */
    public function methodsNegotiated()
    {
	    return $this->dynamicCall('ssh2_methods_negotiated');
    }

    /**
     * @return Ssh2PublicKey
     * @see ssh2_publickey_init()
     */
    public function publicKeyInit()
    {
        return Ssh2PublicKey::initResource('ssh2_publickey_init', $this->compileParameters());
    }

    /**
     * @param string $remoteFile
     * @param string $localFile
     * @return bool
     * @see ssh2_scp_recv()
     */
    public function scpReceive(string $remoteFile, string $localFile)
    {
	    return $this->dynamicCall('ssh2_scp_recv', func_get_args());
    }

    /**
     * @param string $localFile
     * @param string $remoteFile
     * @param int $mode
     * @return bool
     * @see ssh2_scp_send()
     */
    public function scpSend(string $localFile, string $remoteFile, int $mode = 0644)
    {
	    return $this->dynamicCall('ssh2_scp_send', func_get_args());
    }

    /**
     * @return Ssh2Sftp
     * @see ssh2_sftp()
     */
    public function sftp()
    {
        return Ssh2Sftp::initResource('ssh2_sftp', $this->compileParameters());
    }

    /**
     * @param string $termType
     * @param array|null $env
     * @param int $width
     * @param int $height
     * @param int $widthHeightType
     * @return File
     * @see ssh2_shell()
     */
    public function shell(string $termType = 'vanilla', array $env = null, int $width = 80, int $height = 25, int $widthHeightType = SSH2_TERM_UNIT_CHARS)
    {
        return File::initResource('ssh2_shell', $this->compileParameters(func_get_args()));
    }

    /**
     * @param string $host
     * @param int $port
     * @return File
     * @see ssh2_tunnel()
     */
    public function tunnel(string $host, int $port)
    {
        return File::initResource('ssh2_tunnel', $this->compileParameters(func_get_args()));
    }
}
