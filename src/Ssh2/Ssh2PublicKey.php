<?php

namespace ResourceClass\Ssh2;

use ResourceWrapper\AbstractResourceWrapper;

/**
 * Class Ssh2PublicKey
 * @package ResourceClass\Ssh2
 * @link https://www.php.net/manual/en/book.ssh2.php
 */
class Ssh2PublicKey extends AbstractResourceWrapper
{
    /**
     * @inheritdoc
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'SSH2 Publickey Subsystem',
        ];
    }

    /**
     * @param string $algorithmName
     * @param string $blob
     * @param bool $overwrite
     * @param array|null $attributes
     * @return bool
     * @see ssh2_publickey_add()
     */
    public function add(string $algorithmName, string $blob, bool $overwrite = false, array $attributes = null)
    {
	    return $this->dynamicCall('ssh2_publickey_add', func_get_args());
    }

    /**
     * @return array
     * @see ssh2_publickey_list()
     */
    public function list()
    {
	    return $this->dynamicCall('ssh2_publickey_list');
    }

    /**
     * @param string $algorithmName
     * @param string $blob
     * @return bool
     * @see ssh2_publickey_remove()
     */
    public function remove(string $algorithmName, string $blob)
    {
	    return $this->dynamicCall('ssh2_publickey_remove', func_get_args());
    }
}
