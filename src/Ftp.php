<?php

namespace ResourceClass;

use ResourceWrapper\AbstractCloseableResourceWrapper;

/**
 * Class Ftp
 * @package ResourceClass\Resource
 * @link https://www.php.net/manual/en/book.ftp.php
 */
class Ftp extends AbstractCloseableResourceWrapper
{
    public const ASCII = FTP_ASCII;
    public const AUTOSEEK = FTP_AUTOSEEK;
    public const AUTORESUME = FTP_AUTORESUME;
    public const FAILED = FTP_FAILED;
    public const FINISHED = FTP_FINISHED;
    public const MOREDATA = FTP_MOREDATA;
    public const TEXT = FTP_TEXT;
    public const BINARY = FTP_BINARY;
    public const IMAGE = FTP_IMAGE;
    public const TIMEOUT_SEC = FTP_TIMEOUT_SEC;
    public const USEPASVADDRESS = FTP_USEPASVADDRESS;

    /**
     * @param string $host
     * @param int $port
     * @param int $timeout
     * @return static
     * @see ftp_connect()
     */
    public static function connect(string $host, int $port = 21, int $timeout = 90)
    {
        return static::initResource('ftp_connect', func_get_args());
    }

    /**
     * @param string $host
     * @param int $port
     * @param int $timeout
     * @return static
     * @see ftp_ssl_connect()
     */
    public static function sslConnect(string $host, int $port = 21, int $timeout = 90)
    {
        return static::initResource('ftp_ssl_connect', func_get_args());
    }

    /**
     * @inheritdoc
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'FTP Buffer',
        ];
    }

    /**
     * @return bool
     * @see ftp_close()
     */
    public function close(): bool
    {
	    return $this->dynamicCall('ftp_close');
    }

    /**
     * @param int $fileSize
     * @param string|null $result
     * @return bool
     * @see ftp_alloc()
     */
    public function alloc(int $fileSize, string &$result = null)
    {
	    return $this->dynamicCall('ftp_alloc', func_get_args());
    }

    /**
     * @return bool
     * @see ftp_cdup()
     */
    public function cdup()
    {
	    return $this->dynamicCall('ftp_cdup');
    }

    /**
     * @param string $directory
     * @return bool
     * @see ftp_chdir()
     */
    public function chdir(string $directory)
    {
	    return $this->dynamicCall('ftp_chdir', func_get_args());
    }

    /**
     * @param int $mode
     * @param string $filename
     * @return int
     * @see ftp_chmod()
     */
    public function chmod(int $mode, string $filename)
    {
	    return $this->dynamicCall('ftp_chmod', func_get_args());
    }

    /**
     * @param string $path
     * @return bool
     * @see ftp_delete()
     */
    public function delete(string $path)
    {
	    return $this->dynamicCall('ftp_delete', func_get_args());
    }

    /**
     * @param string $command
     * @return bool
     * @see ftp_exec()
     */
    public function exec(string $command)
    {
	    return $this->dynamicCall('ftp_exec', func_get_args());
    }

    /**
     * @param File|resource $file
     * @param string $remoteFile
     * @param int $mode
     * @param int $resumePos
     * @return bool
     * @see ftp_fget()
     */
    public function fget($file, string $remoteFile, int $mode, int $resumePos = 0)
    {
	    return $this->dynamicCall('ftp_fget', func_get_args());
    }

    /**
     * @param string $remoteFile
     * @param File|resource $file
     * @param int $mode
     * @param int $startPos
     * @return bool
     * @see ftp_fput()
     */
    public function fput(string $remoteFile, $file, int $mode, int $startPos = 0)
    {
	    return $this->dynamicCall('ftp_fput', func_get_args());
    }

    /**
     * @param int $option
     * @return mixed
     * @see ftp_get_option()
     */
    public function getOption(int $option)
    {
	    return $this->dynamicCall('ftp_get_option', func_get_args());
    }

    /**
     * @param string $localFile
     * @param string $remoteFile
     * @param int $mode
     * @param int $resumePos
     * @return bool
     * @see ftp_get()
     */
    public function get(string $localFile, string $remoteFile, int $mode, int $resumePos = 0)
    {
	    return $this->dynamicCall('ftp_get', func_get_args());
    }

    /**
     * @param string $username
     * @param string $password
     * @return bool
     * @see ftp_login()
     */
    public function login(string $username, string $password)
    {
	    return $this->dynamicCall('ftp_login', func_get_args());
    }

    /**
     * @param string $remoteFile
     * @return int
     * @see ftp_mdtm()
     */
    public function mdtm(string $remoteFile)
    {
	    return $this->dynamicCall('ftp_mdtm', func_get_args());
    }

    /**
     * @param string $directory
     * @return string
     * @see ftp_mkdir()
     */
    public function mkdir(string $directory)
    {
	    return $this->dynamicCall('ftp_mkdir', func_get_args());
    }

    /**
     * @return int
     * @see ftp_nb_continue()
     */
    public function nbContinue()
    {
	    return $this->dynamicCall('ftp_nb_continue');
    }

    /**
     * @param File|resource $file
     * @param string $remoteFile
     * @param int $mode
     * @param int $resumePos
     * @return int
     * @see ftp_nb_fget()
     */
    public function nbFget($file, string $remoteFile, int $mode, int $resumePos = 0)
    {
	    return $this->dynamicCall('ftp_nb_get', func_get_args());
    }

    /**
     * @param string $remoteFile
     * @param File $file
     * @param int $mode
     * @param int $startPos
     * @return int
     * @see ftp_nb_fput()
     */
    public function nbFput(string $remoteFile, File $file, int $mode, int $startPos = 0)
    {
	    return $this->dynamicCall('ftp_nb_fput', func_get_args());
    }

    /**
     * @param string $localFile
     * @param string $remoteFile
     * @param int $mode
     * @param int $resumePos
     * @return int
     * @see ftp_nb_get()
     */
    public function nbGet(string $localFile, string $remoteFile, int $mode, int $resumePos = 0)
    {
	    return $this->dynamicCall('ftp_nb_get', func_get_args());
    }

    /**
     * @param string $remoteFile
     * @param string $localFile
     * @param int $mode
     * @param int $startPos
     * @return int
     * @see ftp_nb_put()
     */
    public function nbPut(string $remoteFile, string $localFile, int $mode, int $startPos = 0)
    {
	    return $this->dynamicCall('ftp_nb_put', func_get_args());
    }

    /**
     * @param string $directory
     * @return array
     * @see ftp_nlist()
     */
    public function nlist(string $directory)
    {
	    return $this->dynamicCall('ftp_nlist', func_get_args());
    }

    /**
     * @param bool $pasv
     * @return bool
     * @see ftp_pasv()
     */
    public function pasv(bool $pasv)
    {
	    return $this->dynamicCall('ftp_pasv', func_get_args());
    }

    /**
     * @param string $remoteFile
     * @param string $localFile
     * @param int $mode
     * @param int $startPos
     * @return bool
     * @see ftp_put()
     */
    public function put(string $remoteFile, string $localFile, int $mode, int $startPos = 0)
    {
	    return $this->dynamicCall('ftp_put', func_get_args());
    }

    /**
     * @return string
     * @see ftp_pwd()
     */
    public function pwd()
    {
	    return $this->dynamicCall('ftp_pwd');
    }

    /**
     * @param string $command
     * @return array
     * @see ftp_raw()
     */
    public function raw(string $command)
    {
	    return $this->dynamicCall('ftp_raw', func_get_args());
    }

    /**
     * @param string $directory
     * @param bool $recursive
     * @return array
     * @see ftp_rawlist()
     */
    public function rawList(string $directory, bool $recursive = false)
    {
	    return $this->dynamicCall('ftp_rawlist', func_get_args());
    }

    /**
     * @param string $oldName
     * @param string $newName
     * @return bool
     * @see ftp_rename()
     */
    public function rename(string $oldName, string $newName)
    {
	    return $this->dynamicCall('ftp_rename', func_get_args());
    }

    /**
     * @param string $directory
     * @return bool
     * @see rmdir()
     */
    public function rmdir(string $directory)
    {
	    return $this->dynamicCall('ftp_rmdir', func_get_args());
    }

    /**
     * @param int $option
     * @param mixed $value
     * @return bool
     * @see ftp_set_option()
     */
    public function setOption(int $option, $value)
    {
	    return $this->dynamicCall('ftp_set_option', func_get_args());
    }

    /**
     * @param string $command
     * @return bool
     * @see ftp_site()
     */
    public function site(string $command)
    {
	    return $this->dynamicCall('ftp_site', func_get_args());
    }

    /**
     * @param string $remoteFile
     * @return int
     * @see ftp_size()
     */
    public function size(string $remoteFile)
    {
	    return $this->dynamicCall('ftp_size', func_get_args());
    }

    /**
     * @return string
     * @see ftp_systype()
     */
    public function sysType()
    {
	    return $this->dynamicCall('ftp_systype');
    }
}
