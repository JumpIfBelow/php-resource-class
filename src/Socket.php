<?php

namespace ResourceClass;

use ResourceWrapper\AbstractCloseableResourceWrapper;

/**
 * Class Socket
 * @package ResourceClass
 * @link https://www.php.net/manual/en/book.sockets.php
 */
class Socket extends AbstractCloseableResourceWrapper
{
    public const AF_UNIX = AF_UNIX;
    public const AF_INET = AF_INET;
    public const AF_INET6 = AF_INET6;

    public const SOCK_STREAM = SOCK_STREAM;
    public const SOCK_DGRAM = SOCK_DGRAM;
    public const SOCK_RAW = SOCK_RAW;
    public const SOCK_SEQPACKET = SOCK_SEQPACKET;
    public const SOCK_RDM = SOCK_RDM;

    public const MSG_OOB = MSG_OOB;
    public const MSG_WAITALL = MSG_WAITALL;
    public const MSG_PEEK = MSG_PEEK;
    public const MSG_DONTROUTE = MSG_DONTROUTE;
    public const MSG_EOR = MSG_EOR;
    public const MSG_EOF = MSG_EOF;

    public const SO_DEBUG = SO_DEBUG;
    public const SO_REUSEADDR = SO_REUSEADDR;
    public const SO_REUSEPORT = SO_REUSEPORT;
    public const SO_KEEPALIVE = SO_KEEPALIVE;
    public const SO_DONTROUTE = SO_DONTROUTE;
    public const SO_LINGER = SO_LINGER;
    public const SO_BROADCAST = SO_BROADCAST;
    public const SO_OOBINLINE = SO_OOBINLINE;
    public const SO_SNDBUF = SO_SNDBUF;
    public const SO_RCVBUF = SO_RCVBUF;
    public const SO_SNDLOWAT = SO_SNDLOWAT;
    public const SO_RCVLOWAT = SO_RCVLOWAT;
    public const SO_SNDTIMEO = SO_SNDTIMEO;
    public const SO_RCVTIMEO = SO_RCVTIMEO;
    public const SO_TYPE = SO_TYPE;
    public const SO_ERROR = SO_ERROR;

    public const TCP_NODELAY = TCP_NODELAY;

    public const PHP_NORMAL_READ = PHP_NORMAL_READ;
    public const PHP_BINARY_READ = PHP_BINARY_READ;

    public const SOL_SOCKET = SOL_SOCKET;
    public const SOL_TCP = SOL_TCP;
    public const SOL_UDP = SOL_UDP;

    public const EINTR = SOCKET_EINTR;
    public const EBADF = SOCKET_EBADF;
    public const EACCES = SOCKET_EACCES;
    public const EFAULT = SOCKET_EFAULT;
    public const EINVAL = SOCKET_EINVAL;
    public const EMFILE = SOCKET_EMFILE;
    public const ENAMETOOLONG = SOCKET_ENAMETOOLONG;
    public const ENOTEMPTY = SOCKET_ENOTEMPTY;
    public const ELOOP = SOCKET_ELOOP;
    public const EWOULDBLOCK = SOCKET_EWOULDBLOCK;
    public const EREMOTE = SOCKET_EREMOTE;
    public const EUSERS = SOCKET_EUSERS;
    public const ENOTSOCK = SOCKET_ENOTSOCK;
    public const EDESTADDRREQ = SOCKET_EDESTADDRREQ;
    public const EMSGSIZE = SOCKET_EMSGSIZE;
    public const EPROTOTYPE = SOCKET_EPROTOTYPE;
    public const EPROTONOSUPPORT = SOCKET_EPROTONOSUPPORT;
    public const ESOCKTNOSUPPORT = SOCKET_ESOCKTNOSUPPORT;
    public const EOPNOTSUPP = SOCKET_EOPNOTSUPP;
    public const EPFNOSUPPORT = SOCKET_EPFNOSUPPORT;
    public const EAFNOSUPPORT = SOCKET_EAFNOSUPPORT;
    public const EADDRNOTAVAIL = SOCKET_EADDRNOTAVAIL;
    public const ENETDOWN = SOCKET_ENETDOWN;
    public const ENETUNREACH = SOCKET_ENETUNREACH;
    public const ENETRESET = SOCKET_ENETRESET;
    public const ECONNABORTED = SOCKET_ECONNABORTED;
    public const ECONNRESET = SOCKET_ECONNRESET;
    public const ENOBUFS = SOCKET_ENOBUFS;
    public const EISCONN = SOCKET_EISCONN;
    public const ENOTCONN = SOCKET_ENOTCONN;
    public const ESHUTDOWN = SOCKET_ESHUTDOWN;
    public const ETIMEDOUT = SOCKET_ETIMEDOUT;
    public const ECONNREFUSED = SOCKET_ECONNREFUSED;
    public const EHOSTDOWN = SOCKET_EHOSTDOWN;
    public const EHOSTUNREACH = SOCKET_EHOSTUNREACH;
    public const EALREADY = SOCKET_EALREADY;
    public const EINPROGRESS = SOCKET_EINPROGRESS;
    public const ENOPROTOOPT = SOCKET_ENOPROTOOPT;
    public const EADDRINUSE = SOCKET_EADDRINUSE;
    public const ETOOMYREFS = SOCKET_ETOOMYREFS;
    public const EPROCLIM = SOCKET_EPROCLIM;
    public const EDUOT = SOCKET_EDUOT;
    public const ESTALE = SOCKET_ESTALE;
    public const EDISCON = SOCKET_EDISCON;
    public const SYSNOTREADY = SOCKET_SYSNOTREADY;
    public const VERNOTSUPPORTED = SOCKET_VERNOTSUPPORTED;
    public const NOTINITIALISED = SOCKET_NOTINITIALISED;
    public const HOST_NOT_FOUND = SOCKET_HOST_NOT_FOUND;
    public const TRY_AGAIN = SOCKET_TRY_AGAIN;
    public const NO_RECOVERY = SOCKET_NO_RECOVERY;
    public const NO_DATA = SOCKET_NO_DATA;
    public const NO_ADDRESS = SOCKET_NO_ADDRESS;
    public const EPERM = SOCKET_EPERM;
    public const ENOENT = SOCKET_ENOENT;
    public const EIO = SOCKET_EIO;
    public const ENXIO = SOCKET_ENXIO;
    public const E2BIG = SOCKET_E2BIG;
    public const EAGAIN = SOCKET_EAGAIN;
    public const ENOMEM = SOCKET_ENOMEM;
    public const ENOTBLK = SOCKET_ENOTBLK;
    public const EBUSY = SOCKET_EBUSY;
    public const EEXIST = SOCKET_EEXIST;
    public const EXDEV = SOCKET_EXDEV;
    public const ENODEV = SOCKET_ENODEV;
    public const ENOTDIR = SOCKET_ENOTDIR;
    public const EISDIR = SOCKET_EISDIR;
    public const ENFILE = SOCKET_ENFILE;
    public const ENOTTY = SOCKET_ENOTTY;
    public const ENOSPC = SOCKET_ENOSPC;
    public const ESPIPE = SOCKET_ESPIPE;
    public const EROFS = SOCKET_EROFS;
    public const EMLINK = SOCKET_EMLINK;
    public const EPIPE = SOCKET_EPIPE;
    public const ENOLCK = SOCKET_ENOLCK;
    public const ENOSYS = SOCKET_ENOSYS;
    public const ENOMSG = SOCKET_ENOMSG;
    public const EIDRM = SOCKET_EIDRM;
    public const ECHRNG = SOCKET_ECHRNG;
    public const EL2NSYNC = SOCKET_EL2NSYNC;
    public const EL3HLT = SOCKET_EL3HLT;
    public const EL3RST = SOCKET_EL3RST;
    public const ELNRNG = SOCKET_ELNRNG;
    public const EUNATCH = SOCKET_EUNATCH;
    public const ENOCSI = SOCKET_ENOCSI;
    public const EL2HLT = SOCKET_EL2HLT;
    public const EBADE = SOCKET_EBADE;
    public const EBADR = SOCKET_EBADR;
    public const EXFULL = SOCKET_EXFULL;
    public const ENOANO = SOCKET_ENOANO;
    public const EBADRQC = SOCKET_EBADRQC;
    public const EBADSLT = SOCKET_EBADSLT;
    public const ENOSTR = SOCKET_ENOSTR;
    public const ENODATA = SOCKET_ENODATA;
    public const ETIME = SOCKET_ETIME;
    public const ENOSR = SOCKET_ENOSR;
    public const ENONET = SOCKET_ENONET;
    public const ENOLINK = SOCKET_ENOLINK;
    public const EADV = SOCKET_EADV;
    public const ESRMNT = SOCKET_ESRMNT;
    public const ECOMM = SOCKET_ECOMM;
    public const EPROTO = SOCKET_EPROTO;
    public const EMULTIHOP = SOCKET_EMULTIHOP;
    public const EBADMSG = SOCKET_EBADMSG;
    public const ENOTUNIQ = SOCKET_ENOTUNIQ;
    public const EBADFD = SOCKET_EBADFD;
    public const EREMCHG = SOCKET_EREMCHG;
    public const ERESTART = SOCKET_ERESTART;
    public const ESTRPIPE = SOCKET_ESTRPIPE;
    public const EPROTOOPT = SOCKET_EPROTOOPT;
    public const ADDRINUSE = SOCKET_ADDRINUSE;
    public const ETOOMANYREFS = SOCKET_ETOOMANYREFS;
    public const EISNAM = SOCKET_EISNAM;
    public const EREMOTEIO = SOCKET_EREMOTEIO;
    public const EDQUOT = SOCKET_EDQUOT;
    public const ENOMEDIUM = SOCKET_ENOMEDIUM;
    public const EMEDIUMTYPE = SOCKET_EMEDIUMTYPE;

	/**
	 * @param int $domain
	 * @param int $type
	 * @param int $protocol
	 * @return static
	 * @see socket_create()
	 */
	public static function create(int $domain, int $type, int $protocol)
	{
		return static::initResource('socket_create', func_get_args());
	}

	/**
	 * @return void
	 * @see socket_clear_error()
	 */
	public static function globalClearError()
	{
		static::staticCall('socket_clear_error');
	}

	/**
	 * @param int $level
	 * @param int $type
	 * @return int
	 * @see socket_cmsg_space()
	 */
	public static function calculateMessageSpace(int $level, int $type)
	{
		return static::staticCall('socket_cmsg_space', func_get_args());
	}

	/**
	 * @param int $port
	 * @param int $backlog
	 * @return static
	 * @see socket_create_listen()
	 */
	public static function createListen(int $port, int $backlog = 128)
	{
		return static::initResource('socket_create_listen', func_get_args());
	}

	/**
	 * @param int $domain
	 * @param int $type
	 * @param int $protocol
	 * @param static[] $sockets
	 * @return bool
	 * @see socket_create_pair()
	 */
	public static function createPair(int $domain, int $type, int $protocol, array &$sockets)
	{
		$sockets = [];
		$result = static::staticCall('socket_create_pair', func_get_args());

		foreach ($sockets as $socket) {
			$sockets[] = new self($socket);
		}

		return $result;
	}

	/**
	 * @inheritdoc
	 */
	protected static function getAcceptedResources(): array
	{
		return [
			'Socket',
		];
	}

	/**
	 * @return bool
	 * @see socket_close()
	 */
	public function close(): bool
	{
		$this->dynamicCall('socket_close');
		return true;
	}

	/**
	 * @return static
	 * @see socket_accept()
	 */
	public function accept()
	{
		return static::initResource('socket_accept', $this->compileParameters());
	}

	/**
	 * @param string $address
	 * @param int $port
	 * @return bool
	 * @see socket_bind()
	 */
	public function bind(string $address, int $port = 0)
	{
		return $this->dynamicCall('socket_bind', func_get_args());
	}

	/**
	 * @return void
	 * @see socket_clear_error()
	 */
	public function clearError()
	{
		$this->dynamicCall('socket_clear_error');
	}

	/**
	 * @param string $address
	 * @param int $port
	 * @return bool
	 * @see socket_connect()
	 */
	public function connect(string $address, int $port = 0)
	{
		return $this->dynamicCall('socket_connect', func_get_args());
	}

	/**
	 * @param int $level
	 * @param int $optionName
	 * @return mixed
	 * @see socket_get_option()
	 */
	public function getOption(int $level, int $optionName)
	{
		return $this->dynamicCall('socket_get_option', func_get_args());
	}

	/**
	 * @param string $address
	 * @param int|null $port
	 * @return bool
	 * @see socket_getpeername()
	 */
	public function getPeerName(string &$address, int &$port = null)
	{
		return $this->dynamicCall('socket_getpeername', func_get_args());
	}

	/**
	 * @param string $address
	 * @param int|null $port
	 * @return bool
	 * @see socket_getsockname()
	 */
	public function getSocketName(string &$address, int &$port = null)
	{
		return $this->dynamicCall('socket_getsockname', func_get_args());
	}

	/**
	 * @param Stream|resource $stream
	 * @return static
	 * @see socket_import_stream()
	 */
	public static function importStream($stream)
	{
		return static::initResource('socket_import_stream', func_get_args());
	}

	/**
	 * @return int
	 * @see socket_last_error()
	 */
	public function lastError()
	{
		return $this->dynamicCall('socket_last_error');
	}

	/**
	 * @return int
	 * @see socket_last_error()
	 */
	public static function globalLastError()
	{
		return static::staticCall('socket_last_error');
	}

	/**
	 * @param int $backlog
	 * @return bool
	 * @see socket_listen()
	 */
	public function listen(int $backlog = 0)
	{
		return $this->dynamicCall('socket_listen', func_get_args());
	}

	/**
	 * @param int $length
	 * @param int $type
	 * @return string
	 * @see socket_read()
	 */
	public function read(int $length, int $type = PHP_BINARY_READ)
	{
		return $this->dynamicCall('socket_read', func_get_args());
	}

	/**
	 * @param string $buffer
	 * @param int $length
	 * @param int $flags
	 * @return int
	 * @see socket_recv()
	 */
	public function receive(string &$buffer, int $length, int $flags)
	{
		return $this->dynamicCall('socket_recv', func_get_args());
	}

	/**
	 * @param string $buffer
	 * @param int $length
	 * @param int $flags
	 * @param string $name
	 * @param int $port
	 * @return int
	 * @see socket_recvfrom()
	 */
	public function receiveFrom(string &$buffer, int $length, int $flags, string &$name, int &$port)
	{
		return $this->dynamicCall('socket_recvfrom', func_get_args());
	}

	/**
	 * @param string $message
	 * @param int|null $flags
	 * @return int
	 * @see socket_recvmsg()
	 */
	public function receiveMessage(string $message, int $flags = null)
	{
		return $this->dynamicCall('socket_recvmsg', func_get_args());
	}

	/**
	 * @param array $read
	 * @param array $write
	 * @param array $except
	 * @param int $timeoutSecond
	 * @param int $timeoutMicrosecond
	 * @return int
	 * @see socket_select()
	 */
	public static function select(array &$read, array &$write, array &$except, int $timeoutSecond, int $timeoutMicrosecond = 0)
	{
		return static::staticCall('socket_select', func_get_args());
	}

	/**
	 * @param string $buffer
	 * @param int $length
	 * @param int $flags
	 * @return int
	 * @see socket_send()
	 */
	public function send(string $buffer, int $length, int $flags)
	{
		return $this->dynamicCall('socket_send', func_get_args());
	}

	/**
	 * @param array $message
	 * @param int $flags
	 * @return int
	 * @see socket_sendmsg()
	 */
	public function sendMessage(array $message, int $flags)
	{
		return $this->dynamicCall('socket_sendmsg', func_get_args());
	}

	/**
	 * @param string $buffer
	 * @param int $length
	 * @param int $flags
	 * @param string $address
	 * @param int $port
	 * @return int
	 * @see socket_sendto()
	 */
	public function sendTo(string $buffer, int $length, int $flags, string $address, int $port = 0)
	{
		return $this->dynamicCall('socket_sendto', func_get_args());
	}

	/**
	 * @return bool
	 * @see socket_set_block()
	 */
	public function setBlocking()
	{
		return $this->dynamicCall('socket_set_block', func_get_args());
	}

	/**
	 * @return bool
	 * @see socket_set_nonblock()
	 */
	public function setNonBlocking()
	{
		return $this->dynamicCall('socket_set_nonblock', func_get_args());
	}

	/**
	 * @param int $level
	 * @param int $optionName
	 * @param mixed $optionValue
	 * @return bool
	 * @see socket_set_option()
	 */
	public function setOption(int $level, int $optionName, $optionValue)
	{
		return $this->dynamicCall('socket_set_option', func_get_args());
	}

	/**
	 * @param int $how
	 * @return bool
	 * @see socket_shutdown()
	 */
	public function shutdown(int $how = 2)
	{
		return $this->dynamicCall('socket_shutdown', func_get_args());
	}

	/**
	 * @param int $errorNumber
	 * @return string
	 * @see socket_strerror()
	 */
	public static function stringError(int $errorNumber)
	{
		return static::staticCall('socket_strerror', func_get_args());
	}

	/**
	 * @param string $buffer
	 * @param int $length
	 * @return int
	 */
	public function write(string $buffer, int $length = 0)
	{
		return $this->dynamicCall('socket_write', func_get_args());
	}
}
