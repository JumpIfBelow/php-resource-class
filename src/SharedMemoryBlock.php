<?php

namespace ResourceClass;

use ResourceWrapper\AbstractCloseableResourceWrapper;

/**
 * Class SharedMemoryBlock
 * @package ResourceClass
 * @link https://www.php.net/manual/en/book.shmop.php
 */
class SharedMemoryBlock extends AbstractCloseableResourceWrapper
{
    /**
     * @param int $key
     * @param string $flags
     * @param int $mode
     * @param int $size
     * @return static
     * @see shmop_open()
     * @see ftok()
     */
    public static function open(int $key, string $flags, int $mode, int $size)
    {
        return static::initResource('shmop_open', func_get_args());
    }

    /**
     * @inheritdoc
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'shmop',
        ];
    }

    /**
     * @inheritdoc
     * @see shmop_close()
     */
    public function close(): bool
    {
	    $this->dynamicCall('shmop_close');
        return true;
    }

    /**
     * @return bool
     * @see shmop_delete()
     */
    public function delete()
    {
	    return $this->dynamicCall('shmop_delete');
    }

    /**
     * @param int $start
     * @param int $count
     * @return string
     * @see shmop_read()
     */
    public function read(int $start, int $count)
    {
	    return $this->dynamicCall('shmop_read', func_get_args());
    }

    /**
     * @return int
     * @see shmop_size()
     */
    public function size()
    {
	    return $this->dynamicCall('shmop_size', func_get_args());
    }

    /**
     * @param string $data
     * @param int $offset
     * @return int
     * @see shmop_write()
     */
    public function write(string $data, int $offset)
    {
	    return $this->dynamicCall('shmop_write', func_get_args());
    }
}
