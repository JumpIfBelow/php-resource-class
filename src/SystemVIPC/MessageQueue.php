<?php

namespace ResourceClass\SystemVIPC;

use ResourceWrapper\AbstractResourceWrapper;

/**
 * Class MessageQueue
 * @package ResourceClass\SystemVIPC
 * @link https://www.php.net/manual/en/book.sem.php
 */
class MessageQueue extends AbstractResourceWrapper
{
    public const IPC_NOWAIT = MSG_IPC_NOWAIT;
    public const EAGAIN = MSG_EAGAIN;
    public const ENOMSG = MSG_ENOMSG;
    public const NOERROR = MSG_NOERROR;
    public const EXCEPT = MSG_EXCEPT;

    /**
     * @param int $key
     * @param int $permissions
     * @return static
     * @see msg_get_queue()
     * @see ftok()
     */
    public static function get(int $key, int $permissions = 0666)
    {
        return static::initResource('msg_get_queue', func_get_args());
    }

    /**
     * @param int $key
     * @return bool
     * @see msg_queue_exists()
     */
    public static function exists(int $key)
    {
    	return static::staticCall('msg_queue_exists', func_get_args());
    }

    /**
     * @inheritdoc
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'sysvmsg queue',
        ];
    }

    /**
     * @param int $desiredMessageType
     * @param int $messageType
     * @param int $maxSize
     * @param mixed $message
     * @param bool $unserialize
     * @param int $flags
     * @param int|null $errorCode
     * @return bool
     * @see msg_receive()
     */
    public function receive(int $desiredMessageType, int &$messageType, int $maxSize, &$message, bool $unserialize = true, int $flags = 0, int &$errorCode = null)
    {
	    return $this->dynamicCall('msg_receive', func_get_args());
    }

    /**
     * @return bool
     * @see msg_remove_queue()
     */
    public function remove()
    {
	    return $this->dynamicCall('msg_remove_queue');
    }

    /**
     * @param int $messageType
     * @param mixed $message
     * @param bool $serialize
     * @param bool $blocking
     * @param int|null $errorCode
     * @return bool
     * @see msg_send()
     */
    public function send(int $messageType, $message, bool $serialize = true, bool $blocking = true, int &$errorCode = null)
    {
	    return $this->dynamicCall('msg_send', func_get_args());
    }

    /**
     * @param array $data
     * @return bool
     * @see msg_set_queue()
     */
    public function set(array $data)
    {
	    return $this->dynamicCall('msg_set_queue', func_get_args());
    }

    /**
     * @return array
     * @see msg_stat_queue()
     */
    public function stat()
    {
	    return $this->dynamicCall('msg_stat_queue', func_get_args());
    }
}
