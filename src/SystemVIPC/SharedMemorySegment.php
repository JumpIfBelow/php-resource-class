<?php

namespace ResourceClass\SystemVIPC;

use ResourceWrapper\AbstractResourceWrapper;

/**
 * Class SharedMemorySegment
 * @package ResourceClass\SystemVIPC
 * @link https://www.php.net/manual/en/book.sem.php
 */
class SharedMemorySegment extends AbstractResourceWrapper
{
    /**
     * @param int $key
     * @param int|null $memorySize
     * @param int $permissions
     * @return static
     * @see shm_attach()
     */
    public static function attach(int $key, int $memorySize = null, int $permissions = 0666)
    {
        return static::initResource('shm_attach', func_get_args());
    }

    /**
     * @inheritdoc
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'sysvshm',
        ];
    }

    /**
     * @return bool
     * @see shm_detach()
     */
    public function detach()
    {
	    return $this->dynamicCall('shm_detach');
    }

    /**
     * @param int $variableKey
     * @return mixed
     * @see shm_get_var()
     */
    public function getVariable(int $variableKey)
    {
	    return $this->dynamicCall('shm_get_var', func_get_args());
    }

    /**
     * @param int $variableKey
     * @return bool
     * @see shm_has_var()
     */
    public function hasVariable(int $variableKey)
    {
	    return $this->dynamicCall('shm_has_var', func_get_args());
    }

    /**
     * @param int $variableKey
     * @param mixed $variable
     * @return bool
     * @see shm_put_var()
     */
    public function putVariable(int $variableKey, $variable)
    {
	    return $this->dynamicCall('shm_put_var', func_get_args());
    }

    /**
     * @param int $variableKey
     * @return bool
     * @see shm_remove_var()
     */
    public function removeVariable(int $variableKey)
    {
	    return $this->dynamicCall('shm_remove_var', func_get_args());
    }

    /**
     * @return bool
     * @see shm_remove()
     */
    public function remove()
    {
	    return $this->dynamicCall('shm_remove');
    }
}
