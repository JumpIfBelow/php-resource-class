<?php

namespace ResourceClass\SystemVIPC;

use ResourceWrapper\AbstractResourceWrapper;

/**
 * Class Semaphore
 * @package ResourceClass\SystemVIPC
 * @link https://www.php.net/manual/en/book.sem.php
 */
class Semaphore extends AbstractResourceWrapper
{

    /**
     * @param $key
     * @param int $maxAcquire
     * @param int $permissions
     * @param int $autorelease
     * @return static
     * @see sem_get()
     * @see ftok()
     */
    public static function get(int $key, int $maxAcquire = 1, int $permissions = 0666, int $autorelease = 1)
    {
        return static::initResource('sem_get', func_get_args());
    }

    /**
     * @inheritdoc
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'sysvsem',
        ];
    }

    /**
     * @return bool
     * @see sem_acquire()
     */
    public function acquire()
    {
	    return $this->dynamicCall('sem_acquire');
    }

    /**
     * @return bool
     * @see sem_release()
     */
    public function release()
    {
	    return $this->dynamicCall('sem_release');
    }

    /**
     * @return bool
     * @see sem_remove()
     */
    public function remove()
    {
	    return $this->dynamicCall('sem_remove');
    }
}
