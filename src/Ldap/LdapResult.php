<?php

namespace ResourceClass\Ldap;

use ResourceWrapper\AbstractCloseableResourceWrapper;

/**
 * Class LdapResult
 * @package ResourceClass\Ldap
 * @link https://www.php.net/manual/en/book.ldap.php
 */
class LdapResult extends AbstractCloseableResourceWrapper
{

    /**
     * @inheritDoc
     * @return bool
     * @see LdapResult::freeResult()
     */
    public function close(): bool
    {
        return $this->freeResult();
    }

    /**
     * Returns accepted resources for the resource wrapper
     * @return array The list of resources accepted by the resource wrapper
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'ldap result',
        ];
    }

    /**
     * @return bool
     * @see ldap_free_result()
     */
    public function freeResult(): bool
    {
        return $this->dynamicCall('ldap_free_result');
    }
}
