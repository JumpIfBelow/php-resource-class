<?php

namespace ResourceClass\Ldap;

use ResourceWrapper\AbstractResourceWrapper;

/**
 * Class LdapResultEntry
 * @package ResourceClass\Ldap
 * @link https://www.php.net/manual/en/book.ldap.php
 */
class LdapResultEntry extends AbstractResourceWrapper
{
    /**
     * @inheritDoc
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'ldap result entry',
        ];
    }
}
