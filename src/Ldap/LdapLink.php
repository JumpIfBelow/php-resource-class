<?php

namespace ResourceClass\Ldap;

use ResourceWrapper\AbstractCloseableResourceWrapper;

/**
 * Class LdapLink
 * @package ResourceClass\Ldap
 * @link https://www.php.net/manual/en/book.ldap.php
 */
class LdapLink extends AbstractCloseableResourceWrapper
{
    public const DEREF_NEVER = LDAP_DEREF_NEVER;
    public const DEREF_SEARCHING = LDAP_DEREF_SEARCHING;
    public const DEREF_FINDING = LDAP_DEREF_FINDING;
    public const DEREF_ALWAYS = LDAP_DEREF_ALWAYS;

    public const OPT_DEREF = LDAP_OPT_DEREF;
    public const OPT_SIZELIMIT = LDAP_OPT_SIZELIMIT;
    public const OPT_TIMELIMIT = LDAP_OPT_TIMELIMIT;
    public const OPT_NETWORK_TIMEOUT = LDAP_OPT_NETWORK_TIMEOUT;
    public const OPT_PROTOCOL_VERSION = LDAP_OPT_PROTOCOL_VERSION;
    public const OPT_ERROR_NUMBER = LDAP_OPT_ERROR_NUMBER;
    public const OPT_REFERRALS = LDAP_OPT_REFERRALS;
    public const OPT_RESTART = LDAP_OPT_RESTART;
    public const OPT_HOST_NAME = LDAP_OPT_HOST_NAME;
    public const OPT_ERROR_STRING = LDAP_OPT_ERROR_STRING;
    public const OPT_DIAGNOSTIC_MESSAGE = LDAP_OPT_DIAGNOSTIC_MESSAGE;
    public const OPT_MATCHED_DN = LDAP_OPT_MATCHED_DN;
    public const OPT_SERVER_CONTROLS = LDAP_OPT_SERVER_CONTROLS;
    public const OPT_CLIENT_CONTROLS = LDAP_OPT_CLIENT_CONTROLS;
    public const OPT_DEBUG_LEVEL = LDAP_OPT_DEBUG_LEVEL;

    public const OPT_X_KEEPALIVE_IDLE = LDAP_OPT_X_KEEPALIVE_IDLE;
    public const OPT_X_KEEPALIVE_PROBES = LDAP_OPT_X_KEEPALIVE_PROBES;
    public const OPT_X_KEEPALIVE_INTERVAL = LDAP_OPT_X_KEEPALIVE_INTERVAL;

    public const OPT_X_TLS_CACERTDIR = LDAP_OPT_X_TLS_CACERTDIR;
    public const OPT_X_TLS_CACERTFILE = LDAP_OPT_X_TLS_CACERTFILE;
    public const OPT_X_TLS_CERTFILE = LDAP_OPT_X_TLS_CERTFILE;
    public const OPT_X_TLS_CIPHER_SUITE = LDAP_OPT_X_TLS_CIPHER_SUITE;
    public const OPT_X_TLS_CRLCHECK = LDAP_OPT_X_TLS_CRLCHECK;
    public const OPT_X_TLS_CRLFILE = LDAP_OPT_X_TLS_CRLFILE;
    public const OPT_X_TLS_DHFILE = LDAP_OPT_X_TLS_DHFILE;
    public const OPT_X_TLS_KEYFILE = LDAP_OPT_X_TLS_KEYFILE;
    public const OPT_X_TLS_PROTOCOL_MIN = LDAP_OPT_X_TLS_PROTOCOL_MIN;
    public const OPT_X_TLS_RANDOM_FILE = LDAP_OPT_X_TLS_RANDOM_FILE;
    public const OPT_X_TLS_REQUIRE_CERT = LDAP_OPT_X_TLS_REQUIRE_CERT;

    public const GSLC_SSL_NO_AUTH = GSLC_SSL_NO_AUTH;
    public const GSLC_SSL_ONEWAY_AUTH = GSLC_SSL_ONEWAY_AUTH;
    public const GSLC_SSL_TWOWAY_AUTH = GSLC_SSL_TWOWAY_AUTH;

    public const LDAP_EXOP_START_TLS = LDAP_EXOP_START_TLS;

    public const EXOP_MODIFY_PASSWD = LDAP_EXOP_MODIFY_PASSWD;
    public const EXOP_REFRESH = LDAP_EXOP_REFRESH;
    public const EXOP_WHO_AM_I = LDAP_EXOP_WHO_AM_I;
    public const EXOP_TURN = LDAP_EXOP_TURN;

    public const CONTROL_MANAGEDSAIT = LDAP_CONTROL_MANAGEDSAIT;
    public const CONTROL_PROXY_AUTHZ = LDAP_CONTROL_PROXY_AUTHZ;
    public const CONTROL_SUBENTRIES = LDAP_CONTROL_SUBENTRIES;
    public const CONTROL_VALUESRETURNFILTER = LDAP_CONTROL_VALUESRETURNFILTER;
    public const CONTROL_ASSERT = LDAP_CONTROL_ASSERT;
    public const CONTROL_PRE_READ = LDAP_CONTROL_PRE_READ;
    public const CONTROL_POST_READ = LDAP_CONTROL_POST_READ;
    public const CONTROL_SORTREQUEST = LDAP_CONTROL_SORTREQUEST;
    public const CONTROL_SORTRESPONSE = LDAP_CONTROL_SORTRESPONSE;
    public const CONTROL_PAGEDRESULTS = LDAP_CONTROL_PAGEDRESULTS;
    public const CONTROL_AUTHZID_REQUEST = LDAP_CONTROL_AUTHZID_REQUEST;
    public const CONTROL_AUTHZID_RESPONSE = LDAP_CONTROL_AUTHZID_RESPONSE;
    public const CONTROL_SYNC = LDAP_CONTROL_SYNC;
    public const CONTROL_SYNC_STATE = LDAP_CONTROL_SYNC_STATE;
    public const CONTROL_SYNC_DONE = LDAP_CONTROL_SYNC_DONE;
    public const CONTROL_DONTUSECOPY = LDAP_CONTROL_DONTUSECOPY;
    public const CONTROL_PASSWORDPOLICYREQUEST = LDAP_CONTROL_PASSWORDPOLICYREQUEST;
    public const CONTROL_PASSWORDPOLICYRESPONSE = LDAP_CONTROL_PASSWORDPOLICYRESPONSE;

    public const CONTROL_X_INCREMENTAL_VALUES = LDAP_CONTROL_X_INCREMENTAL_VALUES;
    public const CONTROL_X_DOMAIN_SCOPE = LDAP_CONTROL_X_DOMAIN_SCOPE;
    public const CONTROL_X_PERMISSIVE_MODIFY = LDAP_CONTROL_X_PERMISSIVE_MODIFY;
    public const CONTROL_X_SEARCH_OPTIONS = LDAP_CONTROL_X_SEARCH_OPTIONS;
    public const CONTROL_X_TREE_DELETE = LDAP_CONTROL_X_TREE_DELETE;
    public const CONTROL_X_EXTENDED_DN = LDAP_CONTROL_X_EXTENDED_DN;
    public const CONTROL_VLVREQUEST = LDAP_CONTROL_VLVREQUEST;
    public const CONTROL_VLVRESPONSE = LDAP_CONTROL_VLVRESPONSE;

    /**
     * @param string|null $uri
     * @return static
     * @see ldap_connect()
     */
    public static function connect(string $uri = null): self
    {
        return static::initResource('ldap_connect', func_get_args());
    }

    /**
     * @param string $value
     * @return string
     * @see ldap_8859_to_t61()
     */
    public static function iso8859ToT61(string $value): string
    {
        return static::staticCall('ldap_8859_to_t61', func_get_args());
    }

    /**
     * @param string $dn
     * @return string
     * @see ldap_dn2ufn()
     */
    public static function dn2ufn(string $dn): string
    {
        return static::staticCall('ldap_dn2ufn', func_get_args());
    }

    /**
     * @param int $errno
     * @return string
     * @see ldap_err2str()
     */
    public static function err2str(int $errno): string
    {
        return static::staticCall('ldap_err2str', func_get_args());
    }

    /**
     * @param string $value
     * @param string $ignore
     * @param int $flags
     * @return string
     * @see ldap_escape()
     */
    public static function escape(string $value, string $ignore = '', int $flags = 0): string
    {
        return static::staticCall('ldap_escape', func_get_args());
    }

    /**
     * @param string $dn
     * @param int $withAttributes
     * @return array
     * @see ldap_explode_dn()
     */
    public static function explodeDn(string $dn, int $withAttributes): array
    {
        return static::staticCall('ldap_explode_dn', func_get_args());
    }

    /**
     * @param string $value
     * @return string
     * @see ldap_t61_to_8859()
     */
    public static function t61ToIso8859(string $value): string
    {
        return static::staticCall('ldap_t61_to_8859', func_get_args());
    }

    /**
     * @inheritDoc
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'ldap link',
        ];
    }

    /**
     * @inheritDoc
     * @return true
     * @see ldap_close()
     */
    public function close(): bool
    {
        $this->dynamicCall('ldap_close');

        return true;
    }

    /**
     * @param string $dn
     * @param array $entry
     * @param array $serverControls
     * @return LdapResult
     * @see ldap_add_ext()
     */
    public function addExt(string $dn, array $entry, array $serverControls = []): LdapResult
    {
        return LdapResult::initResource('ldap_add_ext', $this->compileParameters(func_get_args()));
    }

    /**
     * @param string $dn
     * @param string $entry
     * @param array $serverControls
     * @return bool
     * @see ldap_add()
     */
    public function add(string $dn, string $entry, array $serverControls = []): bool
    {
        return $this->dynamicCall('ldap_add', func_get_args());
    }

    /**
     * @param string|null $bindRdn
     * @param string|null $bindPassword
     * @param array $serverControls
     * @return LdapResult
     * @see ldap_bind_ext()
     */
    public function bindExt(string $bindRdn = null, string $bindPassword = null, array $serverControls = []): LdapResult
    {
        return LdapResult::initResource('ldap_bind_ext', $this->compileParameters(func_get_args()));
    }

    /**
     * @param string|null $bindRdn
     * @param string|null $bindPassword
     * @param array $serverControls
     * @return bool
     * @see ldap_bind()
     */
    public function bind(string $bindRdn = null, string $bindPassword = null, array $serverControls = []): bool
    {
        return $this->dynamicCall('ldap_bind', func_get_args());
    }

    /**
     * @param string $dn
     * @param string $attribute
     * @param string $value
     * @param array $serverControls
     * @return bool|int
     * @see ldap_compare()
     */
    public function compare(string $dn, string $attribute, string $value, array $serverControls = [])
    {
        return $this->dynamicCall('ldap_compare', func_get_args());
    }

    /**
     * @param LdapResult|resource $result
     * @param string|null $cookie
     * @param int|null $estimated
     * @return bool
     * @see ldap_control_paged_result_response()
     */
    public function controlPagedResultResponse($result, string &$cookie = null, int &$estimated = null): bool
    {
        return $this->dynamicCall('ldap_control_paged_result_response', func_get_args());
    }

    /**
     * @param int $pageSize
     * @param bool $isCritical
     * @param string $cookie
     * @return bool
     * @see ldap_control_paged_result()
     */
    public function controlPagedResult(int $pageSize, bool $isCritical = false, string $cookie = ''): bool
    {
        return $this->dynamicCall('ldap_control_paged_result', func_get_args());
    }

    /**
     * @param $result
     * @return int
     * @see ldap_count_entries()
     */
    public function countEntries($result): int
    {
        return $this->dynamicCall('ldap_count_entries', func_get_args());
    }

    /**
     * @param string $dn
     * @param array $serverControls
     * @return LdapResult
     * @see ldap_delete_ext()
     */
    public function deleteExt(string $dn, array $serverControls = [])
    {
        return $this->dynamicCall('ldap_delete_ext', func_get_args());
    }

    /**
     * @param string $dn
     * @param array $serverControls
     * @return bool
     * @see ldap_delete()
     */
    public function delete(string $dn, array $serverControls = []): bool
    {
        return $this->dynamicCall('ldap_delete', func_get_args());
    }

    /**
     * @return int
     * @see ldap_errno()
     */
    public function errno(): int
    {
        return $this->dynamicCall('ldap_errno', func_get_args());
    }

    /**
     * @return string
     * @see ldap_error()
     */
    public function error(): string
    {
        return $this->dynamicCall('ldap_error', func_get_args());
    }

    /**
     * @param string $user
     * @param string $oldPassword
     * @param string $newPassword
     * @param array $serverControls
     * @return mixed
     * @see ldap_exop_passwd()
     */
    public function exopPasswd(string $user, string $oldPassword, string $newPassword, array &$serverControls = [])
    {
        return $this->dynamicCall('ldap_exop_passwd', func_get_args());
    }

    /**
     * @param string $dn
     * @param int $ttl
     * @return int
     * @see ldap_exop_refresh()
     */
    public function exopRefresh(string $dn, int $ttl): int
    {
        return $this->dynamicCall('ldap_exop_refresh', func_get_args());
    }

    /**
     * @return string
     * @see ldap_exop_whoami()
     */
    public function exopWhoami(): string
    {
        return $this->dynamicCall('ldap_exop_whoami', func_get_args());
    }

    /**
     * @param string $requestOid
     * @param string|null $questData
     * @param array $serverControls
     * @param string|null $returnData
     * @param string|null $returnOid
     * @return mixed
     * @see ldap_exop()
     */
    public function exop(string $requestOid, string $questData = null, array $serverControls = [], string &$returnData = null, string &$returnOid = null)
    {
        return $this->dynamicCall('ldap_exop', func_get_args());
    }

    /**
     * @param LdapResult|resource $result
     * @return string
     * @see ldap_first_attribute()
     */
    public function firstAttribute($result): string
    {
        return $this->dynamicCall('ldap_first_attribute', func_get_args());
    }

    /**
     * @param LdapResult|resource $result
     * @return LdapResultEntry
     * @see ldap_first_entry()
     */
    public function firstEntry($result): LdapResultEntry
    {
        return LdapResultEntry::initResource('ldap_first_entry', $this->compileParameters(func_get_args()));
    }

    /**
     * @param LdapResult|resource $result
     * @return LdapResult
     * @see ldap_first_reference()
     */
    public function firstReference($result): LdapResult
    {
        return LdapResult::initResource('ldap_first_reference', $this->compileParameters(func_get_args()));
    }

    /**
     * @param LdapResult|resource $result
     * @return array|false
     * @see ldap_get_attributes()
     */
    public function getAttributes($result)
    {
        return $this->dynamicCall('ldap_get_attributes', func_get_args());
    }

    /**
     * @param LdapResult|resource $result
     * @return array|false
     * @see ldap_get_dn()
     */
    public function getDn($result)
    {
        return $this->dynamicCall('ldap_get_dn', func_get_args());
    }

    /**
     * @param LdapResult|resource $result
     * @return array|false
     * @see ldap_get_entries()
     */
    public function getEntries($result): array
    {
        return $this->dynamicCall('ldap_get_entries', func_get_args());
    }

    /**
     * @param int $option
     * @param mixed $returnValue
     * @return bool
     * @see ldap_get_option()
     */
    public function getOption(int $option, &$returnValue): bool
    {
        return $this->dynamicCall('ldap_get_option', func_get_args());
    }

    /**
     * @param LdapResult|resource $result
     * @param string $attribute
     * @return array|false
     * @see ldap_get_values_len()
     */
    public function getValuesLen($result, string $attribute)
    {
        return $this->dynamicCall('ldap_get_values_len', func_get_args());
    }

    /**
     * @param LdapResult|resource $result
     * @param string $attribute
     * @return array|false
     */
    public function getValues($result, string $attribute)
    {
        return $this->dynamicCall('ldap_get_values', func_get_args());
    }

    /**
     * @param string $baseDn
     * @param string $filter
     * @param array $attributes
     * @param int $attributesOnly
     * @param int $sizeLimit
     * @param int $timeLimit
     * @param int $deref
     * @param array $serverControls
     * @return LdapResult
     * @see ldap_list()
     */
    public function list(string $baseDn, string $filter, array $attributes = ['*'], int $attributesOnly = 0, int $sizeLimit = -1, int $timeLimit = -1, int $deref = LDAP_DEREF_NEVER, array $serverControls = []): LdapResult
    {
        return LdapResult::initResource('ldap_list', $this->compileParameters(func_get_args()));
    }

    /**
     * @param string $dn
     * @param array $entry
     * @param array $serverControls
     * @return LdapResult
     */
    public function modAddExt(string $dn, array $entry, array $serverControls = []): LdapResult
    {
        return LdapResult::initResource('ldap_mod_add_ext', $this->compileParameters(func_get_args()));
    }

    /**
     * @param string $dn
     * @param array $entry
     * @param array $serverControls
     * @return bool
     * @see ldap_mod_add()
     */
    public function modAdd(string $dn, array $entry, array $serverControls = []): bool
    {
        return $this->dynamicCall('ldap_mod_add', func_get_args());
    }

    /**
     * @param string $dn
     * @param array $entry
     * @param array $serverControls
     * @return LdapResult
     * @see ldap_mod_del_ext()
     */
    public function modDelExt(string $dn, array $entry, array $serverControls = []): LdapResult
    {
        return LdapResult::initResource('ldap_mod_del_ext', $this->compileParameters(func_get_args()));
    }

    /**
     * @param string $dn
     * @param array $entry
     * @param array $serverControls
     * @return bool
     * @see ldap_mod_del()
     */
    public function modDel(string $dn, array $entry, array $serverControls = []): bool
    {
        return $this->dynamicCall('ldap_mod_del', func_get_args());
    }

    /**
     * @param string $dn
     * @param array $entry
     * @param array $serverControls
     * @return LdapResult
     * @see ldap_mod_replace_ext()
     */
    public function modReplaceExt(string $dn, array $entry, array $serverControls): LdapResult
    {
        return LdapResult::initResource('ldap_mod_replace_ext', $this->compileParameters(func_get_args()));
    }

    /**
     * @param string $dn
     * @param array $entry
     * @param array $serverControls
     * @return bool
     * @see ldap_mod_replace()
     */
    public function modReplace(string $dn, array $entry, array $serverControls = []): bool
    {
        return $this->dynamicCall('ldap_mod_replace', func_get_args());
    }

    /**
     * @param string $dn
     * @param array $entry
     * @param array $serverControls
     * @return bool
     * @see ldap_modify_batch()
     */
    public function modifyBatch(string $dn, array $entry, array $serverControls = []): bool
    {
        return $this->dynamicCall('ldap_modify_batch', func_get_args());
    }

    /**
     * @param LdapResult|resource $result
     * @return string
     * @see ldap_next_result()
     */
    public function nextAttribute($result): string
    {
        return $this->dynamicCall('ldap_next_result', func_get_args());
    }

    /**
     * @param LdapResultEntry|resource $entry
     * @return LdapResultEntry
     * @see ldap_next_entry()
     */
    public function nextEntry($entry): LdapResultEntry
    {
        return LdapResultEntry::initResource('ldap_next_entry', $this->compileParameters(func_get_args()));
    }

    /**
     * @param LdapResultEntry|resource $entry
     * @return LdapResultEntry
     * @see ldap_next_reference()
     */
    public function nextReference($entry): LdapResultEntry
    {
        return LdapResultEntry::initResource('ldap_next_reference', $this->compileParameters(func_get_args()));
    }

    /**
     * @param LdapResult|resource $result
     * @param string|null $returnData
     * @param string|null $returnOid
     * @return bool
     */
    public function parseExop($result, string &$returnData = null, string &$returnOid = null): bool
    {
        return $this->dynamicCall('ldap_parse_exop', func_get_args());
    }

    /**
     * @param LdapResultEntry|resource $entry
     * @param array $referrals
     * @return bool
     * @see ldap_parse_reference()
     */
    public function parseReference($entry, array &$referrals): bool
    {
        return $this->dynamicCall('ldap_parse_reference', func_get_args());
    }

    /**
     * @param LdapResult|resource $result
     * @param int $errorCode
     * @param string|null $matchedDn
     * @param string|null $errorMessage
     * @param array $referrals
     * @param array $serverControls
     * @return bool
     * @see ldap_parse_result()
     */
    public function parseResult($result, int &$errorCode, string &$matchedDn = null, string &$errorMessage = null, array &$referrals = [], array &$serverControls = []): bool
    {
        return $this->dynamicCall('ldap_parse_result', func_get_args());
    }

    /**
     * @param string $baseDn
     * @param string $filter
     * @param array $attributes
     * @param int $attributesOnly
     * @param int $sizeLimit
     * @param int $timeLimit
     * @param int $deref
     * @param array $serverControls
     * @return LdapResult
     * @see ldap_read()
     */
    public function read(string $baseDn, string $filter, array $attributes = ['*'], int $attributesOnly = 0, int $sizeLimit = -1, int $timeLimit = -1, int $deref = LDAP_DEREF_NEVER, array $serverControls = []): LdapResult
    {
        return LdapResult::initResource('ldap_read', $this->compileParameters(func_get_args()));
    }

    /**
     * @param string $dn
     * @param string $newRdn
     * @param string $newParent
     * @param bool $deleteOldRdn
     * @param array $serverControls
     * @return LdapResult
     * @see ldap_rename_ext()
     */
    public function renameExt(string $dn, string $newRdn, string $newParent, bool $deleteOldRdn, array $serverControls = []): LdapResult
    {
        return LdapResult::initResource('ldap_rename_ext', $this->compileParameters(func_get_args()));
    }

    /**
     * @param string $dn
     * @param string $newRdn
     * @param string $newParent
     * @param bool $deleteOldRdn
     * @param array $serverControls
     * @return bool
     * @see ldap_rename()
     */
    public function rename(string $dn, string $newRdn, string $newParent, bool $deleteOldRdn, array $serverControls = []): bool
    {
        return $this->dynamicCall('ldap_rename', func_get_args());
    }

    /**
     * @param string|null $bindDn
     * @param string|null $password
     * @param string|null $saslMech
     * @param string|null $saslRealm
     * @param string|null $saslAuthenticationId
     * @param string|null $saslAtuhorizationId
     * @param string|null $properties
     * @return bool
     * @see ldap_sasl_bind()
     */
    public function saslBind(string $bindDn = null, string $password = null, string $saslMech = null, string $saslRealm = null, string $saslAuthenticationId = null, string $saslAtuhorizationId = null, string $properties = null): bool
    {
        return $this->dynamicCall('ldap_sasl_bind', func_get_args());
    }

    /**
     * @param string $baseDn
     * @param string $filter
     * @param array $attributes
     * @param int $attributesOnly
     * @param int $sizeLimit
     * @param int $timeLimit
     * @param int $deref
     * @param array $serverControls
     * @return LdapResult
     * @see ldap_search()
     */
    public function search(string $baseDn, string $filter, array $attributes = ['*'], int $attributesOnly = 0, int $sizeLimit = -1, int $timeLimit = -1, int $deref = LDAP_DEREF_NEVER, array $serverControls = []): LdapResult
    {
        return $this->dynamicCall('ldap_search', func_get_args());
    }

    /**
     * @param int $option
     * @param mixed $newValue
     * @return bool
     * @see ldap_set_option()
     */
    public function setOption(int $option, $newValue): bool
    {
        return $this->dynamicCall('ldap_set_option', func_get_args());
    }

    /**
     * @param callable $callback
     * @return bool
     * @see ldap_set_rebind_proc()
     */
    public function setRebindProc(callable $callback): bool
    {
        return $this->dynamicCall('ldap_set_rebind_proc', func_get_args());
    }

    /**
     * @param LdapResult|resource $result
     * @param string $sortFilter
     * @return bool
     * @see ldap_sort()
     */
    public function sort($result, string $sortFilter): bool
    {
        return $this->dynamicCall('ldap_sort', func_get_args());
    }

    /**
     * @return bool
     * @see ldap_start_tls()
     */
    public function startTls(): bool
    {
        return $this->dynamicCall('ldap_start_tls', func_get_args());
    }
}
