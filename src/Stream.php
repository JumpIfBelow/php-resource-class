<?php

namespace ResourceClass;

use ResourceWrapper\AbstractResourceWrapper;

/**
 * Class Stream
 * @package ResourceClass
 * @link https://www.php.net/manual/en/book.stream.php
 */
class Stream extends AbstractResourceWrapper
{
    public const FILTER_READ = STREAM_FILTER_READ;
    public const FILTER_WRITE = STREAM_FILTER_WRITE;
    public const FILTER_ALL = STREAM_FILTER_ALL;

    public const PSFS_PASS_ON = PSFS_PASS_ON;
    public const PSFS_FEED_ME = PSFS_FEED_ME;
    public const PSFS_ERR_FATAL = PSFS_ERR_FATAL;
    public const PSFS_FLAG_NORMAL = PSFS_FLAG_NORMAL;
    public const PSFS_FLAG_FLUSH_INC = PSFS_FLAG_FLUSH_INC;
    public const PSFS_FLAG_FLUSH_CLOSE = PSFS_FLAG_FLUSH_CLOSE;

    public const USE_PATH = STREAM_USE_PATH;
    public const REPORT_ERRORS = STREAM_REPORT_ERRORS;

    public const CLIENT_ASYNC_CONNECT = STREAM_CLIENT_ASYNC_CONNECT;
    public const CLIENT_CONNECT = STREAM_CLIENT_CONNECT;
    public const CLIENT_PERSISTENT = STREAM_CLIENT_PERSISTENT;

    public const SERVER_BIND = STREAM_SERVER_BIND;
    public const SERVER_LISTEN = STREAM_SERVER_LISTEN;

    public const NOTIFY_RESOLVE = STREAM_NOTIFY_RESOLVE;
    public const NOTIFY_CONNECT = STREAM_NOTIFY_CONNECT;
    public const NOTIFY_AUTH_REQUIRED = STREAM_NOTIFY_AUTH_REQUIRED;
    public const NOTIFY_MIME_TYPE_IS = STREAM_NOTIFY_MIME_TYPE_IS;
    public const NOTIFY_FILE_TYPE_IS = STREAM_NOTIFY_FILE_SIZE_IS;
    public const NOTIFY_REDIRECTED = STREAM_NOTIFY_REDIRECTED;
    public const NOTIFY_PROGRESS = STREAM_NOTIFY_PROGRESS;
    public const NOTIFY_COMPLETED = STREAM_NOTIFY_COMPLETED;
    public const NOTIFY_FAILURE = STREAM_NOTIFY_FAILURE;
    public const NOTIFY_AUTH_RESULT = STREAM_NOTIFY_AUTH_RESULT;
    public const NOTIFY_SEVERITY_INFO = STREAM_NOTIFY_SEVERITY_INFO;
    public const NOTIFY_SEVERITY_WARN = STREAM_NOTIFY_SEVERITY_WARN;
    public const NOTIFY_SEVERITY_ERR = STREAM_NOTIFY_SEVERITY_ERR;

    public const IPPROTO_ICMP = STREAM_IPPROTO_ICMP;
    public const IPPROTO_IP = IPPROTO_IP;
    public const IPPROTO_RAW = STREAM_IPPROTO_RAW;
    public const IPPROTO_TCP = STREAM_IPPROTO_TCP;
    public const IPPROTO_UDP = STREAM_IPPROTO_UDP;

    public const PF_INET = STREAM_PF_INET;
    public const PF_INET6 = STREAM_PF_INET6;
    public const PF_UNIX = STREAM_PF_UNIX;

    public const SOCK_DGRAM = STREAM_SOCK_DGRAM;
    public const SOCK_RAW = STREAM_SOCK_RAW;
    public const SOCK_RDM = STREAM_SOCK_RDM;
    public const SOCK_SEQPACKET = STREAM_SOCK_SEQPACKET;
    public const SOCK_STREAM = STREAM_SOCK_STREAM;

    public const SHUT_RD = STREAM_SHUT_RD;
    public const SHUT_WR = STREAM_SHUT_WR;
    public const SHUT_RDWR = STREAM_SHUT_RDWR;
    public const CAST_FOR_SELECT = STREAM_CAST_FOR_SELECT;
    public const CAST_AS_STREAM = STREAM_CAST_AS_STREAM;

    public const META_TOUCH = STREAM_META_TOUCH;
    public const META_OWNER = STREAM_META_OWNER;
    public const META_OWNER_NAME = STREAM_META_OWNER_NAME;
    public const META_GROUP = STREAM_META_GROUP;
    public const META_GROUP_NAME = STREAM_META_GROUP_NAME;
    public const META_ACCESS = STREAM_META_ACCESS;

    /**
     * @param array|null $options
     * @param array|null $params
     * @return self
     * @see stream_context_create()
     */
    public static function contextCreate(array $options = null, array $params = null)
    {
        return static::initResource('stream_context_create', func_get_args());
    }

    /**
     * @param array|null $options
     * @return self
     * @see stream_context_get_default()
     */
    public static function contextGetDefault(array $options = null)
    {
        return static::initResource('stream_context_get_default', func_get_args());
    }

    /**
     * @param array $options
     * @return self
     * @see stream_context_set_default()
     */
    public static function contextSetDefault(array $options)
    {
        return static::initResource('stream_context_set_default', func_get_args());
    }

    /**
     * @param string $filterName
     * @param string $className
     * @return bool
     * @see stream_filter_register()
     */
    public static function filterRegister(string $filterName, string $className)
    {
        return static::staticCall('stream_filter_register', func_get_args());
    }

    /**
     * @return array
     * @see stream_get_filters()
     */
    public static function getFilters()
    {
        return static::staticCall('stream_get_filters', func_get_args());
    }

    /**
     * @return array
     * @see stream_get_transports()
     */
    public static function getTransports()
    {
        return static::staticCall('stream_get_transports', func_get_args());
    }

    /**
     * @return array
     * @see stream_get_wrappers()
     */
    public static function getWrappers()
    {
        return static::staticCall('stream_get_wrappers', func_get_args());
    }

    /**
     * @param Stream|resource|string $streamOrUrl
     * @return bool
     */
    public static function isLocal($streamOrUrl)
    {
        return stream_is_local($streamOrUrl instanceof Stream ? $streamOrUrl->getResource() : $streamOrUrl);
    }

    /**
     * @param string $filename
     * @return string
     * @see stream_resolve_include_path()
     */
    public static function resolveIncludePath(string $filename)
    {
        return static::staticCall('stream_resolve_include_path', func_get_args());
    }

    /**
     * @param array $read
     * @param array $write
     * @param array $except
     * @param int $timeoutSecond
     * @param int $timeoutMicrosecond
     * @return int
     * @see stream_select()
     */
    public static function select(array &$read, array &$write, array &$except, int $timeoutSecond, int $timeoutMicrosecond)
    {
        return static::staticCall('stream_select', func_get_args());
    }

    /**
     * @param Socket|resource $serverSocket
     * @param float|null $timeout
     * @param string|null $peerName
     * @return static
     */
    public static function socketAccept($serverSocket, float $timeout = null, string &$peerName = null)
    {
    	return static::initResource('stream_socket_accept', func_get_args());
    }

    /**
     * @param string $remoteSocket
     * @param int|null $errorNumber
     * @param string|null $errorString
     * @param float|null $timeout
     * @param int $flags
     * @param Stream|resource|null $context
     * @return static
     * @see stream_socket_client()
     */
    public static function socketClient(string $remoteSocket, int &$errorNumber = null, string &$errorString = null, float $timeout = null, int $flags = STREAM_CLIENT_CONNECT, $context = null)
    {
    	return static::initResource('stream_socket_client', func_get_args());
    }

    /**
     * @param Socket|resource $handle
     * @param bool $wantPeer
     * @return string
     * @see stream_socket_get_name()
     */
    public static function socketGetName($handle, bool $wantPeer)
    {
	    return static::staticCall('socket_stream_get_name', func_get_args());
    }

    /**
     * @param int $domain
     * @param int $type
     * @param int $protocol
     * @return array
     * @see stream_socket_pair()
     */
    public static function socketPair(int $domain, int $type, int $protocol)
    {
        return static::staticCall('stream_socket_pair', func_get_args());
    }

    /**
     * @param Socket|resource $socket
     * @param int $length
     * @param int $flags
     * @param string $address
     * @return string
     * @see stream_socket_recvfrom()
     */
    public static function socketReceiveFrom($socket, int $length, int $flags = 0, string &$address)
    {
    	return static::staticCall('stream_socket_recvfrom', func_get_args());
    }

    /**
     * @param $socket
     * @param string $data
     * @param int $flags
     * @param string $address
     * @return int
     * @see stream_socket_sendto()
     */
    public static function socketSendTo($socket, string $data, int $flags = 0, string $address)
    {
    	return static::staticCall('stream_socket_sendto', func_get_args());
    }

    /**
     * @param string $localSocket
     * @param int|null $errorNumber
     * @param string $errorString
     * @param int $flags
     * @param Stream|resource|null $context
     * @return self
     * @see stream_socket_server()
     */
    public static function socketServer(string $localSocket, int &$errorNumber = null, string &$errorString, int $flags = STREAM_SERVER_BIND | STREAM_SERVER_LISTEN, $context = null)
    {
    	return static::initResource('stream_socket_server', func_get_args());
    }

    /**
     * @param string $protocol
     * @param string $className
     * @param int $flags
     * @return bool
     * @see stream_wrapper_register()
     */
    public static function wrapperRegister(string $protocol, string $className, int $flags = 0)
    {
        return static::staticCall('stream_wrapper_register', func_get_args());
    }

    /**
     * @param string $protocol
     * @return bool
     * @see stream_wrapper_restore()
     */
    public static function wrapperRestore(string $protocol)
    {
        return static::staticCall('stream_wrapper_restore', func_get_args());
    }

    /**
     * @param string $protocol
     * @return bool
     * @see stream_wrapper_unregister()
     */
    public static function wrapperUnregister(string $protocol)
    {
        return static::staticCall('stream_wrapper_unregister', func_get_args());
    }

    /**
     * @return array
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'stream',
        ];
    }

    /**
     * @param resource $bucket
     * @return void
     * @see stream_bucket_append()
     */
    public function bucketAppend($bucket)
    {
	    $this->dynamicCall('stream_bucket_append', func_get_args());
    }

    /**
     * @return object
     * @see stream_bucket_make_writeable()
     */
    public function bucketMakeWriteable()
    {
	    return $this->dynamicCall('stream_bucket_make_writeable', func_get_args());
    }

    /**
     * @param string $buffer
     * @return object
     * @see stream_bucket_new()
     */
    public function bucketNew(string $buffer)
    {
	    return $this->dynamicCall('stream_bucket_new', func_get_args());
    }

    /**
     * @param resource $bucket
     * @return void
     * @see stream_bucket_prepend()
     */
    public function bucketPrepend($bucket)
    {
	    $this->dynamicCall('stream_bucket_prepend', func_get_args());
    }

    /**
     * @return array
     * @see stream_context_get_options()
     */
    public function contextGetOptions()
    {
	    return $this->dynamicCall('stream_context_get_options');
    }

    /**
     * @return array
     * @see stream_context_get_params()
     */
    public function contextGetParameters()
    {
	    return $this->dynamicCall('stream_context_get_params');
    }

    /**
     * @param string $wrapper
     * @param array|string $options
     * @param mixed|null $value
     * @return bool
     * @see stream_context_set_option()
     */
    public function contextSetOption(string $wrapper, $options, $value = null)
    {
	    return $this->dynamicCall('stream_context_set_option', func_get_args());
    }

    /**
     * @param array $parameters
     * @return bool
     * @see stream_context_set_params()
     */
    public function contextSetParameters(array $parameters)
    {
	    return $this->dynamicCall('stream_context_set_params', func_get_args());
    }

    /**
     * @param Stream $destination
     * @param int $maxLength
     * @param int $offset
     * @return int
     * @see stream_copy_to_stream()
     */
    public function copyToStream(Stream $destination, int $maxLength = -1, int $offset = 0)
    {
	    return $this->dynamicCall('stream_copy_to_stream', func_get_args());
    }

    /**
     * @param string|null $encoding
     * @return bool
     * @see stream_encoding()
     */
    public function encoding(string $encoding = null)
    {
	    return $this->dynamicCall('stream_encoding', func_get_args());
    }

    /**
     * @param string $filterName
     * @param int|null $readWrite
     * @param null $parameters
     * @return resource
     * @see stream_filter_append()
     */
    public function filterAppend(string $filterName, int $readWrite = null, $parameters = null)
    {
	    return $this->dynamicCall('stream_filter_append', func_get_args());
    }

    /**
     * @param string $filterName
     * @param int|null $readWrite
     * @param null $parameters
     * @return resource
     * @see stream_filter_prepend()
     */
    public function filterPrepend(string $filterName, int $readWrite = null, $parameters = null)
    {
	    return $this->dynamicCall('stream_filter_prepend', func_get_args());
    }

    /**
     * @return bool
     * @see stream_filter_remove()
     */
    public function filterRemove()
    {
	    return $this->dynamicCall('stream_filter_remove');
    }

    /**
     * @param int|null $maxLength
     * @param int|null $offset
     * @return string
     * @see stream_get_contents()
     */
    public function getContents(int $maxLength = null, int $offset = null)
    {
	    return $this->dynamicCall('stream_get_contents', func_get_args());
    }

    /**
     * @param int $length
     * @param string|null $ending
     * @return string
     * @see stream_get_line()
     */
    public function getLine(int $length, string $ending = null)
    {
	    return $this->dynamicCall('stream_get_line', func_get_args());
    }

    /**
     * @return array
     * @see stream_get_meta_data()
     */
    public function getMetaData()
    {
	    return $this->dynamicCall('stream_get_meta_data');
    }

    /**
     * @param int $mode
     * @return bool
     * @see stream_set_blocking()
     */
    public function setBlocking(int $mode)
    {
	    return $this->dynamicCall('stream_set_blocking', func_get_args());
    }

    /**
     * @param int $chunkSize
     * @return int
     * @see stream_set_chunk_size()
     */
    public function setChunkSize(int $chunkSize)
    {
	    return $this->dynamicCall('stream_set_chunk_size', func_get_args());
    }

    /**
     * @param int $buffer
     * @return int
     * @see stream_set_read_buffer()
     */
    public function setReadBuffer(int $buffer)
    {
	    return $this->dynamicCall('stream_set_read_buffer', func_get_args());
    }

    /**
     * @param int $seconds
     * @param int $microseconds
     * @return bool
     * @see stream_set_timeout()
     */
    public function setTimeout(int $seconds, int $microseconds = 0)
    {
	    return $this->dynamicCall('stream_set_timeout', func_get_args());
    }

    /**
     * @param int $buffer
     * @return int
     * @see stream_set_write_buffer()
     */
    public function setWriteBuffer(int $buffer)
    {
	    return $this->dynamicCall('stream_set_write_buffer', func_get_args());
    }

    /**
     * @param bool $enable
     * @param int|null $cryptographyType
     * @param Stream|resource|null $sessionStream
     * @return mixed
     * @see stream_socket_enable_crypto()
     */
    public function socketEnableCryptography(bool $enable, int $cryptographyType = null, $sessionStream = null)
    {
	    return $this->dynamicCall('stream_socket_enable_crypto', func_get_args());
    }

    /**
     * @param int $how
     * @return bool
     * @see stream_socket_shutdown()
     */
    public function socketShutdown(int $how)
    {
	    return $this->dynamicCall('stream_socket_shutdown', func_get_args());
    }

    /**
     * @return bool
     * @see stream_supports_lock()
     */
    public function supportsLock()
    {
	    return $this->dynamicCall('stream_supports_lock');
    }
}
