<?php

namespace ResourceClass\PostgreSQL;

use ResourceWrapper\AbstractCloseableResourceWrapper;

/**
 * Class PgLargeObject
 * @package ResourceClass\PostgreSQL
 * @link https://www.php.net/manual/en/book.pgsql.php
 */
class PgLargeObject extends AbstractCloseableResourceWrapper
{
    /**
     * @inheritdoc
     */
    protected static function getAcceptedResources(): array
    {
        //TODO find the real resource name
        return [
            parent::ACCEPT_ALL,
            'pgsql large object', //FIXME?
        ];
    }

    /**
     * @return bool
     * @see pg_lo_close()
     */
    public function close(): bool
    {
	    return $this->dynamicCall('pg_lo_close');
    }

    /**
     * @return int
     * @see pg_lo_read_all()
     */
    public function readAll()
    {
	    return $this->dynamicCall('pg_lo_read_all');
    }

    /**
     * @param int $length
     * @return string
     * @see pg_lo_read()
     */
    public function read(int $length = 8192)
    {
	    return $this->dynamicCall('pg_lo_read', func_get_args());
    }

    /**
     * @param int $offset
     * @param int $whence
     * @return bool
     * @see pg_lo_seek()
     */
    public function seek(int $offset, int $whence = PGSQL_SEEK_CUR)
    {
	    return $this->dynamicCall('pg_lo_seek', func_get_args());
    }

    /**
     * @return int
     * @see pg_lo_tell()
     */
    public function tell()
    {
	    return $this->dynamicCall('pg_lo_tell');
    }

    /**
     * @param int $size
     * @return bool
     * @see pg_lo_truncate()
     */
    public function truncate(int $size)
    {
	    return $this->dynamicCall('pg_lo_truncate', func_get_args());
    }

    /**
     * @param string $data
     * @param int|null $length
     * @return int
     * @see pg_lo_write()
     */
    public function write(string $data, int $length = null)
    {
	    return $this->dynamicCall('pg_lo_write', func_get_args());
    }
}
