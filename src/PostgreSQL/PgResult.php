<?php

namespace ResourceClass\PostgreSQL;

use ResourceWrapper\AbstractResourceWrapper;

/**
 * The purpose of this class is to handle all result, as a more convenient way to handle it.
 * @package ResourceClass\PostgreSQL
 * @link https://www.php.net/manual/en/book.pgsql.php
 */
class PgResult extends AbstractResourceWrapper
{

    /**
     * @inheritdoc
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'pgsql result',
        ];
    }

    /**
     * @return int
     * @see pg_affected_rows()
     */
    public function affectedRows()
    {
	    return $this->dynamicCall('pg_affected_rows');
    }

    /**
     * @param int $column
     * @return array
     * @see pg_fetch_all_columns()
     */
    public function fetchAllColumns(int $column = 0)
    {
	    return $this->dynamicCall('pg_fetch_all_columns', func_get_args());
    }

    /**
     * @return array
     * @see pg_fetch_all()
     */
    public function fetchAll()
    {
	    return $this->dynamicCall('pg_fetch_all');
    }

    /**
     * @param int|null $row
     * @param int $resultType
     * @return array
     * @see pg_fetch_array()
     */
    public function fetchArray(int $row = null, int $resultType = PGSQL_BOTH)
    {
	    return $this->dynamicCall('pg_fetch_array', func_get_args());
    }

    /**
     * @param int|null $row
     * @return array
     * @see pg_fetch_assoc()
     */
    public function fetchAssoc(int $row = null)
    {
	    return $this->dynamicCall('pg_fetch_assoc', func_get_args());
    }

    /**
     * @param int|null $row
     * @param null $className
     * @param array $parameters
     * @return object
     * @see pg_fetch_object()
     */
    public function fetchObject(int $row = null, $className = null, array $parameters = [])
    {
	    return $this->dynamicCall('pg_fetch_object', func_get_args());
    }

    /**
     * @param int|null $row
     * @param $field
     * @return string
     * @see pg_fetch_result()
     */
    public function fetchResult(int $row = null, $field)
    {
	    return $this->dynamicCall('pg_fetch_result', func_get_args());
    }

    /**
     * @param int|null $row
     * @return array
     * @see pg_fetch_row()
     */
    public function fetchRow(int $row = null)
    {
	    return $this->dynamicCall('pg_fetch_row', func_get_args());
    }

    /**
     * @param int $row
     * @param $field
     * @return int
     * @see pg_field_is_null()
     */
    public function fieldIsNull(int $row, $field)
    {
	    return $this->dynamicCall('pg_field_is_null', func_get_args());
    }

    /**
     * @param int $fieldNumber
     * @return string
     * @see pg_field_name()
     */
    public function fieldName(int $fieldNumber)
    {
	    return $this->dynamicCall('pg_field_name', func_get_args());
    }

    /**
     * @param string $fieldName
     * @return int
     * @see pg_field_num()
     */
    public function fieldNum(string $fieldName)
    {
	    return $this->dynamicCall('pg_field_num', func_get_args());
    }

    /**
     * @param int|null $rowNumber
     * @param $fieldNameOrNumber
     * @return int
     * @see pg_field_prtlen()
     */
    public function fieldPrtlen(int $rowNumber = null, $fieldNameOrNumber)
    {
	    return $this->dynamicCall('pg_field_prtlen', func_get_args());
    }

    /**
     * @param int $fieldNumber
     * @return int
     * @see pg_field_size()
     */
    public function fieldSize(int $fieldNumber)
    {
	    return $this->dynamicCall('pg_field_size', func_get_args());
    }

    /**
     * @param int $fieldNumber
     * @param bool $oidOnly
     * @return mixed
     * @see pg_field_table()
     */
    public function fieldTable(int $fieldNumber, bool $oidOnly = false)
    {
	    return $this->dynamicCall('pg_field_table', func_get_args());
    }

    /**
     * @param int $fieldNumber
     * @return int
     * @see pg_field_type_oid()
     */
    public function fieldTypeOid(int $fieldNumber)
    {
	    return $this->dynamicCall('pg_field_type_oid', func_get_args());
    }

    /**
     * @param int $fieldNumber
     * @return string
     * @see pg_field_type()
     */
    public function fieldType(int $fieldNumber)
    {
	    return $this->dynamicCall('pg_field_type', func_get_args());
    }

    /**
     * @return bool
     * @see pg_free_result()
     */
    public function freeResult()
    {
	    return $this->dynamicCall('pg_free_result');
    }

    /**
     * @return string
     * @see pg_last_oid()
     */
    public function lastOid()
    {
	    return $this->dynamicCall('pg_last_oid');
    }

    /**
     * @return int
     * @see pg_num_fields()
     */
    public function numFields()
    {
	    return $this->dynamicCall('pg_num_fields');
    }

    /**
     * @return int
     * @see pg_num_rows()
     */
    public function numRows()
    {
	    return $this->dynamicCall('pg_num_rows');
    }

    /**
     * @param int $fieldCode
     * @return string
     * @see pg_result_error_field()
     */
    public function resultErrorField(int $fieldCode)
    {
	    return $this->dynamicCall('pg_result_error_field', func_get_args());
    }

    /**
     * @return string
     * @see pg_result_error()
     */
    public function resultError()
    {
	    return $this->dynamicCall('pg_result_error');
    }

    /**
     * @param int $offset
     * @return bool
     * @see pg_result_seek()
     */
    public function resultSeek(int $offset)
    {
	    return $this->dynamicCall('pg_result_seek', func_get_args());
    }

    /**
     * @param int $type
     * @return mixed
     * @see pg_result_status()
     */
    public function resultStatus(int $type = PGSQL_STATUS_LONG)
    {
	    return $this->dynamicCall('pg_result_status', func_get_args());
    }
}
