<?php

namespace ResourceClass\PostgreSQL;

use ResourceWrapper\AbstractCloseableResourceWrapper;

/**
 * The purpose of this is to use the more specific and faster function of pg_* functions compared to PDO
 * All it does is to reference function within an object containing the pg resource
 * @package ResourceClass\PostgreSQL
 * @link https://www.php.net/manual/en/book.pgsql.php
 */
class PgConnection extends AbstractCloseableResourceWrapper
{
    public const LIBPQ_VERSION = PGSQL_LIBPQ_VERSION;
    public const LIBPQ_VERSION_STR = PGSQL_LIBPQ_VERSION_STR;

    public const ASSOC = PGSQL_ASSOC;
    public const NUM = PGSQL_NUM;
    public const BOTH = PGSQL_BOTH;

    public const CONNECT_FORCE_NEW = PGSQL_CONNECT_FORCE_NEW;
    public const CONNECT_ASYNC = PGSQL_CONNECT_ASYNC;
    public const CONNECTION_BAD = PGSQL_CONNECTION_BAD;
    public const CONNECTION_OK = PGSQL_CONNECTION_OK;

    public const SEEK_SET = PGSQL_SEEK_SET;
    public const SEEK_CUR = PGSQL_SEEK_CUR;
    public const SEEK_END = PGSQL_SEEK_END;

    public const EMPTY_QUERY = PGSQL_EMPTY_QUERY;
    public const COMMAND_OK = PGSQL_COMMAND_OK;
    public const TUPLES_OK = PGSQL_TUPLES_OK;

    public const COPY_OUT = PGSQL_COPY_OUT;
    public const COPY_IN = PGSQL_COPY_IN;

    public const BAD_RESPONSE = PGSQL_BAD_RESPONSE;
    public const NONFATAL_ERROR = PGSQL_NONFATAL_ERROR;
    public const FATAL_ERROR = PGSQL_FATAL_ERROR;

    public const TRANSACTION_IDLE = PGSQL_TRANSACTION_IDLE;
    public const TRANSACTION_ACTIVE = PGSQL_TRANSACTION_ACTIVE;
    public const TRANSACTION_INTRANS = PGSQL_TRANSACTION_INTRANS;
    public const TRANSACTION_INERROR = PGSQL_TRANSACTION_INERROR;
    public const TRANSACTION_UNKNOWN = PGSQL_TRANSACTION_UNKNOWN;

    public const DIAG_SEVERITY = PGSQL_DIAG_SEVERITY;
    public const DIAG_SQLSTATE = PGSQL_DIAG_SQLSTATE;
    public const DIAG_MESSAGE_PRIMARY = PGSQL_DIAG_MESSAGE_PRIMARY;
    public const DIAG_MESSAGE_DETAIL = PGSQL_DIAG_MESSAGE_DETAIL;
    public const DIAG_MESSAGE_HINT = PGSQL_DIAG_MESSAGE_HINT;
    public const DIAG_STATEMENT_POSITION = PGSQL_DIAG_STATEMENT_POSITION;
    public const DIAG_INTERNAL_POSITION = PGSQL_DIAG_INTERNAL_POSITION;
    public const DIAG_INTERNAL_QUERY = PGSQL_DIAG_INTERNAL_QUERY;
    public const DIAG_CONTEXT = PGSQL_DIAG_CONTEXT;
    public const DIAG_SOURCE_FILE = PGSQL_DIAG_SOURCE_FILE;
    public const DIAG_SOURCE_LINE = PGSQL_DIAG_SOURCE_LINE;
    public const DIAG_SOURCE_FUNCTION = PGSQL_DIAG_SOURCE_FUNCTION;

    public const ERRORS_TERSE = PGSQL_ERRORS_TERSE;
    public const ERRORS_DEFAULT = PGSQL_ERRORS_DEFAULT;
    public const ERRORS_VERBOSE = PGSQL_ERRORS_VERBOSE;

    public const NOTICE_LAST = PGSQL_NOTICE_LAST;
    public const NOTICE_ALL = PGSQL_NOTICE_ALL;
    public const NOTICE_CLEAR = PGSQL_NOTICE_CLEAR;

    public const STATUS_LONG = PGSQL_STATUS_LONG;
    public const STATUS_STRING = PGSQL_STATUS_STRING;

    public const CONV_IGNORE_DEFAULT = PGSQL_CONV_IGNORE_DEFAULT;
    public const CONV_FORCE_NULL = PGSQL_CONV_FORCE_NULL;
    public const CONV_IGNORE_NOT_NULL = PGSQL_CONV_IGNORE_NOT_NULL;

    public const DML_NO_CONV = PGSQL_DML_NO_CONV;
    public const DML_EXEC = PGSQL_DML_EXEC;
    public const DML_ASYNC = PGSQL_DML_ASYNC;
    public const DML_STRING = PGSQL_DML_STRING;
    public const DML_ESCAPE = PGSQL_DML_ESCAPE;

    public const POLLING_FAILED = PGSQL_POLLING_FAILED;
    public const POLLING_READING = PGSQL_POLLING_READING;
    public const POLLING_WRITING = PGSQL_POLLING_WRITING;
    public const POLLING_OK = PGSQL_POLLING_OK;
    public const POLLING_ACTIVE = PGSQL_POLLING_ACTIVE;

    public const DIAG_SEVERITY_NONLOCALIZED = PGSQL_DIAG_SEVERITY_NONLOCALIZED;

    /**
     * @param string $connectionString
     * @param int|null $connectType
     * @return static
     * @see pg_connect()
     */
    public static function connect(string $connectionString, int $connectType = null)
    {
        return static::initResource('pg_connect', func_get_args());
    }

    /**
     * @param string $connectionString
     * @param int|null $connectType
     * @return static
     * @see pg_pconnect()
     */
    public static function pconnect(string $connectionString, int $connectType = null)
    {
        return static::initResource('pg_pconnect', func_get_args());
    }

    /**
     * @param string $data
     * @return string
     * @see pg_unescape_bytea()
     */
    public static function unescapeBytea(string $data)
    {
        return static::staticCall('pg_unescape_bytea', func_get_args());
    }

    /**
     * @inheritdoc
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'pgsql link',
            'pgsql link persistent',
        ];
    }

    /**
     * @return bool
     * @see pg_close()
     */
    public function close(): bool
    {
    	return $this->dynamicCall('pg_close');
    }

    /**
     * @return bool
     * @see pg_cancel_query()
     */
    public function cancelQuery()
    {
	    return $this->dynamicCall('pg_cancel_query');
    }

    /**
     * @return string
     * @see pg_client_encoding()
     */
    public function clientEncoding()
    {
	    return $this->dynamicCall('pg_client_encoding');
    }

    /**
     * @return int
     * @see pg_connect_poll()
     */
    public function connectPoll()
    {
	    return $this->dynamicCall('pg_connect_poll');
    }

    /**
     * @return bool
     * @see pg_connection_busy()
     */
    public function connectionBusy()
    {
	    return $this->dynamicCall('pg_connection_busy');
    }

    /**
     * @return bool
     * @see pg_connection_reset()
     */
    public function connectionReset()
    {
	    return $this->dynamicCall('pg_connection_reset');
    }

    /**
     * @return int
     * @see connection_status()
     */
    public function connectionStatus()
    {
	    return $this->dynamicCall('pg_connection_status');
    }

    /**
     * @return bool
     * @see pg_consume_input()
     */
    public function consumeInput()
    {
	    return $this->dynamicCall('pg_consume_input');
    }

    /**
     * @param string $tableName
     * @param array $assocArray
     * @param int $options
     * @return array
     * @see pg_convert()
     */
    public function convert(string $tableName, array $assocArray, int $options = 0)
    {
	    return $this->dynamicCall('pg_convert', func_get_args());
    }

    /**
     * @param string $tableName
     * @param array $rows
     * @param string|null $delimiter
     * @param string|null $nullAs
     * @return bool
     * @see pg_copy_from()
     */
    public function copyFrom(string $tableName, array $rows, string $delimiter = null, string $nullAs = null)
    {
	    return $this->dynamicCall('pg_copy_from', func_get_args());
    }

    /**
     * @param string $tableName
     * @param string|null $delimiter
     * @param string|null $nullAs
     * @return array
     * @see pg_copy_to()
     */
    public function copyTo(string $tableName, string $delimiter = null, string $nullAs = null)
    {
	    return $this->dynamicCall('pg_copy_to', func_get_args());
    }

    /**
     * @return string
     * @see pg_dbname()
     */
    public function databaseName()
    {
	    return $this->dynamicCall('pg_dbname');
    }

    /**
     * @param string $tableName
     * @param array $assocArray
     * @param int $options
     * @return mixed
     * @see pg_delete()
     */
    public function delete(string $tableName, array $assocArray, int $options = PGSQL_DML_EXEC)
    {
	    return $this->dynamicCall('pg_delete', func_get_args());
    }

    /**
     * @return bool
     * @see pg_end_copy()
     */
    public function endCopy()
    {
	    return $this->dynamicCall('pg_end_copy');
    }

    /**
     * @param string $data
     * @return string
     * @see pg_escape_bytea()
     */
    public function escapeBytea(string $data)
    {
	    return $this->dynamicCall('pg_escape_bytea', func_get_args());
    }

    /**
     * @param string $data
     * @return string
     * @see pg_escape_identifier()
     */
    public function escapeIdentifier(string $data)
    {
	    return $this->dynamicCall('pg_escape_identifier', func_get_args());
    }

    /**
     * @param string $data
     * @return string
     * @see pg_escape_literal()
     */
    public function escapeLiteral(string $data)
    {
	    return $this->dynamicCall('pg_escape_literal', func_get_args());
    }

    /**
     * @param string $data
     * @return string
     * @see pg_escape_string()
     */
    public function escapeString(string $data)
    {
	    return $this->dynamicCall('pg_escape_string', func_get_args());
    }

    /**
     * @param string $stmtName
     * @param array $params
     * @return PgResult
     * @see pg_execute()
     */
    public function execute(string $stmtName, array $params)
    {
    	return PgResult::initResource('pg_execute', $this->compileParameters(func_get_args()));
    }

    /**
     * @return mixed
     * @see pg_flush()
     */
    public function flush()
    {
	    return $this->dynamicCall('pg_flush');
    }

    /**
     * @param int|null $resultType
     * @return array
     * @see pg_get_notify()
     */
    public function getNotify(int $resultType = null)
    {
	    return $this->dynamicCall('pg_get_notify', func_get_args());
    }

    /**
     * @return int
     * @see pg_get_pid()
     */
    public function getPid()
    {
	    return $this->dynamicCall('pg_get_pid');
    }

    /**
     * @return PgResult
     * @see pg_get_result()
     */
    public function getResult()
    {
    	return PgResult::initResource('pg_get_result', $this->compileParameters());
    }

    /**
     * @return string
     * @see pg_host()
     */
    public function host()
    {
	    return $this->dynamicCall('pg_host');
    }

    /**
     * @param string $tableName
     * @param array $assocArray
     * @param int $options
     * @return mixed
     * @see pg_insert()
     */
    public function insert(string $tableName, array $assocArray, int $options = PGSQL_DML_EXEC)
    {
	    return $this->dynamicCall('pg_insert', func_get_args());
    }

    /**
     * @return string
     * @see pg_last_error()
     */
    public function lastError()
    {
	    return $this->dynamicCall('pg_last_error');
    }

    /**
     * @return string
     * @see pg_last_notice()
     */
    public function lastNotice()
    {
	    return $this->dynamicCall('pg_last_notice');
    }

    /**
     * @param null $objectId
     * @return int
     * @see pg_lo_create()
     */
    public function loCreate($objectId = null)
    {
	    return $this->dynamicCall('pg_lo_create', func_get_args());
    }

    /**
     * @param int $oid
     * @param string $pathname
     * @return bool
     * @see pg_lo_export()
     */
    public function loExport(int $oid, string $pathname)
    {
	    return $this->dynamicCall('pg_lo_export', func_get_args());
    }

    /**
     * @param string $pathName
     * @param null $objectId
     * @return int
     * @see pg_lo_import()
     */
    public function loImport(string $pathName, $objectId = null)
    {
	    return $this->dynamicCall('pg_lo_import', func_get_args());
    }

    /**
     * @param int $oid
     * @param string $mode
     * @return PgLargeObject
     * @see pg_lo_open()
     */
    public function loOpen(int $oid, string $mode)
    {
    	return PgLargeObject::initResource('pg_lo_open', $this->compileParameters(func_get_args()));
    }

    /**
     * @param int $oid
     * @return bool
     * @see pg_lo_unlink()
     */
    public function loUnlink(int $oid)
    {
	    return $this->dynamicCall('pg_lo_unlink', func_get_args());
    }

    /**
     * @param string $tableName
     * @param bool|null $extended
     * @return array
     * @see pg_meta_data()
     */
    public function metaData(string $tableName, bool $extended = null)
    {
	    return $this->dynamicCall('pg_metadata', func_get_args());
    }

    /**
     * @return string
     * @see pg_options()
     */
    public function options()
    {
	    return $this->dynamicCall('pg_options', func_get_args());
    }

    /**
     * @param string $paramName
     * @return string
     * @see pg_parameter_status()
     */
    public function parameterStatus(string $paramName)
    {
	    return $this->dynamicCall('pg_parameter_status', func_get_args());
    }

    /**
     * @return bool
     * @see pg_ping()
     */
    public function ping()
    {
	    return $this->dynamicCall('pg_ping');
    }

    /**
     * @return int
     * @see pg_ping()
     */
    public function port()
    {
	    return $this->dynamicCall('pg_port');
    }

    /**
     * @param string $stmtName
     * @param string $query
     * @return PgResult
     * @see pg_prepare()
     */
    public function prepare(string $stmtName, string $query)
    {
    	return PgResult::initResource('pg_prepare', $this->compileParameters(func_get_args()));
    }

    /**
     * @param string $data
     * @return bool
     * @see pg_put_line()
     */
    public function putLine(string $data)
    {
	    return $this->dynamicCall('pg_put_line', func_get_args());
    }

    /**
     * @param string $query
     * @param array $params
     * @return PgResult
     * @see pg_query_params()
     */
    public function queryParams(string $query, array $params)
    {
    	return PgResult::initResource('pg_query_param', $this->compileParameters(func_get_args()));
    }

    /**
     * @param string $query
     * @return PgResult
     * @see pg_query()
     */
    public function query(string $query)
    {
    	return PgResult::initResource('pg_query', $this->compileParameters(func_get_args()));
    }

    /**
     * @param string $tableName
     * @param array $assocArray
     * @param int $options
     * @return mixed
     * @see pg_select()
     */
    public function select(string $tableName, array $assocArray, int $options = PGSQL_DML_EXEC)
    {
	    return $this->dynamicCall('pg_select', func_get_args());
    }

    /**
     * @param string $stmtName
     * @param array $params
     * @return bool
     * @see pg_send_execute()
     */
    public function sendExecute(string $stmtName, array $params)
    {
	    return $this->dynamicCall('pg_send_execute', func_get_args());
    }

    /**
     * @param string $stmtName
     * @param string $query
     * @return bool
     * @see pg_send_prepare()
     */
    public function sendPrepare(string $stmtName, string $query)
    {
	    return $this->dynamicCall('pg_send_prepare', func_get_args());
    }

    /**
     * @param string $query
     * @param array $params
     * @return bool
     * @see pg_send_query_params()
     */
    public function sendQueryParams(string $query, array $params)
    {
	    return $this->dynamicCall('pg_send_query_params', func_get_args());
    }

    /**
     * @param string $query
     * @return bool
     * @see pg_send_query()
     */
    public function sendQuery(string $query)
    {
	    return $this->dynamicCall('pg_send_query', func_get_args());
    }

    /**
     * @param string $encoding
     * @return int
     * @see pg_set_client_encoding()
     */
    public function setClientEncoding(string $encoding)
    {
	    return $this->dynamicCall('pg_set_client_encoding', func_get_args());
    }

    /**
     * @param int $verbosity
     * @return int
     * @see pg_set_error_verbosity()
     */
    public function setErrorVerbosity(int $verbosity)
    {
	    return $this->dynamicCall('pg_set_error_verbosity', func_get_args());
    }

    /**
     * @return static
     * @see pg_socket()
     */
    public function socket()
    {
        return static::initResource($this->dynamicCall('pg_socket'));
    }

    /**
     * @param string $pathname
     * @param string $mode
     * @return bool
     * @see pg_trace()
     */
    public function trace(string $pathname, string $mode = 'w')
    {
	    return $this->dynamicCall('pg_trace', func_get_args(), 2);
    }

    /**
     * @return int
     * @see pg_transaction_status()
     */
    public function transactionStatus()
    {
	    return $this->dynamicCall('pg_transaction_status');
    }

    /**
     * @return string
     * @see pg_tty()
     */
    public function tty()
    {
	    return $this->dynamicCall('pg_tty');
    }

    /**
     * @return bool
     * @see pg_untrace()
     */
    public function untrace()
    {
	    return $this->dynamicCall('pg_untrace');
    }

    /**
     * @param string $tableName
     * @param array $data
     * @param array $condition
     * @param $options
     * @return mixed
     * @see pg_update()
     */
    public function update(string $tableName, array $data, array $condition, $options = PGSQL_DML_EXEC)
    {
	    return $this->dynamicCall('pg_update', func_get_args());
    }

    /**
     * @return array
     * @see pg_version()
     */
    public function version()
    {
	    return $this->dynamicCall('pg_version');
    }
}
