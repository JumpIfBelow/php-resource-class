<?php

namespace ResourceClass;

use ResourceWrapper\AbstractCloseableResourceWrapper;

/**
 * Class Image
 * @package ResourceClass
 * @link https://www.php.net/manual/en/book.image.php
 */
class Image extends AbstractCloseableResourceWrapper
{
    public const GD_VERSION = GD_VERSION;
    public const GD_MAJOR_VERSION = GD_MAJOR_VERSION;
    public const GD_MINOR_VERSION = GD_MINOR_VERSION;
    public const GD_RELEASE_VERSION = GD_RELEASE_VERSION;
    public const GD_EXTRA_VERSION = GD_EXTRA_VERSION;
    public const GD_BUNDLED = GD_BUNDLED;

    public const BMP = IMG_BMP;
    public const GIF = IMG_GIF;
    public const JPG = IMG_JPG;
    public const JPEG = IMG_JPEG;
    public const PNG = IMG_PNG;
    public const WBMP = IMG_WBMP;
    public const XPM = IMG_XPM;
    public const WEBP = IMG_WEBP;

    public const COLOR_TILED = IMG_COLOR_TILED;
    public const COLOR_STYLED = IMG_COLOR_STYLED;
    public const COLOR_BRUSHED = IMG_COLOR_BRUSHED;
    public const COLOR_STYLEBRUSHED = IMG_COLOR_STYLEDBRUSHED;
    public const COLOR_TRANSPARENT = IMG_COLOR_TRANSPARENT;

    public const AFFINE_SCALE = IMG_AFFINE_SCALE;
    public const AFFINE_ROTATE  = IMG_AFFINE_ROTATE;
    public const AFFINE_SHEAR_HORIZONTAL = IMG_AFFINE_SHEAR_HORIZONTAL;
    public const AFFINE_SHEAR_VERTICAL = IMG_AFFINE_SHEAR_VERTICAL;

    public const ARC_ROUNDED = IMG_ARC_ROUNDED;
    public const ARC_CHORD = IMG_ARC_CHORD;
    public const ARC_NOFILL = IMG_ARC_NOFILL;
    public const ARC_EDGED = IMG_ARC_EDGED;

    public const GD2_RAW = IMG_GD2_RAW;
    public const GD2_COMPRESSED = IMG_GD2_COMPRESSED;

    public const EFFECT_REPLACE = IMG_EFFECT_REPLACE;
    public const EFFECT_ALPHABLEND = IMG_EFFECT_ALPHABLEND;
    public const EFFECT_NORMAL = IMG_EFFECT_NORMAL;
    public const EFFECT_OVERLAY = IMG_EFFECT_OVERLAY;
    public const EFFECT_MULTIPLY = IMG_EFFECT_MULTIPLY;

    public const FILTER_NEGATE = IMG_FILTER_NEGATE;
    public const FILTER_GRAYSCALE = IMG_FILTER_GRAYSCALE;
    public const FILTER_BRIGHTNESS = IMG_FILTER_BRIGHTNESS;
    public const FILTER_CONTRAST = IMG_FILTER_CONTRAST;
    public const FILTER_COLORIZE = IMG_FILTER_COLORIZE;
    public const FILTER_EDGEDETECT = IMG_FILTER_EDGEDETECT;
    public const FILTER_GAUSSIAN_BLUR = IMG_FILTER_GAUSSIAN_BLUR;
    public const FILTER_SELECTIVE_BLUR = IMG_FILTER_SELECTIVE_BLUR;
    public const FILTER_EMBOSS = IMG_FILTER_EMBOSS;
    public const FILTER_MEAN_REMOVAL = IMG_FILTER_MEAN_REMOVAL;
    public const FILTER_SMOOTH = IMG_FILTER_SMOOTH;
    public const FILTER_PIXELATE = IMG_FILTER_PIXELATE;
    public const FILTER_SCATTER = IMG_FILTER_SCATTER;

    public const TYPE_GIF = IMAGETYPE_GIF;
    public const TYPE_JPEG = IMAGETYPE_JPEG;
    public const TYPE_JPEG2000 = IMAGETYPE_JPEG2000;
    public const TYPE_PNG = IMAGETYPE_PNG;
    public const TYPE_SWF = IMAGETYPE_SWF;
    public const TYPE_PSD = IMAGETYPE_PSD;
    public const TYPE_BMP = IMAGETYPE_BMP;
    public const TYPE_WBMP = IMAGETYPE_WBMP;
    public const TYPE_XBM = IMAGETYPE_XBM;
    public const TYPE_TIFF_II = IMAGETYPE_TIFF_II;
    public const TYPE_TIFF_MM = IMAGETYPE_TIFF_MM;
    public const TYPE_IFF = IMAGETYPE_IFF;
    public const TYPE_JB2 = IMAGETYPE_JB2;
    public const TYPE_JPX = IMAGETYPE_JPX;
    public const TYPE_SWC = IMAGETYPE_SWC;
    public const TYPE_ICO = IMAGETYPE_ICO;
    public const TYPE_WEBP = IMAGETYPE_WEBP;

    public const PNG_NO_FILTER = PNG_NO_FILTER;
    public const PNG_FILTER_NONE = PNG_FILTER_NONE;
    public const PNG_FILTER_SUB = PNG_FILTER_SUB;
    public const PNG_FILTER_UP = PNG_FILTER_UP;
    public const PNG_FILTER_AVG = PNG_FILTER_AVG;
    public const PNG_FILTER_PAETH = PNG_FILTER_PAETH;
    public const PNG_ALL_FILTERS = PNG_ALL_FILTERS;

    public const FLIP_VERTICAL = IMG_FLIP_VERTICAL;
    public const FLIP_HORIZONTAL = IMG_FLIP_HORIZONTAL;
    public const FLIP_BOTH = IMG_FLIP_BOTH;

    public const BELL = IMG_BELL;
    public const BESSEL = IMG_BESSEL;
    public const BILINEAR_FIXED = IMG_BILINEAR_FIXED;
    public const BICUBIC = IMG_BICUBIC;
    public const BLACKMAN = IMG_BLACKMAN;
    public const BOX = IMG_BOX;
    public const BSPLINE = IMG_BSPLINE;
    public const CATMULLROM = IMG_CATMULLROM;
    public const GAUSSIAN = IMG_GAUSSIAN;
    public const GENERALIZED_CUBIC = IMG_GENERALIZED_CUBIC;
    public const HERMITE = IMG_HERMITE;
    public const HAMMING = IMG_HAMMING;
    public const HANNING = IMG_HANNING;
    public const MITCHELL = IMG_MITCHELL;
    public const POWER = IMG_POWER;
    public const QUADRATIC = IMG_QUADRATIC;
    public const SINC = IMG_SINC;
    public const NEAREST_NEIGHBOUR = IMG_NEAREST_NEIGHBOUR;
    public const WEIGHTED4 = IMG_WEIGHTED4;
    public const TRIANGLE = IMG_TRIANGLE;

    /**
     * @param string $filename
     * @param array $imageInfo
     * @return array
     * @see getimagesize()
     */
    public static function getSize(string $filename, array &$imageInfo = null)
    {
        return static::staticCall('getimagesize', func_get_args());
    }

    /**
     * @param string $imageData
     * @param array|null $imageInfo
     * @return array
     * @see getimagesizefromstring()
     */
    public static function getSizeFromString(string $imageData, array &$imageInfo = null)
    {
        return static::staticCall('getimagesizefromstring', func_get_args());
    }

    /**
     * @param int $imageType
     * @param bool $includeDot
     * @return string
     * @see image_type_to_extension()
     */
    public static function typeToExtension(int $imageType, bool $includeDot = true)
    {
        return static::staticCall('image_type_to_extension', func_get_args());
    }

    /**
     * @param int $imageType
     * @return string
     * @see image_type_to_mime_type()
     */
    public static function typeToMimeType(int $imageType)
    {
        return static::staticCall('image_type_to_mime_type', func_get_args());
    }

    /**
     * @param int $width
     * @param int $height
     * @return static
     * @see imagecreate()
     */
    public static function create(int $width, int $height)
    {
        return static::initResource('imagecreate', func_get_args());
    }

    /**
     * @param string $filename
     * @return static
     * @see imagecreatefromgd2()
     */
    public static function createFromGd2(string $filename)
    {
        return static::initResource('imagecreatefromgd2', func_get_args());
    }

    /**
     * @param string $filename
     * @param int $sourceX
     * @param int $sourceY
     * @param int $width
     * @param int $height
     * @return static
     * @see imagecreatefromgd2part()
     */
    public static function createFromGd2Part(string $filename, int $sourceX, int $sourceY, int $width, int $height)
    {
        return static::initResource('imagecreatefromgd2part', func_get_args());
    }

    /**
     * @param string $filename
     * @return static
     * @see imagecreatefromgd()
     */
    public static function createFromGd(string $filename)
    {
        return static::initResource('imagecreatefromgd', func_get_args());
    }

    /**
     * @param string $filename
     * @return static
     * @see imagecreatefromgif()
     */
    public static function createFromGif(string $filename)
    {
        return static::initResource('imagecreatefromgif', func_get_args());
    }

    /**
     * @param string $filename
     * @return static
     * @see imagecreatefromjpeg()
     */
    public static function createFromJpeg(string $filename)
    {
        return static::initResource('imagecreatefromjpeg', func_get_args());
    }

    /**
     * @param string $filename
     * @return static
     * @see imagecreatefrompng()
     */
    public static function createFromPng(string $filename)
    {
        return static::initResource('imagecreatefrompng', func_get_args());
    }

    /**
     * @param string $image
     * @return static
     * @see imagecreatefromstring()
     */
    public static function createFromString(string $image)
    {
        return static::initResource('imagecreatefromstring', func_get_args());
    }

    /**
     * @param string $filename
     * @return static
     * @see imagecreatefromwbmp()
     */
    public static function createFromWbmp(string $filename)
    {
        return static::initResource('imagecreatefromwbmp', func_get_args());
    }

    /**
     * @param string $filename
     * @return static
     * @see imagecreatefromwebp()
     */
    public static function createFromWebP(string $filename)
    {
        return static::initResource('imagecreatefromwebp', func_get_args());
    }

    /**
     * @param string $filename
     * @return static
     * @see imagecreatefromxbm()
     */
    public static function createFromXbm(string $filename)
    {
        return static::initResource('imagecreatefromxbm', func_get_args());
    }

    /**
     * @param string $filename
     * @return static
     * @see imagecreatefromxpm()
     */
    public static function createFromXpm(string $filename)
    {
        return static::initResource('imagecreatefromxpm', func_get_args());
    }

    /**
     * @param int $width
     * @param int $height
     * @return static
     * @see imagecreatetruecolor()
     */
    public static function createTrueColor(int $width, int $height)
    {
        return static::initResource('imagecreatetruecolor', func_get_args());
    }

    /**
     * @param int $font
     * @return int
     * @see imagefontheight()
     */
    public static function fontHeight(int $font)
    {
        return static::staticCall('imagefontheight', func_get_args());
    }

    /**
     * @param int $font
     * @return int
     * @see imagefontwidth()
     */
    public static function fontWidth(int $font)
    {
        return static::staticCall('imagefontwidth', func_get_args());
    }

    /**
     * @param float $size
     * @param float $angle
     * @param string $fontFile
     * @param string $text
     * @param array|null $extraInfo
     * @return array
     * @see imageftbbox()
     */
    public static function ftbBox(float $size, float $angle, string $fontFile, string $text, array $extraInfo = null)
    {
        return static::staticCall('imageftbbox', func_get_args());
    }

    /**
     * @param string $file
     * @return int
     * @see imageloadfont()
     */
    public static function loadFont(string $file)
    {
        return static::staticCall('imageloadfont', func_get_args());
    }

    /**
     * @param float $size
     * @param float $angle
     * @param string $fontFile
     * @param string $text
     * @return array
     * @see imagettfbbox()
     */
    public static function ttfbBox(float $size, float $angle, string $fontFile, string $text)
    {
        return static::staticCall('imagettfbbox', func_get_args());
    }

    /**
     * @return int
     * @see imagetypes()
     */
    public static function types()
    {
        return static::staticCall('imagetypes', func_get_args());
    }

    /**
     * @param string $iptcData
     * @param string $jpegFileName
     * @param int $spool
     * @return static|string|false
     * @see iptcembed()
     */
    public static function iptcEmbed(string $iptcData, string $jpegFileName, int $spool = 0)
    {
        if ($spool < 2) {
    		return static::initResource('iptcembed', func_get_args());
	    } else {
    		return static::staticCall('iptcembed', func_get_args());
	    }
    }

    /**
     * @param string $iptcBlock
     * @return array
     * @see iptcparse()
     */
    public static function iptcParse(string $iptcBlock)
    {
        return static::staticCall('iptcparse', func_get_args());
    }

    /**
     * @param string $jpegName
     * @param string $wbmpName
     * @param int $destHeigth
     * @param int $destWidth
     * @param int $threshold
     * @return bool
     * @see jpeg2wbmp()
     */
    public static function jpegTowbmp(string $jpegName, string $wbmpName, int $destHeigth, int $destWidth, int $threshold)
    {
    	return static::staticCall('jpeg2wbmp', func_get_args());
    }

    /**
     * @param string $pngName
     * @param string $wbmpName
     * @param int $destHeight
     * @param int $destWidth
     * @param int $threshold
     * @return bool
     * @see png2wbmp()
     */
    public static function pngToWbmp(string $pngName, string $wbmpName, int $destHeight, int $destWidth, int $threshold)
    {
    	return static::staticCall('png2wbmp', func_get_args());
    }

    /**
     * @return static
     * @see imagegrabscreen()
     */
    public static function grabScreen()
    {
        return static::initResource('imagegrabscreen');
    }

    /**
     * @param int $windowHandle
     * @param int $clientArea
     * @return static
     * @see imagegrabwindow()
     */
    public static function grabWindow(int $windowHandle, int $clientArea = 0)
    {
        return static::initResource('imagegrabwindow', func_get_args());
    }

    /**
     * @inheritdoc
     */
    protected static function getAcceptedResources(): array
    {
        return [
            'gd',
        ];
    }

    /**
     * Alias of {@see Image::destroy()}
     * @return bool
     * @see Image::destroy()
     */
    public function close(): bool
    {
        return $this->destroy();
    }

    /**
     * @param string|null $filename
     * @param int|null $threshold
     * @return bool
     * @see image2wbmp()
     */
    public function toWbmp(string $filename = null, int $threshold = null)
    {
	    return $this->dynamicCall('image2wbmp', func_get_args());
    }

    /**
     * @param array $affine
     * @param array|null $clip
     * @return static
     * @see imageaffine()
     */
    public function affine(array $affine, array $clip = null)
    {
    	return static::initResource('imageaffine', $this->compileParameters(func_get_args()));
    }

    /**
     * @param bool $blendMode
     * @return bool
     * @see imagealphablending()
     */
    public function alphaBlending(bool $blendMode)
    {
	    return $this->dynamicCall('imagealphablending', func_get_args());
    }

    /**
     * @param bool $enabled
     * @return bool
     * @see imageantialias()
     */
    public function antialias(bool $enabled)
    {
	    return $this->dynamicCall('imageantialias', func_get_args());
    }

    /**
     * @param int $cx
     * @param int $cy
     * @param int $width
     * @param int $height
     * @param int $start
     * @param int $end
     * @param int $color
     * @return bool
     * @see imagearc()
     */
    public function arc(int $cx, int $cy, int $width, int $height, int $start, int $end, int $color)
    {
	    return $this->dynamicCall('imagearc', func_get_args());
    }

    /**
     * @param int $font
     * @param int $x
     * @param int $y
     * @param string $c
     * @param int $color
     * @return bool
     * @see imagechar()
     */
    public function char(int $font, int $x, int $y, string $c, int $color)
    {
	    return $this->dynamicCall('imagechar', func_get_args());
    }

    /**
     * @param int $font
     * @param int $x
     * @param int $y
     * @param string $c
     * @param int $color
     * @return bool
     * @see imagecharup()
     */
    public function charUp(int $font, int $x, int $y, string $c, int $color)
    {
	    return $this->dynamicCall('imagecharup', func_get_args());
    }

    /**
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return int
     * @see imagecolorallocate()
     */
    public function colorAllocate(int $red, int $green, int $blue)
    {
	    return $this->dynamicCall('imagecolorallocate', func_get_args());
    }

    /**
     * @param int $red
     * @param int $green
     * @param int $blue
     * @param int $alpha
     * @return int
     * @see imagecolorallocatealpha()
     */
    public function colorAllocateAlpha(int $red, int $green, int $blue, int $alpha)
    {
	    return $this->dynamicCall('imagecolorallocatealpha', func_get_args());
    }

    /**
     * @param int $x
     * @param int $y
     * @return int
     * @see imagecoloract()
     */
    public function colorAt(int $x, int $y)
    {
	    return $this->dynamicCall('imagecolorat', func_get_args());
    }

    /**
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return int
     * @see imagecolorclosest()
     */
    public function colorClosest(int $red, int $green, int $blue)
    {
	    return $this->dynamicCall('imagecolorclosest', func_get_args());
    }

    /**
     * @param int $red
     * @param int $green
     * @param int $blue
     * @param int $alpha
     * @return int
     * @see imagecolorclosestalpha()
     */
    public function colorClosestAlpha(int $red, int $green, int $blue, int $alpha)
    {
	    return $this->dynamicCall('imagecolorclosestalpha', func_get_args());
    }

    /**
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return int
     * @see imagecolorclosesthwb()
     */
    public function colorClosestHwb(int $red, int $green, int $blue)
    {
	    return $this->dynamicCall('imagecolorclosesthwb', func_get_args());
    }

    /**
     * @param int $color
     * @return bool
     * @see imagecolordeallocate()
     */
    public function colorDeallocate(int $color)
    {
	    return $this->dynamicCall('imagecolordeallocate', func_get_args());
    }

    /**
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return int
     * @see imagecolorexact()
     */
    public function colorExact(int $red, int $green, int $blue)
    {
	    return $this->dynamicCall('imagecolorexact', func_get_args());
    }

    /**
     * @param int $red
     * @param int $green
     * @param int $blue
     * @param int $alpha
     * @return int
     * @see imagecolorexactalpha()
     */
    public function colorExactAlpha(int $red, int $green, int $blue, int $alpha)
    {
	    return $this->dynamicCall('imagecolorexactalpha', func_get_args());
    }

    /**
     * @param self $image
     * @return bool
     * @see imagecolormatch()
     */
    public function colorMatch(self $image)
    {
	    return $this->dynamicCall('imagecolormatch', func_get_args());
    }

    /**
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return int
     * @see imagecolorresolve()
     */
    public function colorResolve(int $red, int $green, int $blue)
    {
	    return $this->dynamicCall('imagecolorresolve', func_get_args());
    }

    /**
     * @param int $red
     * @param int $green
     * @param int $blue
     * @param int $alpha
     * @return int
     * @see imagecolorresolvealpha()
     */
    public function colorResolveAlpha(int $red, int $green, int $blue, int $alpha)
    {
	    return $this->dynamicCall('imagecolorresolvealpha', func_get_args());
    }

    /**
     * @param int $index
     * @param int $red
     * @param int $green
     * @param int $blue
     * @param int $alpha
     * @see imagecolorset()
     */
    public function colorSet(int $index, int $red, int $green, int $blue, int $alpha = 0)
    {
	    $this->dynamicCall('imagecolorset', func_get_args());
    }

    /**
     * @param int $index
     * @return array
     * @see imagecolorsforindex()
     */
    public function colorsForIndex(int $index)
    {
	    return $this->dynamicCall('imagecolorsforindex', func_get_args());
    }

    /**
     * @return int
     * @see imagecolorstotal()
     */
    public function colorsTotal()
    {
	    return $this->dynamicCall('imagecolorstotal');
    }

    /**
     * @param int|null $color
     * @return int
     * @see imagecolortransparent()
     */
    public function colorTransparent(int $color = null)
    {
	    return $this->dynamicCall('imagecolortransparent', func_get_args());
    }

    /**
     * @param array $matrix
     * @param float $div
     * @param float $offset
     * @return bool
     * @see imageconvolution()
     */
    public function convolution(array $matrix, float $div, float $offset)
    {
	    return $this->dynamicCall('imageconvolution', func_get_args());
    }

    /**
     * @param self $sourceImage
     * @param int $destinationX
     * @param int $destinationY
     * @param int $sourceX
     * @param int $sourceY
     * @param int $sourceW
     * @param int $sourceH
     * @return bool
     * @see imagecopy()
     */
    public function copy(self $sourceImage, int $destinationX, int $destinationY, int $sourceX, int $sourceY, int $sourceW, int $sourceH)
    {
	    return $this->dynamicCall('imagecopy', func_get_args());
    }

    /**
     * @param self $sourceImage
     * @param int $destinationX
     * @param int $destinationY
     * @param int $sourceX
     * @param int $sourceY
     * @param int $sourceW
     * @param int $sourceH
     * @param int $pct
     * @return bool
     * @see imagecopymerge()
     */
    public function copyMerge(self $sourceImage, int $destinationX, int $destinationY, int $sourceX, int $sourceY, int $sourceW, int $sourceH, int $pct)
    {
	    return $this->dynamicCall('imagecopymerge', func_get_args());
    }

    /**
     * @param self $sourceImage
     * @param int $destinationX
     * @param int $destinationY
     * @param int $sourceX
     * @param int $sourceY
     * @param int $sourceW
     * @param int $sourceH
     * @param int $pct
     * @return bool
     * @see imagecopymergegray()
     */
    public function copyMergeGray(self $sourceImage, int $destinationX, int $destinationY, int $sourceX, int $sourceY, int $sourceW, int $sourceH, int $pct)
    {
	    return $this->dynamicCall('imagecopymergegray', func_get_args());
    }

    /**
     * @param self $sourceImage
     * @param int $destinationX
     * @param int $destinationY
     * @param int $sourceX
     * @param int $sourceY
     * @param int $destinationW
     * @param int $destinationH
     * @param int $sourceW
     * @param int $sourceH
     * @return bool
     * @see imagecopyresampled()
     */
    public function copyResampled(self $sourceImage, int $destinationX, int $destinationY, int $sourceX, int $sourceY, int $destinationW, int $destinationH, int $sourceW, int $sourceH)
    {
	    return $this->dynamicCall('imagecopyresampled', func_get_args());
    }

    /**
     * @param self $sourceImage
     * @param int $destinationX
     * @param int $destinationY
     * @param int $sourceX
     * @param int $sourceY
     * @param int $destinationW
     * @param int $destinationH
     * @param int $sourceW
     * @param int $sourceH
     * @return bool
     * @see imagecopyresized()
     */
    public function copyResized(self $sourceImage, int $destinationX, int $destinationY, int $sourceX, int $sourceY, int $destinationW, int $destinationH, int $sourceW, int $sourceH)
    {
	    return $this->dynamicCall('imagecopyresized', func_get_args());
    }

    /**
     * @param array $rect
     * @return static
     * @see imagecrop()
     */
    public function crop(array $rect)
    {
    	return static::initResource('imagecrop', $this->compileParameters(func_get_args()));
    }

    /**
     * @param int $mode
     * @param float $threshold
     * @param int $color
     * @return static
     * @see imagecropauto()
     */
    public function cropAuto(int $mode = -1, float $threshold = .5, int $color = -1)
    {
    	return static::initResource('imagecropauto', $this->compileParameters(func_get_args()));
    }

    /**
     * @param int $x1
     * @param int $y1
     * @param int $x2
     * @param int $y2
     * @param int $color
     * @return bool
     * @see imagedashedline()
     */
    public function dashedLine(int $x1, int $y1, int $x2, int $y2, int $color)
    {
	    return $this->dynamicCall('imagedashedline', func_get_args());
    }

    /**
     * @return bool
     * @see imagedestroy()
     */
    public function destroy()
    {
	    return $this->dynamicCall('imagedestroy');
    }

    /**
     * @param int $cX
     * @param int $cY
     * @param int $width
     * @param int $height
     * @param int $color
     * @return bool
     * @see imageellipse()
     */
    public function ellipse(int $cX, int $cY, int $width, int $height, int $color)
    {
	    return $this->dynamicCall('imageellipse', func_get_args());
    }

    /**
     * @param int $x
     * @param int $y
     * @param int $color
     * @return bool
     * @see imagefill()
     */
    public function fill(int $x, int $y, int $color)
    {
	    return $this->dynamicCall('imagefill', func_get_args());
    }

    /**
     * @param int $cX
     * @param int $cY
     * @param int $width
     * @param int $height
     * @param int $start
     * @param int $end
     * @param int $color
     * @param int $style
     * @return bool
     * @see imagefilledarc()
     */
    public function filledArc(int $cX, int $cY, int $width, int $height, int $start, int $end, int $color, int $style)
    {
	    return $this->dynamicCall('imagefilledarc', func_get_args());
    }

    /**
     * @param int $cX
     * @param int $cY
     * @param int $width
     * @param int $height
     * @param int $color
     * @return bool
     * @see imagefilledellipse()
     */
    public function filledEllipse(int $cX, int $cY, int $width, int $height, int $color)
    {
	    return $this->dynamicCall('imagefilledellipse', func_get_args());
    }

    /**
     * @param array $points
     * @param int $numPoints
     * @param int $color
     * @return bool
     * @see imagefilledpolygon()
     */
    public function filledPolygon(array $points, int $numPoints, int $color)
    {
	    return $this->dynamicCall('imagefilledpolygon', func_get_args());
    }

    /**
     * @param int $x1
     * @param int $y1
     * @param int $x2
     * @param int $y2
     * @param int $color
     * @return bool
     * @see imagefilledrectangle()
     */
    public function filledRectangle(int $x1, int $y1, int $x2, int $y2, int $color)
    {
	    return $this->dynamicCall('imagefilledrectangle', func_get_args());
    }

    /**
     * @param int $x
     * @param int $y
     * @param int $border
     * @param int $color
     * @return bool
     * @see imagefilltoborder()
     */
    public function fillToBorder(int $x, int $y, int $border, int $color)
    {
	    return $this->dynamicCall('imagefilltoborder', func_get_args());
    }

    /**
     * @param int $filterType
     * @param int|null $arg1
     * @param int|null $arg2
     * @param int|null $arg3
     * @param int|null $arg4
     * @return bool
     * @see imagefilter()
     */
    public function filter(int $filterType, int $arg1 = null, int $arg2 = null, int $arg3 = null, int $arg4 = null)
    {
	    return $this->dynamicCall('imagefilter', func_get_args());
    }

    /**
     * @param int $mode
     * @return bool
     * @see imageflip()
     */
    public function flip(int $mode)
    {
	    return $this->dynamicCall('imageflip', func_get_args());
    }

    /**
     * @param float $size
     * @param float $angle
     * @param int $x
     * @param int $y
     * @param int $color
     * @param string $fontFile
     * @param string $text
     * @param array|null $extraInfo
     * @return array
     * @see imagefttext()
     */
    public function ftText(float $size, float $angle, int $x, int $y, int $color, string $fontFile, string $text, array $extraInfo = null)
    {
	    return $this->dynamicCall('imagefttext', func_get_args());
    }

    /**
     * @param float $inputGamma
     * @param float $outputGamma
     * @return bool
     * @see imagegammacorrect()
     */
    public function gammaCorrect(float $inputGamma, float $outputGamma)
    {
	    return $this->dynamicCall('imagegammacorrect', func_get_args());
    }

    /**
     * @param File|string|null $to
     * @param int|null $chunkSize
     * @param int|null $type
     * @return bool
     * @see imagegd2()
     */
    public function gd2($to = null, int $chunkSize = null, int $type = null)
    {
	    return $this->dynamicCall('imagegd2', func_get_args());
    }

    /**
     * @param File|string|null $to
     * @return bool
     * @see imagegd()
     */
    public function gd($to = null)
    {
	    return $this->dynamicCall('imagegd', func_get_args());
    }

    /**
     * @param File|string|null $to
     * @return bool
     * @see imagegif()
     */
    public function gif($to = null)
    {
	    return $this->dynamicCall('imagegif', func_get_args());
    }

    /**
     * @param int $interlace
     * @return int
     * @see imageinterlace()
     */
    public function interlace(int $interlace = 0)
    {
	    return $this->dynamicCall('imageinterlace', func_get_args());
    }

    /**
     * @return bool
     * @see imageistruecolor()
     */
    public function isTrueColor()
    {
	    return $this->dynamicCall('imageistruecolor', func_get_args());
    }

    /**
     * @param File|string|null $to
     * @param int|null $quality
     * @return bool
     * @see imagejpeg()
     */
    public function jpeg($to = null, int $quality = null)
    {
	    return $this->dynamicCall('imagejpeg', func_get_args());
    }

    /**
     * @param int $effect
     * @return bool
     * @see imagelayereffect()
     */
    public function layerEffect(int $effect)
    {
	    return $this->dynamicCall('imagelayereffect', func_get_args());
    }

    /**
     * @param int $x1
     * @param int $y1
     * @param int $x2
     * @param int $y2
     * @param int $color
     * @return bool
     * @see imageline()
     */
    public function line(int $x1, int $y1, int $x2, int $y2, int $color)
    {
	    return $this->dynamicCall('imageline', func_get_args());
    }

    /**
     * @param self $source
     * @see imagepalettecopy()
     */
    public function paletteCopy(self $source)
    {
	    $this->dynamicCall('imagepalettecopy', func_get_args());
    }

    /**
     * @return bool
     * @see imagepalettetotruecolor()
     */
    public function paletteToTrueColor()
    {
	    return $this->dynamicCall('imagepalettetotruecolor');
    }

    /**
     * @param File|string|null $to
     * @param int|null $quality
     * @param int|null $filters
     * @return bool
     * @see imagepng()
     */
    public function png($to = null, int $quality = null, int $filters = null)
    {
	    return $this->dynamicCall('imagepng', func_get_args());
    }

    /**
     * @param array $points
     * @param int $numPoints
     * @param int $color
     * @return bool
     * @see imagepolygon()
     */
    public function polygon(array $points, int $numPoints, int $color)
    {
	    return $this->dynamicCall('imagepolygon', func_get_args());
    }

    /**
     * @param int $x1
     * @param int $y1
     * @param int $x2
     * @param int $y2
     * @param int $color
     * @return bool
     * @see imagerectangle()
     */
    public function rectangle(int $x1, int $y1, int $x2, int $y2, int $color)
    {
	    return $this->dynamicCall('imagerectangle', func_get_args());
    }

    /**
     * @param float $angle
     * @param int $backgroundColor
     * @param int $ignoreTransparent
     * @return resource
     * @see imagerotate()
     */
    public function rotate(float $angle, int $backgroundColor, int $ignoreTransparent = 0)
    {
	    return $this->dynamicCall('imagerotate', func_get_args());
    }

    /**
     * @param bool $saveFlag
     * @return bool
     * @see imagesavealpha()
     */
    public function saveAlpha(bool $saveFlag)
    {
	    return $this->dynamicCall('imagesavealpha', func_get_args());
    }

    /**
     * @param int $newWidth
     * @param int $newHeight
     * @param int $mode
     * @return static
     * @see imagescale()
     */
    public function scale(int $newWidth, int $newHeight = -1, int $mode = IMG_BILINEAR_FIXED)
    {
    	return static::initResource('imagescale', $this->compileParameters(func_get_args()));
    }

    /**
     * @param self $brush
     * @return bool
     * @see imagesetbrush()
     */
    public function setBrush(self $brush)
    {
	    return $this->dynamicCall('imagesetbrush', func_get_args());
    }

    /**
     * @param int $method
     * @return bool
     * @see imagesetinterpolation()
     */
    public function setInterpolation(int $method = IMG_BILINEAR_FIXED)
    {
	    return $this->dynamicCall('imagesetinterpolation', func_get_args());
    }

    /**
     * @param int $x
     * @param int $y
     * @param int $color
     * @return bool
     * @see imagesetpixel()
     */
    public function setPixel(int $x, int $y, int $color)
    {
	    return $this->dynamicCall('imagesetpixel', func_get_args());
    }

    /**
     * @param array $style
     * @return bool
     * @see imagesetstyle()
     */
    public function setStyle(array $style)
    {
	    return $this->dynamicCall('imagesetstyle', func_get_args());
    }

    /**
     * @param int $thickness
     * @return bool
     * @see imagesetthickness()
     */
    public function setThickness(int $thickness)
    {
	    return $this->dynamicCall('imagesetthickness', func_get_args());
    }

    /**
     * @param self $tile
     * @return bool
     * @see imagesettile()
     */
    public function setTile(self $tile)
    {
	    return $this->dynamicCall('imagecopymerge', func_get_args());
    }

    /**
     * @param int $font
     * @param int $x
     * @param int $y
     * @param string $string
     * @param int $color
     * @return bool
     * @see imagestring()
     */
    public function string(int $font, int $x, int $y, string $string, int $color)
    {
	    return $this->dynamicCall('imagestring', func_get_args());
    }

    /**
     * @param int $font
     * @param int $x
     * @param int $y
     * @param string $string
     * @param int $color
     * @return bool
     * @see imagestringup()
     */
    public function stringUp(int $font, int $x, int $y, string $string, int $color)
    {
	    return $this->dynamicCall('imagestringup', func_get_args());
    }

    /**
     * @return int
     * @see imagesx()
     */
    public function sx()
    {
	    return $this->dynamicCall('imagesx');
    }

    /**
     * @return int
     * @see imagesy()
     */
    public function sy()
    {
	    return $this->dynamicCall('imagesy');
    }

    /**
     * @param bool $dither
     * @param int $nColors
     * @return bool
     * @see imagetruecolortopalette()
     */
    public function trueColorToPalette(bool $dither, int $nColors)
    {
	    return $this->dynamicCall('imagetruecolortopalette', func_get_args());
    }

    /**
     * @param float $size
     * @param float $angle
     * @param int $x
     * @param int $y
     * @param int $color
     * @param string $fontFile
     * @param string $text
     * @return array
     * @see imagettftext()
     */
    public function ttfText(float $size, float $angle, int $x, int $y, int $color, string $fontFile, string $text)
    {
	    return $this->dynamicCall('imagettftext', func_get_args());
    }

    /**
     * @param File|string|null $to
     * @param int|null $foreground
     * @return bool
     * @see imagewbmp()
     */
    public function wbmp($to = null, int $foreground = null)
    {
	    return $this->dynamicCall('imagewbmp', func_get_args());
    }

    /**
     * @param File|string|null $to
     * @param int $quality
     * @return bool
     * @see imagewebp()
     */
    public function webP($to = null, int $quality = 80)
    {
	    return $this->dynamicCall('imagewebp', func_get_args());
    }

    /**
     * @param string $filename
     * @param int|null $foreground
     * @return bool
     * @see imagexbm()
     */
    public function xbm(string $filename = null, int $foreground = null)
    {
	    return $this->dynamicCall('imagexbm', func_get_args());
    }
}
