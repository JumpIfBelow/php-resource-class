<?php

namespace ResourceClass;

use ResourceWrapper\AbstractCloseableResourceWrapper;

/**
 * Class Process
 * @package ResourceClass
 * @link https://www.php.net/manual/en/book.exec.php
 */
class Process extends AbstractCloseableResourceWrapper
{
	/**
	 * @param string $arg
	 * @return string
	 * @see escapeshellarg()
	 */
	public static function escapeShellArg(string $arg)
	{
		return static::staticCall('escapeshellarg', func_get_args());
	}

	/**
	 * @param string $command
	 * @return string
	 * @see escapeshellcmd()
	 */
	public static function escapeShellCmd(string $command)
	{
		return static::staticCall('escapeshellcmd', func_get_args());
	}

	/**
	 * @param string $command
	 * @param array|null $output
	 * @param int $returnVar
	 * @return string
	 * @see exec()
	 */
	public static function execute(string $command, array &$output = null, int &$returnVar = null)
	{
		return static::staticCall('exec', func_get_args());
	}

	/**
	 * @param int $increment
	 * @return bool
	 * @see proc_nice()
	 */
	public static function nice(int $increment)
	{
		return static::staticCall('proc_nice', func_get_args());
	}

	/**
	 * @param string $command
	 * @param int|null $returnVar
	 * @return void
	 * @see passthru()
	 */
	public static function passThrough(string $command, int &$returnVar = null)
	{
		static::staticCall('passthru', func_get_args());
	}

	/**
	 * @param string $command
	 * @param array $descriptorSpec
	 * @param array|File[] $pipes
	 * @param string|null $cwd
	 * @param array|null $env
	 * @param array|null $otherOptions
	 * @return static
	 * @see proc_open()
	 */
	public static function open(string $command, array $descriptorSpec, &$pipes, string $cwd = null, array $env = null, array $otherOptions = null)
	{
		return static::initResource('proc_open', func_get_args());
	}

	/**
	 * @param string $command
	 * @return string
	 * @see shell_exec()
	 */
	public static function shellExecute(string $command)
	{
		return static::staticCall('shell_exec', func_get_args());
	}

	/**
	 * @param string $command
	 * @param int|null $returnVar
	 * @return string
	 * @see system()
	 */
	public static function system(string $command, int &$returnVar = null)
	{
		return static::staticCall('system', func_get_args());
	}

	/**
	 * @inheritdoc
	 */
	protected static function getAcceptedResources(): array
	{
		return [
			'process',
		];
	}

	/**
	 * @return array
	 * @see proc_get_status()
	 */
	public function getStatus()
	{
		return $this->dynamicCall('proc_get_status');
	}

	/**
	 * @param int $signal
	 * @return bool
	 * @see proc_terminate()
	 */
	public function terminate(int $signal = 15)
	{
		return $this->dynamicCall('proc_terminate', func_get_args());
	}

	/**
	 * @inheritdoc
	 * @see proc_close()
	 */
	public function close(): bool
	{
		return $this->dynamicCall('proc_close') === 0;
	}
}
