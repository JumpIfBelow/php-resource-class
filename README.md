# PHP Resource Class: just enhance vanilla resources
ResourceClass enhance the way you use resources from procedural to fully-supported,
with the benefits of it.
It means you can clone, serialize, unserialize, destruct it fairly easily.
Oh, and it is unit tested.

## Development stage
This dev is still running. It is usable but not well tested.
Unit tests are to be created for all classes, except some that needs dedicated services.
**As it is still in development, those files could crash on your machine. You use it
at your own risks.**

## What's to be done?
- Create all unit tests
- Add more resources? (which ones?)
- Complete the PHP Docs
- Rename some methods with ugly names (needs more creativity here)
- Complete this README.md
- Make it available on Composer

## What it is
Vanilla PHP only allows resources to be used as is, with procedural functions.
There is some issues with this, like not knowing what functions are available, and
some functions are missing.
It adds more control on resources, like allowing serialization, auto close, copy, etc.

## What it is not
It does not adds more functions to the resources. This is a methodological adds, not
a functional one. cURL will still be cURL, it will only change the flavor by writing
it in OOP.

## How to install it?
Just use Composer, as usual:
```bash
composer require jumpifbelow/php-resource-class
```

## What are the differences
Procedural way:
```php
<?php

$f = fopen('dummy.txt', 'w');
fwrite($f, 'my data');

$copy = fopen('dummy.txt', 'w'); // needs to reenter same parameters

// hard to serialize
// $serialized = serialize($f); // WON'T WORK

// needs to close everything when we're done
fclose($f);
fclose($copy);
```
OOP way:
```php
<?php

use ResourceClass\File;

// simple to now what to use
$f = File::open('dummy.txt', 'w');
$f->write('my data');

// easy to copy
$copy = clone $f;

// now we can do this!
$serialized = serialize($f);
$f = unserialize($serialized);

// and we don't need to close the resource!
// you can still unset, it will close the resource automatically :)
unset($f);
```

## Security note
Note that **serializing an object with a resource that is created with credentials could be insecure**.
Those credentials will be stored. So if the string is stored in a file, you are basically putting
credentials clearly in a file.
You have to be sure on what you do with the serialization, I am not responsible on any bad practice
you are doing.
